<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'demoweba_wp-financial-life' );

/** MySQL database username */
define( 'DB_USER', 'demoweba_wp-financial-life' );

/** MySQL database password */
define( 'DB_PASSWORD', '$I*-9eHqVbP?' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'S>7f7.}?<%<36M+x9{:%NeLh.>z`@@&x}|IB--c4cQ3O0zmZ5#oK`*,AI|:jM=-W' );
define( 'SECURE_AUTH_KEY',  '90oCqx1*Kpd:8:$^vuXN>A %lNKTD8`>aj$-6yo7mM|!LtSbK,pBNo/R/:hWg0.B' );
define( 'LOGGED_IN_KEY',    '+VE}$F5`|r&=r{rf1:1&WyZG|[dUXmM-CQXfP*%d:7IDJbWjr*w.FYT~^KO:t:#n' );
define( 'NONCE_KEY',        'af7Ls+Ua ~Cbsy@u^Oz_&5,W*AH;0P#NlMX` <}&Gp}#{{N8%Xl8w_2WaB{La~40' );
define( 'AUTH_SALT',        '?ex`E63Y`$4)`}]_9l,v/OS9S|bDxZV)4^y!$*@[T-9PVSc61 K;_3%V7d,+#I6H' );
define( 'SECURE_AUTH_SALT', '[m$@$JQNk|UVad29Z3SGmfrl+DixDYtLBj;C~No=o3t-|`xv5Q{xK(cp*sLhr)r9' );
define( 'LOGGED_IN_SALT',   'Z~rf%-afbMlv=GEj-zw@&+$UK5$]T`0XUwqYNn m32~pxKl<K[Me2*<7g&8;zGZ7' );
define( 'NONCE_SALT',       '{iZd7UcokY7/6Zb`Es$O%=@8QqHX_~uBaZR6ZGg%W0I~XkNmrMseC)$m|.+e5$]n' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */

// Enable WP_DEBUG mode
define( 'WP_DEBUG', true );

// Enable Debug logging to the /wp-content/debug.log file
define( 'WP_DEBUG_LOG', true );

// Disable display of errors and warnings
define( 'WP_DEBUG_DISPLAY', false );
@ini_set( 'display_errors', 0 );

// Use dev versions of core JS and CSS files (only needed if you are modifying these core files)
define( 'SCRIPT_DEBUG', true );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );