<?php
/*
 * Plugin Name: Protection
 * Description: This plugin is made for Protection from the site and save it to the database.
 * Version: 1.0
 * Author: Sachin kuruvila
 * License: webandcrafts.com
 */


if (!class_exists('WP_List_Table')) {
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

class Protection extends WP_List_Table {

    /** Class constructor */
    public function __construct() {
        parent::__construct([
            'singular' => 'Protection',
            'plural' => 'Protection',
            'ajax' => false,
        ]);
    }

    /**
     * Retrieve Donation History data from the database
     *
     * @param int $per_page
     * @param int $page_number
     * @author Sachin Kuruvila
     * @return mixed
     */
    public static function get_protection($per_page = 20, $page_number = 1) {
        global $wpdb;
        if ($_REQUEST['page']) {
            $pro_type = filter_var($_REQUEST['page'], FILTER_SANITIZE_STRING);
        } else {
            $pro_type = 'protection';
        }

        $sql = "SELECT * FROM {$wpdb->base_prefix}protection WHERE `protection_name` ='" . $pro_type . "'";
        $sql .= !empty($_REQUEST['orderby']) ? ' ORDER BY ' . esc_sql($_REQUEST['orderby']) : ' ORDER BY id_protection';
        $sql .= !empty($_REQUEST['order']) ? ' ' . esc_sql($_REQUEST['order']) : ' DESC';
        $sql .= " LIMIT $per_page";
        $sql .= ' OFFSET ' . ( $page_number - 1 ) * $per_page;
        $protection = $wpdb->get_results($sql, 'ARRAY_A');
        $output = $result = [];
        //echo '<pre>';
        //print_r($protection);
        //echo '< /pre>';
        foreach ($protection as $history) {

            //  $donation_amount = isset($history['donation_amount']) ? $history['donation_amount'] : 0;

            $result[] = [
                'id_protection' => $history['id_protection'],
                'protection_name' => $history['protection_name'],
                'name' => $history['name'],
                'amount' => $history['amount'],
                'term_cover' => $history['term_cover'],
                'type' => $history['type'],
                'email' => $history['email'],
                'contact_number' => $history['contact_number'],
                'dob' => $history['dob'],
                'gender' => $history['gender'],
                'is_smoker' => $history['is_smoker'],
                'agecover' => $history['agecover'],
                'is_convertable' => $history['is_convertable'],
                'amount_illness_cover' => $history['amount_illness_cover'],
                'sec_dob' => $history['sec_dob'],
                'sec_gender' => $history['sec_gender'],
                'sec_is_smoker' => $history['sec_is_smoker']
            ];
        }
        return $result;
    }

    /**
     * Delete a record.
     * @author Sachin Kuruvila
     * @param int $id history ID
     */
    public static function delete_protection($id) {
        global $wpdb;

        $wpdb->delete("{$wpdb->base_prefix}protection", ['id_protection' => $id], ['%d']);
    }

    /**
     * Returns the count of records in the database.
     * @author Sachin Kuruvila
     * @return null|string
     */
    public static function record_count() {
        global $wpdb;
        if ($_REQUEST['page']) {
            $pro_type = filter_var($_REQUEST['page'], FILTER_SANITIZE_STRING);
        } else {
            $pro_type = 'protection';
        }
        $sql = "SELECT COUNT(*) FROM {$wpdb->base_prefix}protection  WHERE `protection_name` ='" . $pro_type . "'";

        return $wpdb->get_var($sql);
    }

    /** Text displayed when no Product Enquiry data is available */
    public function no_items() {
        echo 'No Protection avaliable.';
    }

    /**
     * Method for name column
     *
     * @param array $item an array of DB data
     * @author Sachin Kuruvila
     * @return string
     */
    function column_name($item) {

        // create a nonce
        $delete_nonce = wp_create_nonce('delete_protection');

        $title = '<strong>' . $item['name'] . '</strong>';
        //$innerpage = admin_url( 'view-donation-history.php');
        $actions = [
           // 'ids' => "ID: " . absint($item['id_protection']),
           // 'view' => sprintf('<a href="?page=view-donation-history.php&action=%s&viewprotection=%s">View</a>', 'view', absint($item['id_protection'])),
            'delete' => sprintf('<a  onClick="return confirm(\'Do you want to delete?\')" href="?page=%s&action=%s&protection=%s&_wpnonce=%s" >Delete</a>', esc_attr($_REQUEST['page']), 'delete', absint($item['id_protection']), $delete_nonce),
        ];

        return $title . $this->row_actions($actions);
    }

    /**
     * Render a column when no column specific method exists.
     *
     * @param array $item
     * @param string $column_name
     * @author Sachin Kuruvila
     * @return mixed
     */
    public function column_default($item, $column_name) {
        $output='';
        switch ($column_name) {
            case 'name':
            case 'protection_name': if ($item[$column_name] == 'protection') {
                    $type_name = 'Mortgage Protection Form';
                } elseif ($item[$column_name] == 'life_insurance') {
                    $type_name = 'Life Insurance';
                } elseif ($item[$column_name] == 'serious_illness') {
                    $type_name = 'Life & Serious Illness';
                } else {
                    $type_name = 'default';
                }
                return $type_name;
            case 'amount': return $item[$column_name];
            case 'term_cover': return $item[$column_name];
            case 'email': return $item[$column_name];
            case 'contact_number': return $item[$column_name];
            case 'type': 
                    if($item[$column_name] == 'Joint') {
                        $issec_smoker = ($item['sec_is_smoker'] == 0) ? 'No' : 'Yes';
                        $output = $item[$column_name] . 
                        $output .= '<input alt="#TB_inline?height=300&amp;width=400&amp;inlineId=sec'.$item['id_protection'].'" title="Second Person" class="thickbox" type="button" value="View" />';
                        $output .= '<div id="sec'.$item['id_protection'].'" style="display:none;">';
                        $output .= '<div class="sec-table"><span>DOB: </span><p>'.$item['sec_dob'].'<p></div>';
                        $output .= '<div class="sec-table"><span>Gender: </span><p>'.$item['sec_gender'].'</p></div>';
                        $output .= '<div class="sec-table"><span>Smoker: </span><p>'.$issec_smoker.'</p></div>';
                        $output .= '</div>'; 
                    } else {
                        $output = $item[$column_name];
                    }
                
                return $output;    
            case 'dob': return $item[$column_name];
            case 'agecover': return $item[$column_name];   
            
            case 'is_smoker':
                if ($item[$column_name] == 1) {
                    $is_smoker = 'Yes';
                } else {
                    $is_smoker = 'No';
                }
                return $is_smoker;
            
             case 'is_convertable':
                if ($item[$column_name] == 1) {
                    $is_convertable = 'Yes';
                } else {
                    $is_convertable = 'No';
                }
                return $is_convertable;
            
            case 'gender':return $item[$column_name];
            case 'amount_illness_cover':return $item[$column_name];
            default:
                return $item[$column_name]; //Show the whole array for troubleshooting purposes
        }
      }   

    /**
     * Render the bulk edit checkbox
     *
     * @param array $item
     * @author Sachin Kuruvila
     * @return string
     */
    function column_cb($item) {
        return sprintf(
                '<input type="checkbox" name="bulk-delete[]" value="%s" />', $item['id_protection']
        );
    }
function my_custom_redirect() {
                wp_redirect(esc_url(add_query_arg()));
            }
    /**
     *  Associative array of columns
     * @author Sachin Kuruvila
     * @return array
     */
    function get_columns() {
        if ($_REQUEST['page'] == 'income_protection') {
        $columns = [
            'cb' => '<input type="checkbox" />',
            'id_protection' => 'ID',
            'name' => 'Name',
            'protection_name' => 'Protection',          
            'amount' => 'Amount life cover',
            'term_cover' => 'Term Cover',
            'email' => 'Email',
            'contact_number' => 'Phone',
            'email' => 'Email',
            'type' => 'Type',
            'dob' => 'Date of birth',
            'gender' => 'Gender',
            'is_smoker' => 'Smoker',
            'agecover' => 'Agecover',
        ];
        }
        else if ($_REQUEST['page'] == 'life_insurance') {
          $columns = [
              'cb' => '<input type="checkbox" />',
              'id_protection' => 'ID',
              'name' => 'Name',
              'protection_name' => 'Protection',          
              'amount' => 'Amount life cover',
              'term_cover' => 'Term Cover',
              'email' => 'Email',
              'contact_number' => 'Phone',
              'email' => 'Email',
              'type' => 'Type',
              'dob' => 'Date of birth',
              'gender' => 'Gender',
              'is_smoker' => 'Smoker',
              'is_convertable' => 'Convertable',
          ];
        }
      
        else if ($_REQUEST['page'] == 'serious_illness') {
          $columns = [
              'cb' => '<input type="checkbox" />',
              'id_protection' => 'ID',
              'name' => 'Name',
              'protection_name' => 'Protection',          
              'amount' => 'Amount life cover',
              'term_cover' => 'Term Cover',
              'email' => 'Email',
              'contact_number' => 'Phone',
              'email' => 'Email',
              'type' => 'Type',
              'dob' => 'Date of birth',
              'gender' => 'Gender',
              'is_smoker' => 'Smoker',
              'is_convertable' => 'Convertable',
              'amount_illness_cover' => 'Amount illness Cover',
          ];
        }
      
        else {
            
           $columns = [
            'cb' => '<input type="checkbox" />',
            'id_protection' => 'ID',
            'name' => 'Name',
            'protection_name' => 'Protection',          
            'amount' => 'Amount life cover',
            'term_cover' => 'Term Cover',
            'email' => 'Email',
            'contact_number' => 'Phone',
            'email' => 'Email',
            'type' => 'Type',
            'dob' => 'Date of birth',
            'gender' => 'Gender',
            'is_smoker' => 'Smoker',
        ]; 
        }
        return $columns;
    }

    /**
     * Columns to make sortable.
     * @author Sachin Kuruvila
     * @return array
     */
    public function get_sortable_columns() {
        $sortable_columns = array(
            'id_protection' => array('id_protection', true),
            'protection_name' => array('protection_name', true),
            'name' => array('name', true),
            'amount' => array('amount', true),
            'contact_number' => array('contact_number', false),
            'dob' => array('dob', false),
            'email' => array('email', false),
            'is_smoker' => array('is_smoker', false),
            'term_cover' => array('term_cover', false),
            'agecover' => array('agecover', false),
            'is_convertable' => array('is_convertable', true),
            'amount_illness_cover' => array('amount_illness_cover', true),
        );

        return $sortable_columns;
    }

    /**
     * Returns an associative array containing the bulk action
     * @author Sachin Kuruvila
     * @return array
     */
    public function get_bulk_actions() {
        $actions = [
            'bulk-delete' => 'Delete'
        ];

        return $actions;
    }

    /**
     * Handles data query and filter, sorting, and pagination.
     */
    public function prepare_items() {

        $this->_column_headers = $this->get_column_info();

        /** Process bulk action */
        $this->process_bulk_action();

        $per_page = $this->get_items_per_page('protection_per_page', 20);
        $current_page = $this->get_pagenum();
        $total_items = self::record_count();

        $this->set_pagination_args([
            'total_items' => $total_items, //WE h ave to calculate the total number of items
            'per_page' => $per_page //WE have to determine how many items to show on a page
        ]);


        $this->items = self::get_protection($per_page, $current_page);
    }

    public function process_bulk_action() {

        //Detect when a bulk action is being triggered...
        if ('delete' === $this->current_action()) {

            // In our file that handles the request, verify the nonce.
            $nonce = esc_attr($_REQUEST['_wpnonce']);

            if (!wp_verify_nonce($nonce, 'delete_protection')) {
                die('Go get a life script kiddies');
            } else {
                self::delete_protection(absint($_GET['protection']));

                add_action('wp_loaded', 'my_custom_redirect');
            }
            
        }

        // If the delete bulk action is triggered
        if (( isset($_POST['action']) && $_POST['action'] == 'bulk-delete' ) || ( isset($_POST['action2']) && $_POST['action2'] == 'bulk-delete' )
        ) {

            $delete_ids = esc_sql($_POST['bulk-delete']);

            // loop over the array of record IDs and delete them
            foreach ($delete_ids as $id) {
                self::delete_protection($id);
            }

             add_action('wp_loaded', 'my_custom_redirect');
        }
    }

}

// PE for Donation History
class DH_Plugin {

    // class instance
    static $instance;
    // Donation History List object
    public $protection_obj;

    // class constructor
    public function __construct() {
        add_filter('set-screen-option', [__CLASS__, 'set_screen'], 10, 3);
        add_action('admin_init', [$this, 'create_donation_table']);
        add_action('admin_menu', [$this, 'plugin_menu']);

        // add_action('wp_ajax_make_donation', [ $this, 'process_donation_function']);
        // add_action('wp_ajax_nopriv_make_donation', [ $this, 'process_donation_function']);
        // add_action('wp_ajax_update_donation', [ $this, 'update_donation_callback']);
        // add_action('wp_ajax_nopriv_update_donation', [ $this, 'update_donation_callback']);
    }

    /*
     *  Donation section script starts here
     * Code added By sachin
     */


    /*
     * Donation intimation mail to the donors.
     */

    public function donationMail($name, $email, $amount) {
        // Always set content-type when sending HTML email
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        // More headers
        $headers .= 'From: Believers Eastern Church <websupport@bec.org>' . "\r\n";
        // $headers .= get_option('donation_mail_from') . "\r\n";
        $msg = get_option('donation_mail_content');
        $msg = str_replace('[name]', $name, $msg);
        $msg = str_replace('[amount]', "$" . $amount, $msg);
        $msg = html_entity_decode($msg);
        wp_mail($email, get_option('donation_mail_subject'), $msg, $headers);
    }

    /*
     *  Donation section script starts here
     * Code added By sachin
     */

    // creating table for the donation
    public function create_donation_table() {
        global $wpdb;
        $charset_collate = $wpdb->get_charset_collate();

        $sql = "CREATE TABLE `wp_protection` (
        `id_protection` int(11) NOT NULL,
        `protection_name` text NOT NULL,
        `amount` int(11) NOT NULL,
        `term_cover` varchar(256) NOT NULL,
        `type` varchar(256) NOT NULL,
        `name` varchar(256) NOT NULL,
        `contact_number` text NOT NULL,
        `email` varchar(256) NOT NULL,
        `dob` text NOT NULL,
        `gender` text NOT NULL,
        `is_smoker` tinyint(4) NOT NULL
      ) ENGINE=InnoDB DEFAULT CHARSET=$charset_collate;";

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql);
//        $wpdb->query($wpdb->prepare("ALTER TABLE `{$wpdb->base_prefix}wp_protection` CHANGE `authzcode` `authzcode` VARCHAR(150) NULL DEFAULT NULL;"));
//        $test = $wpdb->get_results("SHOW COLUMNS FROM `{$wpdb->base_prefix}wp_protection`");       
        // $success = empty($wpdb->last_error);
//        echo $wpdb->last_error;
        // die();
    }

    public static function set_screen($status, $option, $value) {
        return $value;
    }

    public function plugin_menu() {
        $hook = add_menu_page(
                'Protection', 'Protection', 'manage_options', 'protection', [$this, 'plugin_settings_page'], "dashicons-backup", 58
        );
        add_action("load-$hook", [$this, 'screen_option']);
        $hook_mortage = add_submenu_page('protection', 'Mortage Protection', 'Mortage Protection', 'manage_options', 'protection', [$this, 'plugin_settings_page']);
        add_action("load-$hook_mortage", [$this, 'screen_option']);
        $hook_insurance = add_submenu_page('protection', 'Life insurance', 'Life insurance', 'manage_options', 'life_insurance', [$this, 'life_insurance']);
        add_action("load-$hook_insurance", [$this, 'screen_option']);
        $hook_illness = add_submenu_page('protection', 'Life & Serious Illness', 'Life & Serious Illness', 'manage_options', 'serious_illness', [$this, 'serious_illness']);
        add_action("load-$hook_illness", [$this, 'screen_option']);
        $hook_income = add_submenu_page('protection', 'Income Protection', 'Income Protection', 'manage_options', 'income_protection', [$this, 'income_protection']);
        add_action("load-$hook_income", [$this, 'screen_option']);
        $hook_life = add_submenu_page('protection', 'Whole of Life cover', 'Whole of Life cover', 'manage_options', 'whole_life_cover', [$this, 'whole_life_cover']);
        add_action("load-$hook_life", [$this, 'screen_option']);
        add_menu_page('View Donation Details', 'View', 'manage_options', 'view-donation-history.php', [$this, 'donation_view_page'], 'dashicons-tickets', 6);
        remove_menu_page('view-donation-history.php');
    }

    /**
     * Screen options
     */
    public function screen_option() {

        $option = 'per_page';
        $args = [
            'label' => 'Name',
            'default' => 20,
            'option' => 'protection_per_page'
        ];

        add_screen_option($option, $args);

        $this->protection_obj = new protection();
    }

    /**
     * Plugin settings page
     */
    public function plugin_settings_page() {
        ?>
<div class="wrap">
  <h2>Protection</h2>
  <div id="poststuff">
    <div id="post-body" class="metabox-holder">
      <div id="post-body-content">
        <div class="meta-box-sortables ui-sortable">
          <form method="post">
            <?php
                                $this->protection_obj->prepare_items();
                                $this->protection_obj->display();
                                ?>
          </form>
        </div>
      </div>
    </div>
    <br class="clear">
  </div>
</div>
<?php
    }

    public function life_insurance() {
        ?>
<div class="wrap">
  <h2>Life insurance</h2>
  <div id="poststuff">
    <div id="post-body" class="metabox-holder">
      <div id="post-body-content">
        <div class="meta-box-sortables ui-sortable">
          <form method="post">
            <?php
                                $this->protection_obj = new Protection();
                                $this->protection_obj->prepare_items();
                                $this->protection_obj->display();
                                ?>
          </form>
        </div>
      </div>
    </div>
    <br class="clear">
  </div>
</div>
<?php
    }

    public function serious_illness() {
        ?>
<div class="wrap">
  <h2>Life & Serious Illness</h2>
  <div id="poststuff">
    <div id="post-body" class="metabox-holder">
      <div id="post-body-content">
        <div class="meta-box-sortables ui-sortable">
          <form method="post">
            <?php
                                $this->protection_obj = new Protection();
                                $this->protection_obj->prepare_items();
                                $this->protection_obj->display();
                                ?>
          </form>
        </div>
      </div>
    </div>
    <br class="clear">
  </div>
</div>
<?php
    }

    public function income_protection() {
        ?>
<div class="wrap">
  <h2>Income Protection</h2>
  <div id="poststuff">
    <div id="post-body" class="metabox-holder">
      <div id="post-body-content">
        <div class="meta-box-sortables ui-sortable">
          <form method="post">
            <?php
                                $this->protection_obj = new Protection();
                                $this->protection_obj->prepare_items();
                                $this->protection_obj->display();
                                ?>
          </form>
        </div>
      </div>
    </div>
    <br class="clear">
  </div>
</div>
<?php
    }

    public function whole_life_cover() {
        ?>
<div class="wrap">
  <h2>Whole of Life cover</h2>
  <div id="poststuff">
    <div id="post-body" class="metabox-holder">
      <div id="post-body-content">
        <div class="meta-box-sortables ui-sortable">
          <form method="post">
            <?php
                                $this->protection_obj = new Protection();
                                $this->protection_obj->prepare_items();
                                $this->protection_obj->display();
                                ?>
          </form>
        </div>
      </div>
    </div>
    <br class="clear">
  </div>
</div>
<?php
    }

    /** Singleton instance */
    public static function get_instance() {
        if (!isset(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

}

add_action('plugins_loaded', function () {
    DH_Plugin::get_instance();
});
