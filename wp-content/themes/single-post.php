<?php
/* Template for displaying single blog */
get_header();
global $post;
?>

    <section class="bloginner">
        <div class="container">
            <div class="brudcum">
                <ul>
                    <li><?php echo get_hansel_and_gretel_breadcrumbs(); ?></li>
                </ul>
            </div>
            <?php
            if (have_posts()):
            while (have_posts()): the_post();
            $latest_post = array(get_the_ID());
            $blog_featured = get_the_post_thumbnail_url(get_the_id(),'blog_featured');
            $content = wpautop( $post->post_content );
            $categories_p = get_the_terms(get_the_ID(), 'category');
            $cats_res=array();
                foreach($categories_p as $catp){
                    array_push($cats_res,$catp->slug); 
                }
             ?>

            <div class="bloginnerBnr" style="background-image: url('<?= $blog_featured ?>')"></div>
            <div class="blogdetail">
                <div class="sharebtn sticky">
                    <span class="icon-fb"></span>
                    <span class="icon-twiter"></span>
                </div>
                <span class="blogtype">
                    <p class="type <?php  echo implode(" ",$cats_res); ?>"><?php  echo implode(" ",$cats_res); ?></p>
                    <i class="line"></i>
                    <span class="img"><?php echo get_avatar( get_the_author_meta( 'ID' ), 32 ); ?></span>
                    <p class="name"><?php the_author(); ?></p>
                </span>
                <div class="detailCntnt">
                    <h1><?php echo get_the_title(); ?></h1>
                    <P><?php echo $content ?></p>
                </div>
            </div>
            <?php endwhile; endif; wp_reset_postdata(); ?>

            <div class="row sticky-stopper">
                <span class="latetsBlog"><h2>Latest Blogs</h2></span>
                <?php
                $args = array(
                    'post_type' => 'post', 'post_status' => 'publish', 'posts_per_page' => 3,'order' => 'DESC',//'orderby' => 'date'
                );
                $my_query = '';
                $my_query = new WP_Query($args);
                if ($my_query->have_posts()):
                while ($my_query->have_posts()):
                $my_query->the_post();
                $categories_p = get_the_terms(get_the_ID(), 'category');
                $blog_all_img = get_the_post_thumbnail_url(get_the_id(),'blog_all_img');
                $post_date = get_the_date( 'F j, Y' );
                $cats_res=array();
                foreach($categories_p as $catp){
                    array_push($cats_res,$catp->slug); 
                }
                ?>
                <div class="column">
                    <a href="blog-inner.php">
                    <div class="blogBox">
                        <div class="blogbanner"><div style="background-image: url('<?= $blog_all_img ?>')"></div></div>
                        <span class="blogtype">
                            <p class="type <?php  echo implode(" ",$cats_res); ?>"><?php  echo implode(" ",$cats_res); ?></p>
                            <i class="line"></i>
                            <span class="img"><?php echo get_avatar( get_the_author_meta( 'ID' ), 32 ); ?></span>
                            <p class="name"><?php the_author(); ?></p>
                        </span>
                        <h5 class="blogtitile"><?php echo get_the_title(); ?></h5>
                        <p class="blogdate"><?= $post_date;?></p>
                    </div>
                    </a>
                </div>
                <?php endwhile; endif; wp_reset_postdata(); ?>
            </div>
        </div>
    </section>
<?php include('footer.php'); ?>