<?php

/**
  Template Name: mortgage-protection
*/


get_header();
?>

<div id="barba-wrapper">
  <div class="barba-container mortgage-protection" data-namespace="mortgage-protection">
    <section class="banner mortgage-protection-banner" style="background-image: url(<?php the_field('banner_background_image'); ?>);">
      <div class="fl-container">
        <div class="inner_banner_contnt">
          <div class="banner_data">
            <h1><?php the_field('inner_banner_title');?></h1>
            <p><?php the_field('inner_banner_sub_content');?></p>
            <div class="get_q_wrpr">
              <a class="animate-btn blue" href="<?php echo get_page_link( get_page_by_path( 'contact-us' ) ); ?>">Contact Us<span></span></a>
              <a class="animate-btn blue" href="<?php the_field('get_quote_url');?>">Get Quote<span></span></a>
              <!--
              <div class="vdo_wrpr">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/play.svg">
                <p><?php the_field('video_button_title');?></p>
              </div>
              -->
            </div>
          </div>
          <div class="banner_img">
            <img src="<?php the_field('inner_banner_icon'); ?>">
          </div>
        </div>
      </div>
    </section>

    <section class="mortgage-protection-cover section-2 mortgage-protection_scroll_box">
      <span class="cover_bg"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/mortgage_cover.svg"></span>
      <div class="fl-container">
        <h3><?php the_field('protection_title');?></h3>
        <p><?php the_field('protection__sub_content');?></p>
        <div class="cover-slider" id="cover-slider">
          <?php if( have_rows('protection__slide_content') ): ?>
          <?php while( have_rows('protection__slide_content') ): the_row(); ?>
          <div class="item cover_item">
            <img src="<?php the_sub_field('icon');?>">
            <h4><?php the_sub_field('title');?></h4>
            <p><?php the_sub_field('details');?></p>
          </div>
          <?php endwhile; ?>
          <?php endif; ?>
        </div>
      </div>
    </section>

    <section class="mpi-section section-3">
      <div class="fl-container">
        <div class="mpi_wrpr">
          <div class="mpi_img_wrpr">
            <img src="<?php the_field('mpi_main_image');?>">
          </div>
          <div class="mpi_contnt_wrpr">
            <h3><?php the_field('mpi_title');?></h3>
            <p><?php the_field('mpi_sub_content');?></p>
            <ul>
              <?php if( have_rows('mpi_list') ): ?>
              <?php while( have_rows('mpi_list') ): the_row(); ?>
              <li><span><img src="<?php the_sub_field('list_icon');?>"></span> <b><?php the_sub_field('list_title');?></b>
                <p><?php the_sub_field('list_detail_content');?></p>
              </li>
              <?php endwhile; ?>
              <?php endif; ?>
            </ul>
            <a class="animate-btn blue" href="<?php the_field ('get_quote_url');?>">Get Quote<span></span></a>
          </div>

        </div>
      </div>
    </section>

    <!--
    <section class="three_steps section-4">
      <div class="fl-container">
        <h3>Easy to start mortgage protection in <span>3 simple steps</span></h3>

        <div class="protcton_steps">
          <?php if( have_rows('simple_steps') ): ?>
          <?php while( have_rows('simple_steps') ): the_row(); ?>
          <div class="protction_step_blk">
            <img src="<?php the_sub_field('step_symbol');?>">
            <h4><?php the_sub_field('step_title');?></h4>
            <p><?php the_sub_field('step_content');?></p>
          </div>
          <?php endwhile; ?>
          <?php endif; ?>
        </div>
        <a class="animate-btn blue" href="<?php the_field('get_quote_url');?>">Get Quote<span></span></a>
      </div>
    </section>
    -->
  </div>
</div>


<?php
get_footer();