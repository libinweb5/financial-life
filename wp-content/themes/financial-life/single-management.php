<?php


/* Template for displaying single management */



get_header(); ?>

<?php

if (isset($_GET['pgid'])) {
    $mgmt_page_id = $_GET['pgid'];
    //// echo get_the_title($mgmt_page_id);
    //// echo get_field('designation',$mgmt_page_id);
    //get_field('list_data',$mgmt_page_id);
    //$adv_email_id = get_field('advisors_email_id',$mgmt_page_id);
  
}

$adv_email_id = get_field( "advisors_email_id" );



?>

<div id="barba-wrapper">
  <div class="barba-container contact" data-namespace="contact">
    <section class="about-section management_single">
      <div class="fl-container">
        <div class="team_wrpr">

          <div class="grp_members_wrpr">
            <?php
            $pid=get_the_ID();
              $latest_post = array(get_the_ID());
              $blog_featured = get_the_post_thumbnail_url(get_the_id(),'blog_featured');
              $content = wpautop( $post->post_content );
            ?>

            <div class="management_single_img-wrap">
              <img src="<?= $blog_featured ?>">
            </div>
            <div class="advisors_details">
              <h4><?php the_title(); ?></h4>
              <p><?php the_field('designation');?></p>
              <ul>
                <li><span>Mobile</span><a href="<?php the_field('advisors_phone_number');?>"><?php the_field('advisors_phone_number');?></a></li>
                <li><span>Email</span> <a href="<?php the_field('advisors_email_id');?>" target="_blank"><?php the_field('advisors_email_id');?></a></li>
                <li><span>LinkedIn </span><a href="<?php the_field('linkedin_link');?>" target="_blank"><?php the_field('linkedin_link');?></a></li>
              </ul>

<!--              <a href="javascript:void(0);" class="contact_btn_wrap">Contact Now</a>-->
            </div>

            <div class="management_single_content-wrap">
              <div class="description_box"><?php the_field('description');?></div>
            </div>
          </div>
          <div class="grp_members_fom">
            <div class="contact_form-management">
              <div class="contact-box mort-popup">
                <div class="popup_contnt">
                 
                  <form id="client-form" novalidate="novalidate">
                    <?php
                  if( $adv_email_id ) {
                    ?>
                    <input type="hidden" name="adv_email" value="<?php echo $adv_email_id ?>">
                    <?php
                     }
                  ?>
                    <input type="hidden" name="action" value="clientForm">
                    <div class="is-floating-label">
                      <label for="name">Name</label>
                      <input type="text" name="name" id="name">
                    </div>
                    <div class="is-floating-label">
                      <label for="email">Email</label>
                      <input type="text" name="email" id="email">
                    </div>
                    <div class="is-floating-label">
                      <label for="contact">Phone</label>
                      <input type="text" name="contact" id="contact">
                    </div>

                    <div class="is-floating-label">
                      <select name="query" id="query">
                        <option value="0" disabled selected>Nature of Query</option>
                        <option value="Life Insurance">Life Insurance</option>
                        <option value="Serious Illness Cover">Serious Illness Cover</option>
                        <option value="Income Protection">Income Protection</option>
                        <option value="Pension">Pension</option>
                        <option value="Investment">Investment</option>
                        <option value="Mortgage">Mortgage</option>
                        <option value="Other">Other</option>
                      </select>
                    </div>

                    <div class="is-floating-label textarea">
                      <label for="comments">Your Message </label>
                      <textarea rows="7" cols="50" name="comments"></textarea>
                    </div>

                    <div class="contactbtn-wrap animate-btn blue">
                      <input style="" type="submit" id="" name="submit" value="Contact Now" />
                      <span></span>
                    </div>
                    <div id="myElem" style="display:none">
                      <p>Thank you for contacting us.</p>
                    </div>
                  </form>

                </div>
              </div>
            </div>
          </div>

        </div>
      </div>

    </section>
  </div>
</div>



<?php get_footer(); ?>
