<?php

/**
  Template Name: investment
*/

get_header();
?>

<div id="barba-wrapper" class="investment_wrap">
  <div class="barba-container investment" data-namespace="investment">
    <section class="banner mortgage-protection-banner testimonial_banner" style="background-image: url(<?php the_field('inner_banner_bg');?>);">
      <div class="fl-container">
        <div class="inner_banner_contnt">
          <div class="banner_data">
            <h1><?php the_field('inner_banner_content');?></h1>
            <p><?php the_field('inner_banner_sub_content');?></p>
          </div>
        </div>
      </div>
    </section>
    <section class="mpi-section investment_ex section-3">
      <div class="fl-container">
        <div class="mpi_wrpr">
          <div class="mpi_img_wrpr">
            <img src="<?php the_field('mpi_main_image');?>">
          </div>
          <div class="mpi_contnt_wrpr investment_n_wrap">
            <h3><?php the_field('mpi_title');?>:</h3>
            <div class="know_you_content">
              <?php the_field('getting_to_know_you');?>
            </div>
            <ul>
              <?php if( have_rows('mpi_list') ): ?>
              <?php while( have_rows('mpi_list') ): the_row(); ?>
              <li><span><img src="<?php the_sub_field('list_icon');?>"></span> <b><?php the_sub_field('list_title');?></b>
              </li>
              <?php endwhile; ?>
              <?php endif; ?>
            </ul>
            <div>
            <!-- <?php the_field('investments_list_contents');?> -->
            </div>
            <!-- <a class="animate-btn blue" href="<?php the_field ('get_quote_url');?>">Enquire Now<span></span></a>-->
          </div>
        </div>
      </div>
    </section>

    <section class="investment_plan_section">
      <div class="fl-container">
        <div class="mpi_wrpr">
          <?php the_field('investments_list_contents');?>

        </div>
      </div>
    </section>

    <section class="mort-popup section-1">
      <div class="popup_contnt">
        <div class="fl-container">
          <div class="form_head">
            <h4>Investment</h4>
          </div>
          <div class="modal-body">
            <form action="" id="investment">
              <input type="hidden" name="action" value="investmentform" />
              <input type="hidden" name="protection_type" value="7" id="protection_type" />
              <div class="is-floating-label">
                <label for="name">Name</label>
                <input type="text" name="name" id="name" />
              </div>
              <div class="is-floating-label">
                <label for="contact">Contact Number</label>
                <input type="text" name="contact" id="contact" />
              </div>
              <div class="is-floating-label">
                <label for="email">Email</label>
                <input type="text" name="email" id="email" />
              </div>
              <div class="is-floating-label">
                <label for="income">Approx Annual Income</label>
                <input type="text" name="income" id="income" />
              </div>

              <div class="dob_wrpr">
                <label>Date Of Birth</label>
                <div class="date_wrpr">
                  <div class="is-floating-label datebirth">
                    <select name="day" id="day">
                      <option value="0" disabled selected>DD</option>
                      <option value="01">1</option>
                      <option value="02">2</option>
                      <option value="03">3</option>
                      <option value="04">4</option>
                      <option value="05">5</option>
                      <option value="06">6</option>
                      <option value="07">7</option>
                      <option value="08">8</option>
                      <option value="09">9</option>
                      <option value="10">10</option>
                      <option value="11">11</option>
                      <option value="12">12</option>
                      <option value="13">13</option>
                      <option value="14">14</option>
                      <option value="15">15</option>
                      <option value="16">16</option>
                      <option value="17">17</option>
                      <option value="18">18</option>
                      <option value="19">19</option>
                      <option value="20">20</option>
                      <option value="21">21</option>
                      <option value="22">22</option>
                      <option value="23">23</option>
                      <option value="24">24</option>
                      <option value="25">25</option>
                      <option value="26">26</option>
                      <option value="27">27</option>
                      <option value="28">28</option>
                      <option value="29">29</option>
                      <option value="30">30</option>
                      <option value="31">31</option>
                    </select>
                  </div>
                  <div class="is-floating-label datebirth">
                    <select name="month" id="month">
                      <option value="MM" disabled selected>MM</option>
                      <option value="01">Jan</option>
                      <option value="02">Feb</option>
                      <option value="03">Mar</option>
                      <option value="04">Apr</option>
                      <option value="05">May</option>
                      <option value="06">Jun</option>
                      <option value="07">Jul</option>
                      <option value="08">Aug</option>
                      <option value="09">Sep</option>
                      <option value="10">Oct</option>
                      <option value="11">Nov</option>
                      <option value="12">Dec</option>
                    </select>
                  </div>
                  <div class="is-floating-label datebirth">
                    <select name="years" id="years">
                      <option value="YY" disabled selected>YY</option>
                      <option value="2001">2001</option>
                      <option value="2000">2000</option>
                      <option value="1999">1999</option>
                      <option value="1998">1998</option>
                      <option value="1997">1997</option>
                      <option value="1996">1996</option>
                      <option value="1995">1995</option>
                      <option value="1994">1994</option>
                      <option value="1993">1993</option>
                      <option value="1992">1992</option>
                      <option value="1991">1991</option>
                      <option value="1990">1990</option>
                      <option value="1989">1989</option>
                      <option value="1988">1988</option>
                      <option value="1987">1987</option>
                      <option value="1986">1986</option>
                      <option value="1985">1985</option>
                      <option value="1984">1984</option>
                      <option value="1983">1983</option>
                      <option value="1982">1982</option>
                      <option value="1981">1981</option>
                      <option value="1980">1980</option>
                      <option value="1979">1979</option>
                      <option value="1978">1978</option>
                      <option value="1977">1977</option>
                      <option value="1976">1976</option>
                      <option value="1975">1975</option>
                      <option value="1974">1974</option>
                      <option value="1973">1973</option>
                      <option value="1972">1972</option>
                      <option value="1971">1971</option>
                      <option value="1970">1970</option>
                      <option value="1969">1969</option>
                      <option value="1968">1968</option>
                      <option value="1967">1967</option>
                      <option value="1966">1966</option>
                      <option value="1965">1965</option>
                      <option value="1964">1964</option>
                      <option value="1963">1963</option>
                      <option value="1962">1962</option>
                      <option value="1961">1961</option>
                      <option value="1960">1960</option>
                      <option value="1959">1959</option>
                      <option value="1958">1958</option>
                      <option value="1957">1957</option>
                      <option value="1956">1956</option>
                      <option value="1955">1955</option>
                      <option value="1954">1954</option>
                      <option value="1953">1953</option>
                      <option value="1952">1952</option>
                      <option value="1951">1951</option>
                      <option value="1950">1950</option>
                      <option value="1949">1949</option>
                      <option value="1948">1948</option>
                      <option value="1947">1947</option>
                      <option value="1946">1946</option>
                      <option value="1945">1945</option>
                      <option value="1944">1944</option>
                      <option value="1943">1943</option>
                      <option value="1942">1942</option>
                      <option value="1941">1941</option>
                      <option value="1940">1940</option>
                      <option value="1939">1939</option>
                      <option value="1938">1938</option>
                      <option value="1937">1937</option>
                    </select>
                  </div>
                </div>
              </div>


              <!--
              <div class="radio-label">
                <p>
                  <input type="radio" id="test3" name="gender1a" value="Male">
                  <label for="test3"> <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/man.svg">Male</label>
                </p>
                <p>
                  <input type="radio" id="test4" name="gender1a" value="Female">
                  <label for="test4"> <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/woman.svg">Female</label>
                </p>
              </div>

              <div class="radio-wrpr">
                <label>Smoker</label>
                <div class="radio-label">
                  <p>
                    <input type="radio" id="test5" name="radiosmoker" value="Yes">
                    <label for="test5"> <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/smoker.svg">Yes</label>
                  </p>
                  <p>
                    <input type="radio" id="test6" name="radiosmoker" value="No">
                    <label for="test6"> <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/no_smoke.svg">No</label>
                  </p>
                </div>
              </div>
-->


              <div class="termandcodtn_wrpr">
                <div class="condtn_wrpr">
                  <div class="chk_box">
                    <input type="checkbox" id="terms" name="terms" />
                    <label class="cutm_chk" for="terms"></label>
                  </div>
                  <p>I have read and accept the <a href="#" class="term-business">Terms of Business</a> and <a href="#" class="privacy-statement">Privacy Statement</a></p>
                </div>
                <div class="formbtn-wrap animate-btn blue">
                  <input type="submit" class="" id="submitbtn" name="submit" onclick="" value="Send" />
                  <span></span>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>
  </div>
</div>


<?php get_footer();
