<?php

/**
  Template Name: contact
*/

get_header();
?>
<div id="barba-wrapper">
  <div class="barba-container contact" data-namespace="contact">
    <main>
      <section class="contact-nav">
        <div class="container">
          <div class="row">
            <article class="col-lg-12 head">
              <hr>
              <h4><?php the_field('contact_page_title');?></h4>
              <p><?php the_field('contact_page_sub_title');?></p>
            </article>
            <div class="col-lg-6">
              <div class="left-box mail">
                <img src="<?php the_field('by_form_icon');?>" alt="">

                <h4><?php the_field('by_form_tilte');?></h4>
                <p><?php the_field('by_form_sub_content');?></p>
              </div>

              <div class="left-box">
                <img src="<?php the_field('by_phone_icon');?>" alt="">

                <h4><?php the_field('by_phone_tilte');?></h4>
                <p><?php the_field('by_phone_sub_content');?></p>

                <?php if( have_rows('by_phone')): ?>
                <?php while( have_rows('by_phone')): the_row(); ?>
                <p><b><?php the_sub_field('phone_title');?></b></p>
                <h3><a href="tel:<?php the_sub_field('number');?>"><?php the_sub_field('number');?></a></h3>
                <?php endwhile; ?>
                <?php endif; ?>
              </div>
            </div>
            <div class="col-lg-6">
              <div class="contact-box mort-popup">
                <div class="popup_contnt">
                  <form id="contact-form">
                    <input type="hidden" name="action" value="consultForm" />
                    <div class="is-floating-label">
                      <label for="name">Name</label>
                      <input type="text" name="name" id="name">
                    </div>
                    <div class="is-floating-label">
                      <label for="email">Email</label>
                      <input type="text" name="email" id="email">
                    </div>
                    <div class="is-floating-label">
                      <label for="contact">Phone</label>
                      <input type="text" name="contact" id="contact">
                    </div>
                    <div class="is-floating-label">
                      <select name="protection" id="protection">
                        <option value="0" disabled selected>Subject</option>
                        <option value="Mortgage Protection">Mortgage Protection</option>
                        <option value="life insurance">life insurance</option>
                        <option value="Life & Serious Illness">Life & Serious Illness</option>
                        <option value="Income Protection">Income Protection</option>
                        <option value="Pensions">Pensions</option>
                        <option value="Mortages">Mortages</option>
                        <option value="Investments">Investments</option>
                      </select>
                    </div>
                    <div class="is-floating-label textarea">
                      <label for="massage">Message</label>
                      <textarea rows="7" cols="50" name="massage"></textarea>
                    </div>

                    <div class="termandcodtn_wrpr check_terms">
                      <div class="condtn_wrpr">
                        <div class="chk_box">
                          <input type="checkbox" id="terms" name="terms">
                          <label class="cutm_chk" for="terms"></label>
                        </div>
                        <p>I have read and accept the <a href="#" class="term-business">Terms of Business</a> and <a href="#" class="privacy-statement">Privacy Statement</a></p>
                      </div>
                    </div>

                    <div class="recaptch_wrap">
                      <div class="g-recaptcha" data-sitekey="6Ld7mtkUAAAAAEO-U0KZE4nMlu17ISe4sZvC-1UM" data-callback="recaptchaCallback"></div>
                      <input type="hidden" class="hiddenRecaptcha required" name="hiddenRecaptcha" id="hiddenRecaptcha">
                      <span id="recaptcha-error" style="display:none; color: red;font-size: 14px;">The CAPTCHA field is required.</span>
                    </div>
                    <div class="contactbtn-wrap animate-btn blue">
                      <input style="" type="submit" id="consultFormSubmit" name="submit" value="Send Message" />
                      <span></span>
                    </div>

                    <div id="myElem" style="display:none">
                      <p>Thank you for contacting us.</p>
                    </div>


                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </main>


    <!--

    <script>
      ("body").mousemove(function(e) {

        parallaxIt(e, ".move-element", -5);

      });

      function parallaxIt(e, target, movement) {
        var $this = $("body");
        var relX = e.pageX - $this.offset().left;
        var relY = e.pageY - $this.offset().top;

        TweenMax.to(target, 1, {
          x: (relX - $this.width() * 1) / $this.width() * 10 * movement,
          y: (relY - $this.height() / 10) / $this.height() * movement + $this.height() * 1.5
        });
      }

    </script>
-->
    <script>
      function recaptchaCallback() {
        $('#hiddenRecaptcha').valid();
      };

    </script>

  </div>
</div>


<?php 
get_footer();
