<?php

/**
  Template Name: management-contact-form
*/


get_header();
?>
<?php

if (isset($_GET['pgid'])) {
    $mgmt_page_id = $_GET['pgid'];
    //// echo get_the_title($mgmt_page_id);
    //// echo get_field('designation',$mgmt_page_id);
    //get_field('list_data',$mgmt_page_id);
    $adv_email_id = get_field('advisors_email_id',$mgmt_page_id);
}
?>
<div id="barba-wrapper">
  <div class="barba-container contact" data-namespace="contact">
    <section class="about-section management_single">
      <div class="fl-container">

        <div class="breadcrumb_management">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:history.go(-1)">Go Back</a></li>
            <li class="breadcrumb-item active" aria-current="page">Contact <?php echo get_the_title($mgmt_page_id); ?></li>
          </ol>
        </div>
        <div>
          <div class="contact_form-management">
            <div class="contact-box mort-popup">

              <div class="popup_contnt">
                <h3>Contact <?php echo get_the_title($mgmt_page_id); ?></h3>
                <form id="client-form" novalidate="novalidate">
                  <input type="hidden" name="adv_email" value="<?php echo $adv_email_id ?>">
                  <input type="hidden" name="action" value="clientForm">
                  <div class="is-floating-label">
                    <label for="name">Name</label>
                    <input type="text" name="name" id="name">
                  </div>
                  <div class="is-floating-label">
                    <label for="email">Email</label>
                    <input type="text" name="email" id="email">
                  </div>
                  <div class="is-floating-label">
                    <label for="contact">Phone</label>
                    <input type="text" name="contact" id="contact">
                  </div>


                  <div class="is-floating-label">
                    <select name="query" id="query">
                      <option value="0" disabled selected>Nature of Query</option>
                      <option value="Life Insurance">Life Insurance</option>
                      <option value="Serious Illness Cover">Serious Illness Cover</option>
                      <option value="Income Protection">Income Protection</option>
                      <option value="Pension">Pension</option>
                      <option value="Investment">Investment</option>
                      <option value="Mortgage">Mortgage</option>
                      <option value="Other">Other</option>
                    </select>
                  </div>


                  <div class="is-floating-label textarea">
                    <label for="comments">Message </label>
                    <textarea rows="7" cols="50" name="comments"></textarea>
                  </div>

                  <div class="contactbtn-wrap animate-btn blue">
                    <input style="" type="submit" id="" name="submit" value="Send Message" />
                    <span></span>
                  </div>
                  <div id="myElem" style="display:none">
                    <p>Thank you for contacting us.</p>
                  </div>
                </form>

              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
</div>



<?php get_footer(); ?>
