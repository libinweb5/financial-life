<?php

/**
  Template Name: serious-illness-q-s
*/


get_header();
?>

<div id="barba-wrapper">
  <div class="barba-container investment" data-namespace="investment">
    <section class="banner mortgage-protection-banner testimonial_banner" style="background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/images/blue_banner.png);">
      <div class="fl-container">
        <div class="inner_banner_contnt">
          <div class="banner_data">
            <h1><?php the_title ();?></h1>
          </div>
        </div>
      </div>
    </section>


    <section class="serious-illness-q-s">
      <div class="container">
        <div class="serious-illness_wrap" id="qasection">
          <ul>
            <?php if( have_rows('serious_illness_question_answer') ): ?>
            <?php while( have_rows('serious_illness_question_answer') ): the_row(); ?>
            <li>
              <h4><?php the_sub_field('serious_illness_question');?></h4>
              <p><?php the_sub_field('serious_illness_answer');?></p>
            </li>
            <?php endwhile; ?>
            <?php endif; ?>
          </ul>

        </div>
      </div>
    </section>
  </div>
</div>



<?php
get_footer();
