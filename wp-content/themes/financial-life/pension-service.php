<?php

/**
  Template Name: pension-service
*/

get_header();
?>

<div id="barba-wrapper">
  <div class="barba-container mortgage-protection" data-namespace="pension">
    <main class="pension_overlay">
      <section class="banner mortgage-protection-banner pension_banner" style="background-image: url(<?php the_field('pension_service_banner_bg_img');?>);">
        <div class="fl-container">
          <div class="pension_banner_contnt">
            <div class="pension_cntnt">
              <h1><?php the_field('pension_service_banner_title');?></h1>
              <p><b><?php the_field('pension_service_banner_contents');?></b></p>
              <div class="pension_btn">
                <a class="animate-btn blue" href="<?php the_field('pension_button_link');?>"><?php the_field('pension_button_text');?><span></span></a>
              </div>
            </div>
            <div class="pension_img">
              <img src="<?php the_field('pension_service_banner_img');?>">
            </div>
          </div>
        </div>
      </section>
    </main>

    <section class="pension_service">
      <div class="fl-container">
        <h4><?php the_field('pension_services_main_title');?> </h4>
        <p><?php the_field('pension_services_content_text');?> </p>

        <div class="types_pension">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/about_bg.svg">
          <div class="pension_listwrap">
            <?php if( have_rows('pension_services_list')): ?>
            <?php while( have_rows('pension_services_list')): the_row(); ?>
            <div class="pension_blk">
              <div class="pension_head">
                <img src="<?php the_sub_field('pension_services_icon');?>">
                <h4><?php the_sub_field('pension_services_tilte');?></h4>
                <span></span>
              </div>
              <div class="pension_details">
                <h4><?php the_sub_field('pension_services_tilte');?></h4>
                <p><?php the_sub_field('pension_services_details');?></p>
              </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>

          </div>
        </div>
      </div>
    </section>

    <section class="pension_ftr">
      <div class="fl-container">
        <div class="pension_ftr_wrpr">
          <div class="blue_blk">
            <h4><?php the_field('pension_calculator_caption');?></h4>
            <a class="animate-btn" href="<?php the_field('pension_calculator_button_link');?>"><?php the_field('pension_calculator_button_title');?><span></span></a>
          </div>
          <div class="theme_blue_blk">
            <h4><?php the_field('pension_form_caption');?></h4>
            <a class="animate-btn" href="<?php the_field('pension_form_button_link');?>"><?php the_field('pension_form_button_title');?><span></span></a>
          </div>
        </div>
      </div>
    </section>
  </div>
</div>

<?php
get_footer();
