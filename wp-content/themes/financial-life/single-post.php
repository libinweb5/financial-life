<?php
/* Template for displaying single blog */
get_header();

?>


<div id="barba-wrapper">
  <div class="barba-container blog-details" data-namespace="blog-details">
    <?php

      $latest_post = array(get_the_ID());
      $blog_featured = get_the_post_thumbnail_url(get_the_id(),'blog_featured');
      $content = wpautop( $post->post_content );
      $categories_p = get_the_terms(get_the_ID(), 'category');
      $post_date = get_the_date( 'F j, Y' );
      $cats_res=array();
          foreach($categories_p as $catp){
              array_push($cats_res,$catp->slug); 
          }
       ?>
    <section class="banner blog-details-banner" style="background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/images/blue_bg.png); background-repeat: no-repeat;">
      <div class="fl-container">
        <div class="inner_banner_contnt">
        </div>
      </div>
    </section>
    <section class="blog-detail-content">
      <div class="container">
        <div class="row">
          <article class="col-lg-9 mx-auto">
           <div class="single_img-banner"><img src="<?= $blog_featured ?>"></div>
            <div class="breadcrumb-nav">
              <p><span><?php echo $categories_p[0]->name; ?></span><span><?= $post_date;?></span></p>
            </div>
            <h3><?php the_title(); ?></h3>
            <div class="blog_detail_content">
              <?php the_content(); ?>
            </div>
          </article>
        </div>
      </div>
    </section>

    <!-- Latest blog section start -->
    <section class="latest-blog">
      <div class="container">
        <div class="row">
          <div class="col-lg-6">
            <hr>
            <h4>Latest Blog</h4>
          </div>
          <div class="col-lg-6 text-right">
            <a class="flex-vcenter more-blog" href="<?php echo get_page_link(607); ?>"><span><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/more-blog-arrow.svg"></span>More Blog</a>
          </div>
          <div class="blog_section">
            <div class="fl-container">

              <div class="blog_wrpr">
                <?php
                  $queried_object = get_queried_object();
                   $args = array(
                       'post_type' => 'post',
                       'posts_per_page' => '3',
                       'post__not_in' => array($queried_object->ID),
                   );
                  $post_query = new WP_Query($args);
                    if($post_query->have_posts() ) {
                      while($post_query->have_posts() ) {                        
                        $post_query->the_post();
                        $categories_p = get_the_terms(get_the_ID(), 'category');
                        $post_date = get_the_date( 'F j, Y' );
                        $max_num_pages= $post_query->max_num_pages;
                        $found_posts= $post_query->found_posts;
                        ?>

                      <a href="<?php the_permalink(); ?>" class="blog_blk <?php echo $categories_p[0]->name; ?>">
                        <?php if (has_post_thumbnail( $post->ID ) ): ?>
                        <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
                        <img src="<?php echo $image[0]; ?>" alt="">
                        <?php endif; ?>
                        <?php
                  $category_detail=get_the_category('4');//$post->ID
                  foreach($category_detail as $cd){
                  echo $cd->cat_name;
                  }
                        ?>
                        <p class="blog_title"><span><?php echo $categories_p[0]->name; ?></span><span><?= $post_date;?></span></p>
                        <h4><?php the_title(); ?></h4>
                      </a>
                      <?php
                      }
                    }
                  ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Latest blog section start -->

  </div>
</div>


<?php get_footer(); ?>
