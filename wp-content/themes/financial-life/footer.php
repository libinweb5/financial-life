<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage financial-life
 * @since 1.0.0
 */

?>

<footer class="footer fl-container">
  <div class="ftr_logo_wrpr d-flex flex-vcenter flex-hcenter">
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/f_logo.svg">
  </div>
  <div class="ftr_nav">
    <div class="nav_blks">
      <h4>Insurance Products</h4>
      <?php
         wp_nav_menu( array( 'theme_location' => 'insurance-products', 'container' => '' ) );      
         wp_reset_query();
      ?>
    </div>
    <div class="nav_blks">
      <h4>Pension</h4>
      <?php
         wp_nav_menu( array( 'theme_location' => 'pension', 'container' => '' ) );
         wp_reset_query();
      ?>
    </div>
    <div class="nav_blks">
      <h4>Company</h4>
      <?php
         wp_nav_menu( array( 'theme_location' => 'company', 'container' => '' ) );
         wp_reset_query();
      ?>
    </div>
    <div class="nav_blks">
      <h4>Get Help</h4>
      <?php
         wp_nav_menu( array( 'theme_location' => 'gethelp', 'container' => '' ) );
         wp_reset_query();
      ?>
    </div>
  </div>

  <div class="ftr_contnt">
    <p><?php the_field('footer_content', 'option');?></p>
  </div>
  <div class="ftr_dwn">
    <p><?php the_field('rights_reserved', 'option');?></p>

    <div class="socil_wrpr">
      <?php if( have_rows('social_media_icons', 'option') ): ?>
      <?php while( have_rows('social_media_icons', 'option') ): the_row(); ?>
      <a href="<?php the_sub_field('social_links');?>" target="_blank">
        <img src="<?php the_sub_field('social_icon');?>" alt="">
      </a>
      <?php endwhile; ?>
      <?php endif; ?>
    </div>
  </div>
  </div>

</footer>
<div id="Mask"></div>
<!-- <script src="assets/js/jquery.js"></script> -->
<!--
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="anonymous"></script>
-->
<!--<script src="https://www.google.com/recaptcha/api.js" async defer></script>-->


<script async type="text/javascript">
  WebFontConfig = {
    google: {
      families: ['Archivo:400,400i,500,500i,600,600i,700']
    }
  };
  (function() {
    var wf = document.createElement('script');
    wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
      '://ajax.googleapis.com/ajax/libs/webfont/1.5.18/webfont.js';
    wf.type = 'text/javascript';
    wf.async = 'true';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(wf, s);
  })();

</script>
<?php wp_footer(); ?>
</body>

</html>
