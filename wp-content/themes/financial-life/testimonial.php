<?php

/**
  Template Name: testimonial
*/


get_header();
?>




<div id="barba-wrapper">
  <div class="barba-container testimonial" data-namespace="testimonial">
    <section class="banner mortgage-protection-banner testimonial_banner" style="background-image: url(<?php the_field('inner_banner_bg');?>);">
      <div class="fl-container">
        <div class="inner_banner_contnt">
          <div class="banner_data">
            <h1><?php the_field('inner_banner_content');?></h1>
            <!--<p><?php the_field('inner_banner_sub_content');?></p>-->
          </div>
        </div>
      </div>
    </section>

    <section class="testimonials">
      <div class="fl-container">
        <div class="testi_wrpr">
          <?php if( have_rows('testimonials')): ?>
          <?php while( have_rows('testimonials')): the_row(); ?>
          <div class="testi_blks">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/comma.svg">
            <p><?php the_sub_field('client_comments');?></p>
            <div class="img_wrpr">
              <div class="userimg_wrap">
                <img src="<?php the_sub_field('client_image');?>">
              </div>

              <div class="details">
                <h4><?php the_sub_field('client_name');?></h4>
                <p><?php the_sub_field('client_designation');?></p>
                <img class="rating" src="http://demo.webandcrafts.com/wp-financial-life/wp-content/themes/financial-life/assets/images/unnamed.png">
              </div>
            </div>
          </div>
          <?php endwhile; ?>
          <?php endif; ?>
        </div>
      </div>
    </section>

  </div>
</div>


<?php
get_footer();
