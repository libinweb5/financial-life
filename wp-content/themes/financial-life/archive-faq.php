<?php

/**
  Template Name: faq
*/

get_header();
?>

<div id="barba-wrapper">
  <div class="barba-container faq" data-namespace="faq">
    <section class="banner mortgage-protection-banner testimonial_banner faq_banner" style="background-image: url(<?php the_field('faq_banner_bg_imgs', 108);?>); background-color:#2034b5;">
      <div class="fl-container">
        <div class="inner_banner_contnt">
          <div class="banner_data">
            <h1><?php the_field('faq_banner_heading', 108);?></h1>
            <form id="find_faq" method="POST">
              <div class="form-group">
                <input name="faqname" id="faqname" class="required" type="text" placeholder="Search for a question…" required />
                <!-- <input class="btn_web_fill submit-btn alumni-submit submit" type="submit" value="Find" />-->
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>

    <section class="faq_tabs">
      <div class="fl-container">
        <div class="tabgroup">
          <div class="achiev-view panel" id="achieve-view"></div>
          <?php
                    $taxonomy     = 'faqcategory';
                    $orderby      = 'name';  
                    $args = array(
                        'taxonomy'     => $taxonomy,
                        'orderby'      => $orderby,
                    );
                    ?>
          <?php
                    $all_categories = get_categories($args);

                    foreach ($all_categories as $cat) {

                        $taxonomy = $cat->slug;
                        $orderby = 'name';
                        $args = array(
                            'taxonomy' => $taxonomy,
                            'orderby' => $orderby,
                        );
                        $category = get_queried_object();
                        $categort_slug = $cat->slug;
                        $categort_name = $cat->name;
                        $active_class = ($categort_slug == 'general-faq') ? 'active' : '';
                    
              ?>

          <li class="tab faq-tab-holder <?=$active_class?>" id="<?= $categort_slug ?>">
            <p>Invest, Portages etc</p>
            <h4><?= $categort_name ?></h4>
          </li>
          <?php
                        $i++;
                    }
                    ?>
                    
          <div class="panel panel--1" id="loadpanel">
            <?php
                $args = array(
                  'post_type' => 'faq', 
                );
                $events = new WP_Query( $args );

                if( $events->have_posts() ) {
                  while( $events->have_posts() ) {
                    $events->the_post();
                    $categories_p = get_the_terms(get_the_ID(), 'faqcategory');
                    $active_block = ($categories_p[0]->slug == 'general-faq') ? 'block' : 'none';
                    ?>
            <div class="faq_blk <?php echo $categories_p[0]->slug; ?>" style="display:<?= $active_block ?>">
              <div class="faq_ques">

                <p><?php the_title() ?></p>
                <span></span>
              </div>
              <div class="faq_ans">
                <p><?php the_content() ?></p>
              </div>
            </div>

            <?php
                  }
                } else {
                  echo 'No events!';
                }
              ?>
          </div>
        </div>
      </div>
    </section>

    <section class="home-section home_contact faq_contact ">
      <div class="fl-container">
        <div class="home_contact_main_wrpr">
          <h3><?php the_field('start_financial_life', 108);?></h3>
          <div class="home_contact_wrpr">
            <div class="online_wrpr">
              <img src="<?php the_field('online_icon', 108);?>">
              <h4><?php the_field('caption', 108);?></h4>
              <p><?php the_field('online_paragraph_text', 108);?></p>
              <a href="<?php echo get_page_link( get_page_by_path( 'contact-us' ) ); ?>" class="animate-btn blue">Get Quote <span></span></a>
            </div>
            <div class="call_wrpr">
              <img src="<?php the_field('call_icon', 108);?>">
              <h4><?php the_field('call_caption', 108);?></h4>
              <p><?php the_field('call_paragraph_text', 108);?></p>
              <a class="phone-num animate-btn" href="tel: (01) 256 0430"><svg xmlns="http://www.w3.org/2000/svg" width="15.102" height="15.118" viewBox="0 0 15.102 15.118">
                  <path id="Path_5" data-name="Path 5" d="M1343.751-167.105a10.875,10.875,0,0,1-5.855-1.879,13.05,13.05,0,0,1-5.946-8.512,6.9,6.9,0,0,1,.116-3.276,3.119,3.119,0,0,1,.309-.728,1.218,1.218,0,0,1,1.64-.6,5.616,5.616,0,0,1,3.054,3.062,1.671,1.671,0,0,1-.5,1.976c-.082.076-.167.149-.25.223a.858.858,0,0,0-.2,1.314,11.439,11.439,0,0,0,3.259,3.554c.239.169.483.332.733.486.709.438.962.4,1.508-.211a1.854,1.854,0,0,1,2.608-.463,6.2,6.2,0,0,1,2.427,2.484,1.4,1.4,0,0,1-.806,2.185,3.9,3.9,0,0,1-.842.24C1344.534-167.174,1344.06-167.139,1343.751-167.105Z" transform="translate(-1331.807 182.223)" fill="#25A9DE" />
                </svg><?php the_field('contact_number', 'option'); ?><span></span></a>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
</div>


<?php
get_footer();
