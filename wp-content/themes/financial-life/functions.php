<?php
/**
 * financial life functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WordPress
 * @subpackage Lsi_World
 * @since 1.0.0
 */
 
 define('ADMINEMAIL', "sreelakshmi.webandcrafts@gmail.com");

if ( ! function_exists( 'financiallife_setup' ) ) :
	
	function financiallife_setup() {
		
		load_theme_textdomain( 'financiallife', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );		
		add_theme_support( 'title-tag' );
		add_theme_support( 'post-thumbnails' );
		set_post_thumbnail_size( 1568, 9999 );
	
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'script',
				'style',
			)
		);
		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );
		// Add support for Block Styles.
		add_theme_support( 'wp-block-styles' );
		// Add support for full and wide align images.
		add_theme_support( 'align-wide' );
		// Add support for editor styles.
		add_theme_support( 'editor-styles' );
		// Enqueue editor styles.
		add_editor_style( 'style-editor.css' );
		// Add support for responsive embedded content.
		add_theme_support( 'responsive-embeds' );
	}
endif;
add_action( 'after_setup_theme', 'financiallife_setup' );


function my_login_logo_one() {
?>
<style type="text/css">
  body.login div#login h1 a {
    background-image: url(<?php echo get_stylesheet_directory_uri();
    ?>/assets/images/logo-black.svg);
    height: 45px !important;
    background-size: contain !important;
    width: 100% !important;
  }

</style>
<?php
}

add_action('login_enqueue_scripts', 'my_login_logo_one');


function add_theme_scripts() {
    wp_enqueue_style('style', get_stylesheet_uri());
    
    wp_enqueue_style('main-css', get_template_directory_uri() . '/assets/css/main.min.css', array(), '1.1', 'all');
    wp_enqueue_style('financial-custom', get_template_directory_uri() . '/assets/css/financial-custom.css', array(), '1.1', 'all');
    wp_enqueue_style('jquery-ui', '//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css', array(), '1.1', 'all');
}

add_action('wp_enqueue_scripts', 'add_theme_scripts');


add_action('wp_enqueue_scripts', 'my_script_holder');

function my_script_holder() {

	  wp_deregister_script('jquery');
	  // Change the URL if you want to load a local copy of jQuery from your own server.
	  wp_register_script('jquery', "https://code.jquery.com/jquery-3.1.1.min.js", array(), '3.1.1', true, false);
    wp_register_script('barba_script', get_template_directory_uri() . '/assets/js/barba.min.js', array('jquery'), '', true);
    wp_register_script('bootstrap_script', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js', array('jquery'), '', true);
    wp_register_script('slick_script', get_template_directory_uri() . '/assets/js/vendors/slick.min.js', array('jquery'), '', true);
    wp_register_script('selectric_script', get_template_directory_uri() . '/assets/js/jquery.selectric.min.js', array('jquery'), '', true);
    wp_register_script('anime_script', 'https://cdnjs.cloudflare.com/ajax/libs/animejs/2.0.2/anime.min.js', array('jquery'), '', true);
    wp_register_script('jqueryui_script', 'https://code.jquery.com/ui/1.12.1/jquery-ui.js', array('jquery'), '', true);
    wp_register_script('tweenmax_script', get_template_directory_uri() . '/assets/js/vendors/TweenMax.min.js', array('jquery'), '', true);
    wp_register_script('scrollmagic_script', get_template_directory_uri() . '/assets/js/vendors/ScrollMagic.js', array('jquery'), '', true);
    wp_register_script('animation_gsap_script', get_template_directory_uri() . '/assets/js/vendors/animation.gsap.min.js', array('jquery'), '', true);
    wp_register_script('animation_script', get_template_directory_uri() . '/assets/js/vendors/animation.js', array('jquery'), '', true);
    wp_register_script('scroll_script', get_template_directory_uri() . '/assets/js/vendors/scroll.js', array('jquery'), '', true);
    wp_register_script('validate_script', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.14.0/jquery.validate.min.js', array('jquery'), '', true);
    wp_register_script('main_script', get_template_directory_uri() . '/assets/js/main.js', array('jquery'), '', true);
    wp_register_script('financial-custom_script', get_template_directory_uri() . '/assets/js/financial-custom.js', array('jquery'), '', true);
    wp_register_script('validationform_script', get_template_directory_uri() . '/assets/js/validationform.js', array('jquery'), '', true);
  
    wp_enqueue_script('jquery');
    wp_enqueue_script('barba_script');
    wp_enqueue_script('bootstrap_script');
    wp_enqueue_script('slick_script');
    wp_enqueue_script('selectric_script');
    wp_enqueue_script('anime_script');
    wp_enqueue_script('jqueryui_script');
    wp_enqueue_script('tweenmax_script');
    wp_enqueue_script('scrollmagic_script');
    wp_enqueue_script('animation_gsap_script');
    wp_enqueue_script('animation_script');
    wp_enqueue_script('scroll_script');
    wp_enqueue_script('validate_script');
    wp_enqueue_script('main_script');
    wp_enqueue_script('financial-custom_script');
    wp_enqueue_script('validationform_script');
    
  
     // Localize the script with new data
         $args = array(
             'siteUrl' => home_url(),
             'themeUrl' => get_template_directory_uri(),
             'ajaxurl' => admin_url( 'admin-ajax.php' ),
         );
         wp_localize_script( 'financial-custom_script', 'ajaxObj', $args );

    }

    add_action('wp_footer', 'wpb_hook_javascript');


//General Options

if (function_exists('acf_add_options_page')) {

acf_add_options_page(array(
'page_title' => 'Theme General Settings',
'menu_title' => 'General Options',
'menu_slug' => 'theme-general-settings',
'capability' => 'edit_posts',
'redirect' => false
));

acf_add_options_sub_page(array(
'page_title' => 'Theme Header Settings',
'menu_title' => 'Header',
'parent_slug' => 'theme-general-settings',
));

acf_add_options_sub_page(array(
'page_title' => 'Theme Footer Settings',
'menu_title' => 'Footer',
'parent_slug' => 'theme-general-settings',
));
}

// Menu registration

function register_my_menus() {
register_nav_menus(
array(
'main-menu' => __('Main Menu'),
'insurance-products' => __('Insurance Products'),
'pension' => __('Pension'),
'company' => __('Company'),
'gethelp' => __('Get Help')
)
);
}

add_action('init', 'register_my_menus');

/**
* Adding extended support for the uploads format
*/
function cc_mime_types($mimes) {
   $mimes['svg'] = 'image/svg+xml';
   return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

define( 'ALLOW_UNFILTERED_UPLOADS', true );


function webp_upload_mimes( $existing_mimes ) {
	// add webp to the list of mime types
	$existing_mimes['webp'] = 'image/webp';

	// return the array back to the function with our added mime type
	return $existing_mimes;
}
add_filter( 'mime_types', 'webp_upload_mimes' );

// Application form

function create_posttype() {

        /**add FAQ post here */

        $labels = array(
            'name'               => _x( 'Faq', 'post type general name', 'your-plugin-textdomain' ),
            'singular_name'      => _x( 'Faq', 'post type singular name', 'your-plugin-textdomain' ),
            'menu_name'          => _x( 'Faq', 'admin menu', 'your-plugin-textdomain' ),
            'name_admin_bar'     => _x( 'Faq', 'add new on admin bar', 'your-plugin-textdomain' ),
            'add_new'            => _x( 'Add Faq', 'book', 'your-plugin-textdomain' ),
            'add_new_item'       => __( 'Add New Faq', 'your-plugin-textdomain' ),
            'new_item'           => __( 'Faq', 'your-plugin-textdomain' ),
            'edit_item'          => __( 'Edit Faq', 'your-plugin-textdomain' ),
            'view_item'          => __( 'View Faq', 'your-plugin-textdomain' ),
            'all_items'          => __( 'All Faq', 'your-plugin-textdomain' ),
            'search_items'       => __( 'Search Faq', 'your-plugin-textdomain' ),
            'parent_item_colon'  => __( 'Parent Faq:', 'your-plugin-textdomain' ),
            'not_found'          => __( 'No Faq found.', 'your-plugin-textdomain' ),
            'not_found_in_trash' => __( 'No Faq found in Trash.', 'your-plugin-textdomain' )
        );

        $args = array(
            'labels'             => $labels,
            'description'        => __( 'Description.', 'your-plugin-textdomain' ),
            'public'             => true,
            'publicly_queryable' => true,
            'show_ui'            => true,
            'show_in_menu'       => true,
            'query_var'          => true,
            'rewrite'            => array( 'slug' => 'faq' ),
            'capability_type'    => 'post',
            'has_archive'        => true,
            'hierarchical'       => false,
            'menu_position'      => null,
            'menu_icon'           => 'dashicons-clipboard',
            'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
        );

        register_post_type( 'faq', $args );
  
       /*     * ***************Taxonamy******************* */
        $labels = array(
            'name' => _x('Faq Category', 'taxonomy general name', 'textdomain'),
            'singular_name' => _x('Faq Category', 'taxonomy singular name', 'textdomain'),
            'search_items' => __('Search Category', 'textdomain'),
            'popular_items' => __('Popular Category', 'textdomain'),
            'all_items' => __('All Category', 'textdomain'),
            'parent_item' => null,
            'parent_item_colon' => null,
            'edit_item' => __('Edit Category', 'textdomain'),
            'update_item' => __('Update Category', 'textdomain'),
            'add_new_item' => __('Add New Category', 'textdomain'),
            'new_item_name' => __('New Category', 'textdomain'),
            'separate_items_with_commas' => __('Separate Package Type with commas', 'textdomain'),
            'add_or_remove_items' => __('Add or remove Category', 'textdomain'),
            'choose_from_most_used' => __('Choose from the most used Category', 'textdomain'),
            'not_found' => __('No Category found.', 'textdomain'),
            'menu_name' => __('Add Category', 'textdomain'),
        );

        $args = array(
            'hierarchical' => true,
            'labels' => $labels,
            'show_ui' => true,
            'show_admin_column' => true,
            'query_var' => true,
            'rewrite' => array('slug' => 'faq'),
        );

        register_taxonomy('faqcategory', 'faq', $args);
  
  
          /**add Team post here */

        $labels = array(
            'name'               => _x( 'Management', 'post type general name', 'your-plugin-textdomain' ),
            'singular_name'      => _x( 'Management', 'post type singular name', 'your-plugin-textdomain' ),
            'menu_name'          => _x( 'Management', 'admin menu', 'your-plugin-textdomain' ),
            'name_admin_bar'     => _x( 'Management', 'add new on admin bar', 'your-plugin-textdomain' ),
            'add_new'            => _x( 'Add Management', 'book', 'your-plugin-textdomain' ),
            'add_new_item'       => __( 'Add New Management', 'your-plugin-textdomain' ),
            'new_item'           => __( 'Management', 'your-plugin-textdomain' ),
            'edit_item'          => __( 'Edit Management', 'your-plugin-textdomain' ),
            'view_item'          => __( 'View Management', 'your-plugin-textdomain' ),
            'all_items'          => __( 'All Management', 'your-plugin-textdomain' ),
            'search_items'       => __( 'Search Management', 'your-plugin-textdomain' ),
            'parent_item_colon'  => __( 'Parent Management:', 'your-plugin-textdomain' ),
            'not_found'          => __( 'No Management found.', 'your-plugin-textdomain' ),
            'not_found_in_trash' => __( 'No Management found in Trash.', 'your-plugin-textdomain' )
        );

        $args = array(
            'labels'             => $labels,
            'description'        => __( 'Description.', 'your-plugin-textdomain' ),
            'public'             => true,
            'publicly_queryable' => true,
            'show_ui'            => true,
            'show_in_menu'       => true,
            'query_var'          => true,
            'rewrite'            => array( 'slug' => 'management' ),
            'capability_type'    => 'post',
            'has_archive'        => true,
            'hierarchical'       => false,
            'menu_position'      => null,
            'menu_icon'           => 'dashicons-clipboard',
            'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
        );

        register_post_type( 'management', $args );
  
    }


// Hooking up our function to theme setup
add_action( 'init', 'create_posttype' );



//FAQ Search

function get_faq_details(){
   $args = array("post_type" => "faq", "s" => $_POST['faqname'],

);
$res_html='';
$my_query = new WP_Query($args);


if($my_query->have_posts()):

   while ($my_query->have_posts()):

   $my_query->the_post();
    
   $faq_name= get_the_title();
   $faq_conent= get_the_content();
   
   $year=get_terms( $post->ID, "alumni_year");

  $res_html .= '<div class="faq_blk general-faq active">';
  $res_html .= '<div class="faq_ques">';

  $res_html .= '<p>' . $faq_name . '</p>';
  $res_html .= '<span></span>';
  $res_html .= '</div>';
  $res_html .= '<div class="faq_ans">';
  $res_html .= '<p>'.$faq_conent.'</p>';
  $res_html .= '</div>';
  $res_html .= '</div>';

     endwhile; endif;
     echo $res_html;
     exit();
}

add_action( 'wp_ajax_get_faq_details', 'get_faq_details' );
add_action( 'wp_ajax_nopriv_get_faq_details', 'get_faq_details' );






/* Client form mail send to Advisor */

if(!function_exists("clientForm")) {
    function clientForm() {
        $name        = $_POST['name'];
        $email       = $_POST['email'];
        $contact     = $_POST['contact'];
        $query       = $_POST['query'];
        $comments    = $_POST['comments'];
        $adv_email    = $_POST['adv_email'];

        // More headers
        $headers[] = "MIME-Version: 1.0" . "\r\n";
        $headers[] = 'From: Financial Life <noreply@financiallife.com>' . "\r\n";
        $headers[] = 'Content-Type: text/html; charset=UTF-8';

        $to = "$adv_email";
        $subject ="Financial Life Client";

        $message = '
        <!DOCTYPE html><html lang="en" xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">

            <meta name="viewport" content="width=device-width">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="x-apple-disable-message-reformatting">
            <title>Financial Life</title>
            <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&display=swap" rel="stylesheet">
        </head>
        <style>
            table, tr, td{
                border: none;
                margin: 0px;
                box-sizing: border-box;
                padding: 0px;
                border-collapse: collapse;
            }
            img + div { display:none; }
        </style>
        <body width="100%" style="margin: 0; padding: 0 !important; mso-line-height-rule: exactly; background-color: #fff;font-family: Poppins, sans-serif;">
        <center style="width: 100%;">
          <table id="main-table" style="width:100%;max-width: 700px; min-width: 700px; background-color: #fff;border-collapse: collapse;border: none;margin: 0px;box-sizing: border-box;padding: 0px;color:#000;">
              <tr style="background-color:#fff;">
                  <td>
                    <table style="width: 100%;">
                        <tr>
                            <td align="left" style="padding: 18px 0 18px 34px;">
                                <a href="http://demo.webandcrafts.com/wp-financial-life" title="spirae" style="cursor: pointer;" target="_blank">
                                    <img src="http://demo.webandcrafts.com/wp-financial-life/wp-content/themes/financial-life/assets/images/logopng.png" alt="Financial Life Logo" title="Financial Life Logo">
                                </a
                            </td>
                        </tr>
                    </table>
                  </td>
              </tr>
              <tr>
                  <td>
                      <table style="width: 100%;">
                        <tr>
                            <td>
                              <table cellpadding="0" cellspacing="0"
                                style="text-align:center;font-family:Verdana,Arial;font-weight:normal;">
                                <tbody>
                                  <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb;">Name:
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;"> '.$name.'</td>
                                  </tr>
                                  <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Email:
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$email.'</td>
                                  </tr>
                                  <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Contact:
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$contact .'</td>
                                  </tr>
                                  <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Protection:
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$query.'</td>
                                  </tr>
                                  <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Massage:
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$comments.'</td>
                                  </tr>

                                </tbody>
                              </table>
                            </td>
                        </tr>
                      </table>
                  </td>
              </tr>
          </table>
        </center>
        </body></html>';
        // $mailResult = false;
        // $mailResult = wp_mail( $to, $subject, $message );
         //echo $mailResult;
        // exit;
        if(wp_mail($to, $subject, $message, $headers )) {
          wp_mail( $employee_emails, $subject, $message );

            echo 1;
            //echo $to;
          } else {
            echo 0;
          }exit;
    }
}

add_action('wp_ajax_clientForm', 'clientForm');
add_action('wp_ajax_nopriv_clientForm', 'clientForm');







/* Contact form */

if(!function_exists("consultForm")) {
    function consultForm() {
        $name        = $_POST['name'];
        $email       = $_POST['email'];
        $contact     = $_POST['contact'];
        $protection  = $_POST['protection'];
        $massage     = $_POST['massage'];

        // More headers
        $headers[] = "MIME-Version: 1.0" . "\r\n";
        $headers[] = 'From: Financial Life <noreply@financiallife.com>' . "\r\n";
        $headers[] = 'Content-Type: text/html; charset=UTF-8';

        $to = "libin.webandcrafts@gmail.com";
        $to = "$email";
        $subject = "Financial Life Contact ";

        $message = '
        <!DOCTYPE html><html lang="en" xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">

            <meta name="viewport" content="width=device-width">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="x-apple-disable-message-reformatting">
            <title>Financial Life</title>
            <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&display=swap" rel="stylesheet">
        </head>
        <style>
            table, tr, td{
                border: none;
                margin: 0px;
                box-sizing: border-box;
                padding: 0px;
                border-collapse: collapse;
            }
            img + div { display:none; }
        </style>
        <body width="100%" style="margin: 0; padding: 0 !important; mso-line-height-rule: exactly; background-color: #fff;font-family: Poppins, sans-serif;">
        <center style="width: 100%;">
          <table id="main-table" style="width:100%;max-width: 700px; min-width: 700px; background-color: #fff;border-collapse: collapse;border: none;margin: 0px;box-sizing: border-box;padding: 0px;color:#000;">
              <tr style="background-color:#fff;">
                  <td>
                    <table style="width: 100%;">
                        <tr>
                            <td align="left" style="padding: 18px 0 18px 34px;">
                                <a href="http://demo.webandcrafts.com/wp-financial-life" title="spirae" style="cursor: pointer;" target="_blank">
                                    <img src="http://demo.webandcrafts.com/wp-financial-life/wp-content/themes/financial-life/assets/images/logopng.png" alt="Financial Life Logo" title="Financial Life Logo">
                                </a
                            </td>
                        </tr>
                    </table>
                  </td>
              </tr>
              <tr>
                  <td>
                      <table style="width: 100%;">
                        <tr>
                            <td>
                              <table cellpadding="0" cellspacing="0"
                                style="text-align:center;font-family:Verdana,Arial;font-weight:normal;">
                                <tbody>
                                  <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb;">Name:
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;"> '.$name.'</td>
                                  </tr>
                                  <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Email:
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$email.'</td>
                                  </tr>
                                  <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Contact:
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$contact .'</td>
                                  </tr>
                                  <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Protection:
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$protection.'</td>
                                  </tr>
                                  <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Massage:
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$massage.'</td>
                                  </tr>

                                </tbody>
                              </table>
                            </td>
                        </tr>
                      </table>
                  </td>
              </tr>
          </table>
        </center>
        </body></html>';
        // $mailResult = false;
        // $mailResult = wp_mail( $to, $subject, $message );
         //echo $mailResult;
        // exit;
        if(wp_mail($to, $subject, $message, $headers )) {
            echo 1;
            // echo $to;
          } else {
            echo 0;
          }exit;
    }
}

add_action('wp_ajax_consultForm', 'consultForm');
add_action('wp_ajax_nopriv_consultForm', 'consultForm');





/* Mortgage Protection cover Form */

if(!function_exists("mortgageprotectionForm")) {
    function mortgageprotectionForm() {
        
        global $wpdb;
        $name             = $_POST['name'];
        $email            = $_POST['email'];
        $contact          = $_POST['contact'];
        $termcover        = $_POST['termcover'];
        $lifecover        = $_POST['lifecover']; 
        $protection_type  = $_POST['protection_type']; 
        $radiorelation    = $_POST['radiorelation']; 
        $gender1a         = $_POST['gender1a']; 
        $radiosmoker      = $_POST['radiosmoker'];
        $day              = $_POST['day'];
        $month            = $_POST['month'];
        $years            = $_POST['years'];
        $secday           = $_POST['secday'];
        $secmonth         = $_POST['secmonth'];
        $secyears         = $_POST['secyears'];
        $sec_gender1a     = $_POST['sec_gender1a'];
        $sec_radiosmoker  = $_POST['sec_radiosmoker'];
      
        $dob = $day.'-'.$month.'-'.$years;
        $secdob = $secday.'-'.$secmonth.'-'.$secyears;
        $apidob = $day.'/'.$month.'/'.$years;
        $apidob1 = '';
        
        if($radiorelation == 'Joint') {
            $apidob1 = $secday.'/'.$secmonth.'/'.$secyears;
        }
        
        $apismoker1 = ($radiosmoker == 1) ? 'Smoker' : 'Non-Smoker';
        $apismoker2 = ($sec_radiosmoker == 'Yes') ? 'Smoker' : 'Non-Smoker';
        
        $userdetails = '
        <p><span>Name: </span>'.$name.'</p>
        <p><span>Email: </span>'.$email.'</p>
        <p><span>Contact: </span>'.$contact.'</p>
        <p><span>Term Cover:</span>'.$termcover.'</p>
        <p><span>Life Cover:</span>'.$lifecover.'</p>
        <p><span>Protection Type:</span>'.$protection_type.'</p>
        <p><span>Single/Joint:</span>'.$radiorelation.'</p>
        <p><span>Gender:</span>'.$gender1a.'</p>
        <p><span>DOB:</span>'.$apidob.'</p>
        <p><span>Is Smoker?:</span>'.$apismoker1.'</p>';
        
        if($radiorelation == 'Joint') {
            $userdetails .= '
            <p><span>Second Person Gender:</span>'.$sec_gender1a.'</p>
            <p><span>Second Person DOB:</span>'.$apidob1.'</p>
            <p><span>Second Person is smoker?:</span>'.$apismoker2.'</p>';
        }
        
        $apidata = array(
            'dob1'    => $apidob,
            'dob2'    => $apidob1,
            'lc1'     => $lifecover,
            'lc2'     => $lifecover,
            'term'    => $termcover,
            'sex1'    => $gender1a,
            'sex2'    => $sec_gender1a,
            'smoker1' => $apismoker1,
            'smoker2' => $apismoker2
        );
        
        $plan_node = array(
            'qlo' => 'Y',
            'qla' => 'N',
            'qli' => 'N',
            'qio' => 'N'
        );
        
        $apiresult = call_best_advice_api($apidata,$plan_node);
        
        $html = '';
        
        foreach ($apiresult['Life Cover Only'] as $key => $val) {
            
            if($val !== '-' && $val !== 0) {
                $html .= "<tr><td>$key</td><td>$val</td></tr>";
            }
        }
        
        if($radiorelation == 'Joint') {
            $qry="INSERT INTO `wp_protection`"
               . " (`protection_name`,`amount`, `term_cover`, `type`, `name`, `contact_number`, `email`, `dob`, `gender`, `is_smoker`, `sec_dob`, `sec_gender`, `sec_is_smoker`)"
               . " VALUES ( '".$protection_type."', '".$lifecover."', '".$termcover."', '".$radiorelation."', '".$name."', '".$contact."', '".$email."', '".$dob."', '".$gender1a."', '".$radiosmoker."', '".$secdob."', '".$sec_gender1a."', '".$sec_radiosmoker."');";   
        } else {
            $qry="INSERT INTO `wp_protection`"
               . " (`protection_name`,`amount`, `term_cover`, `type`, `name`, `contact_number`, `email`, `dob`, `gender`, `is_smoker`)"
               . " VALUES ( '".$protection_type."', '".$lifecover."', '".$termcover."', '".$radiorelation."', '".$name."', '".$contact."', '".$email."', '".$dob."', '".$gender1a."', '".$radiosmoker."');";   
        }
       
       
        $wpdb->query($qry);
       
        // More headers
        $headers[] = "MIME-Version: 1.0" . "\r\n";
        $headers[] = 'From: Financial Life Insurance <noreply@financiallife.com>' . "\r\n";
        $headers[] = 'Content-Type: text/html; charset=UTF-8';

        $to = "$email";
        $subject = "Thank you for Quoting with Financial Life Insurance";

        $message = '
        <!DOCTYPE html><html lang="en" xmlns="http://www.w3.org/1999/xhtml"><head>
            <meta name="viewport" content="width=device-width">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="x-apple-disable-message-reformatting">
            <title>Financial Life</title>
            <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&display=swap" rel="stylesheet">
        </head>
        
        <body width="100%" style="margin: 0; padding: 0 !important; mso-line-height-rule: exactly; background-color: #fff;font-family: Poppins, sans-serif;">
        <div style="padding: 20px;font-size:14px;line-height:30px;width: 500px;text-align: center;border-radius: 6px; border: 1px solid #ccc;">
          <a href="http://demo.webandcrafts.com/wp-financial-life" title="financeiallife" style="cursor: pointer;" target="_blank">
            <img src="http://demo.webandcrafts.com/wp-financial-life/wp-content/themes/financial-life/assets/images/mail-logo.png" alt="Financial Life Logo" title="Financial Life Logo">
          </a><br>
          Hello '.$name.',<br>
          Thanks for getting a quick quote with Financial Life Insurance.<br>
          If you havent already applied online and you re still thinking about it,<br>
          We have summarised your personal quote below.<br>'.
        
        $message .= '
        
        <table style="margin-top: 20px; width: 100%;">
            <tr>
                <td style="border:1px solid silver;border-radius:5px;">
                    <table style="width:100%;">
                        <tr>
                            <td style="width:35%;"><u>Provider</td>
                            <td><u>Premium</td>
                        </tr>';
        $message .=    $html;
        $message .= '   <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table><br>';
          
        //   You can apply online and pick up where you left off:<br>
        //   <a href="http://demo.webandcrafts.com/wp-financial-life/thank-you/" title="financeiallife" style="cursor: pointer;" target="_blank">Quote Link</a><br>
          'Regards<br>
          Financial Life Insurance
        </div>
        </body></html>';
        
        $headers_admin[] = "MIME-Version: 1.0" . "\r\n";
        $headers_admin[] = 'From: '.$name.' <'.$email.'>' . "\r\n";
        $headers_admin[] = 'Content-Type: text/html; charset=UTF-8';
        
        $admsub = "Mortgage Protection Cover Request";
        
        
        $admmsg = '
        <!DOCTYPE html><html lang="en" xmlns="http://www.w3.org/1999/xhtml"><head>

            <meta name="viewport" content="width=device-width">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="x-apple-disable-message-reformatting">
            <title>Financial Life</title>
            <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&display=swap" rel="stylesheet">
        </head>
        <style>
            table, tr, td{
                border: none;
                margin: 0px;
                box-sizing: border-box;
                padding: 0px;
                border-collapse: collapse;
            }
            img + div { display:none; }
        </style>
        <body width="100%" style="margin: 0; padding: 0 !important; mso-line-height-rule: exactly; background-color: #fff;font-family: Poppins, sans-serif;">
        <center style="width: 100%;">
            Hello Admin,<br>
            New Life Insurance Cover Request details is as follows <br>
          <table id="main-table" style="width:100%;max-width: 700px; min-width: 700px; background-color: #fff;border-collapse: collapse;border: none;margin: 0px;box-sizing: border-box;padding: 0px;color:#000;">
              <tr style="background-color:#fff;">
                  <td>
                    <table style="width: 100%;">
                        <tr>
                            <td align="left" style="padding: 18px 0 18px 34px;">
                                <a href="http://demo.webandcrafts.com/wp-financial-life" title="spirae" style="cursor: pointer;" target="_blank">
                                    <img src="http://demo.webandcrafts.com/wp-financial-life/wp-content/themes/financial-life/assets/images/logopng.png" alt="Financial Life Logo" title="Financial Life Logo">
                                </a
                            </td>
                        </tr>
                    </table>
                  </td>
              </tr>
              <tr>
                  <td>
                      <table style="width: 100%;">
                        <tr>
                            <td>
                              <table cellpadding="0" cellspacing="0"
                                style="text-align:center;font-family:Verdana,Arial;font-weight:normal;">
                                <tbody>
                                  <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb;">Name:
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;"> '.$name.'</td>
                                  </tr>
                                  <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Email:
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$email.'</td>
                                  </tr>
                                  <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Contact:
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$contact .'</td>
                                  </tr>
                                  <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Amount:
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$lifecover.'</td>
                                  </tr>
                                  
                                   <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Term of Cover:
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$termcover.'</td>
                                  </tr>
                                   <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Protection Type:
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$protection_type.'</td>
                                  </tr>
                                   <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Single/Joint:
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$radiorelation.'</td>
                                  </tr>
                                  <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Date Of Birth:
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;text-align: center;">'.$apidob .'</td>
                                  </tr>
                                        <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Gender:
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;text-align: center;">'.$gender1a.'</td>
                                  </tr>
                                  <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Convertible Option:
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$convertiable.'</td>
                                  </tr>
                                  <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Smoker:
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$apismoker1.'</td>
                                  </tr>';
                            if($radiorelation == 'Joint') {
                                $admmsg .= '<tr>
                                                <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Second Person Gender:</th>
                                                <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$sec_gender1a.'</td>
                                            </tr>
                                            <tr>
                                                <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Second DateOf Birth:</th>
                                                <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$apidob1.'</td>
                                            </tr>
                                            <tr>
                                                <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Second DateOf Birth:</th>
                                                <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$apismoker2.'</td>
                                            </tr>';
                            } 
                                  
                      $admmsg .= '</tbody>
                                </table>
                            </td>
                        </tr>
                      </table>
                  </td>
              </tr>
            </table>';
            $admmsg .= '
        
            <table style="width:550px;">
                <tr>
                    <td style="border:1px solid silver;border-radius:5px;">
                        <table style="width:100%;">
                            <tr>
                                <td style="width:35%;"><u>Provider</td>
                                <td><u>Premium</td>
                            </tr>';
            $admmsg .=    $html;
            $admmsg .= '   <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>';
        $admmsg .= '</center>
            Regards<br>
            Financial Life Insurance
        </body></html>';

        wp_mail(ADMINEMAIL, $admsub, $admmsg, $headers_admin );
        
        $result = array();
        
        if(wp_mail($to, $subject, $message, $headers )) {
            $result['status'] = 1;
            $result['data'] = base64_encode(serialize($apiresult['Life Cover Only']));
            echo json_encode($result);
        } else {
            $result['status'] = 0;
            $result['data'] = base64_encode(serialize($apiresult['Life Cover Only']));
            echo json_encode($result);
        }
        
        exit;

    }
}

add_action('wp_ajax_mortgageprotectionForm', 'mortgageprotectionForm');
add_action('wp_ajax_nopriv_mortgageprotectionForm', 'mortgageprotectionForm');


/* Serious Illness Form */

if(!function_exists("seriousillnessForm")) {
    function seriousillnessForm() {
        
        global $wpdb;
        $name             = $_POST['name'];
        $email            = $_POST['email'];
        $contact          = $_POST['contact'];
        $termcover        = $_POST['termcover'];
        $lifecover        = $_POST['lifecover']; 
        $protection_type  = $_POST['protection_type']; 
        $radiorelation    = $_POST['radiorelation']; 
        $gender1a         = $_POST['gender1a']; 
        $radiosmoker      = $_POST['radiosmoker'];
        $day              = $_POST['day'];
        $month            = $_POST['month'];
        $years            = $_POST['years'];
        $secday           = $_POST['secday'];
        $secmonth         = $_POST['secmonth'];
        $secyears         = $_POST['secyears'];
        $sec_gender1a     = $_POST['sec_gender1a'];
        $sec_radiosmoker  = $_POST['sec_radiosmoker'];
      
      
        $dob = $day.'-'.$month.'-'.$years;
        $secdob = $secday.'-'.$secmonth.'-'.$secyears;
        $apidob = $day.'/'.$month.'/'.$years;
        $apidob1 = '';
        
        if($radiorelation == 'Joint') {
            $apidob1 = $secday.'/'.$secmonth.'/'.$secyears;
        }
        
        $apismoker1 = ($radiosmoker == 1) ? 'Smoker' : 'Non-Smoker';
        $apismoker2 = ($sec_radiosmoker == 'Yes') ? 'Smoker' : 'Non-Smoker';
        
        $userdetails = '
        <p><span>Name: </span>'.$name.'</p>
        <p><span>Email: </span>'.$email.'</p>
        <p><span>Contact: </span>'.$contact.'</p>
        <p><span>Term Cover:</span>'.$termcover.'</p>
        <p><span>Life Cover:</span>'.$lifecover.'</p>
        <p><span>Protection Type:</span>'.$protection_type.'</p>
        <p><span>Single/Joint:</span>'.$radiorelation.'</p>
        <p><span>Gender:</span>'.$gender1a.'</p>
        <p><span>DOB:</span>'.$apidob.'</p>
        <p><span>Is Smoker?:</span>'.$apismoker1.'</p>';
        
        if($radiorelation == 'Joint') {
            $userdetails .= '
            <p><span>Second Person Gender:</span>'.$sec_gender1a.'</p>
            <p><span>Second Person DOB:</span>'.$apidob1.'</p>
            <p><span>Second Person is smoker?:</span>'.$apismoker2.'</p>';
        }
        
        $apidata = array(
            'dob1'    => $apidob,
            'dob2'    => $apidob1,
            'lc1'     => $lifecover,
            'lc2'     => $lifecover,
            'term'    => $termcover,
            'sex1'    => $gender1a,
            'sex2'    => $sec_gender1a,
            'smoker1' => $apismoker1,
            'smoker2' => $apismoker2
        );
        
        $plan_node = array(
            'qlo' => 'N',
            'qla' => 'N',
            'qli' => 'Y',
            'qio' => 'N'
        );
        
        $apiresult = call_best_advice_api($apidata,$plan_node);
        
        $html = '';
        
        foreach ($apiresult['Life Cover Only'] as $key => $val) {
            
            if($val !== '-' && $val !== 0) {
                $html .= "<tr><td>$key</td><td>$val</td></tr>";
            }
        }
       
       
        if($radiorelation == 'Joint') {
            $qry="INSERT INTO `wp_protection`"
               . " (`protection_name`,`amount`, `term_cover`, `type`, `name`, `contact_number`, `email`, `dob`, `gender`, `is_smoker`, `sec_dob`, `sec_gender`, `sec_is_smoker`)"
               . " VALUES ( '".$protection_type."', '".$lifecover."', '".$termcover."', '".$radiorelation."', '".$name."', '".$contact."', '".$email."', '".$dob."', '".$gender1a."', '".$radiosmoker."', '".$secdob."', '".$sec_gender1a."', '".$sec_radiosmoker."');";   
        } else {
            $qry="INSERT INTO `wp_protection`"
               . " (`protection_name`,`amount`, `term_cover`, `type`, `name`, `contact_number`, `email`, `dob`, `gender`, `is_smoker`)"
               . " VALUES ( '".$protection_type."', '".$lifecover."', '".$termcover."', '".$radiorelation."', '".$name."', '".$contact."', '".$email."', '".$dob."', '".$gender1a."', '".$radiosmoker."');";   
        }
       
       
        $wpdb->query($qry);
      
        // More headers
        $headers[] = "MIME-Version: 1.0" . "\r\n";
        $headers[] = 'From: Financial Life Insurance <noreply@financiallife.com>' . "\r\n";
        $headers[] = 'Content-Type: text/html; charset=UTF-8';

        $to = "$email";
        $subject = "Thank you for Quoting with Financial Life Insurance";

        $message = '
        <!DOCTYPE html><html lang="en" xmlns="http://www.w3.org/1999/xhtml"><head>
            <meta name="viewport" content="width=device-width">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="x-apple-disable-message-reformatting">
            <title>Financial Life</title>
            <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&display=swap" rel="stylesheet">
        </head>
        
        <body width="100%" style="margin: 0; padding: 0 !important; mso-line-height-rule: exactly; background-color: #fff;font-family: Poppins, sans-serif;">
        <div style="background-color: #1dacec6b;padding: 20px;font-size:14px;line-height:30px;width: 500px;text-align: center;border-radius: 6px;">
          <a href="http://demo.webandcrafts.com/wp-financial-life" title="financeiallife" style="cursor: pointer;" target="_blank">
            <img src="http://demo.webandcrafts.com/wp-financial-life/wp-content/themes/financial-life/assets/images/mail-logo.png" alt="Financial Life Logo" title="Financial Life Logo">
          </a><br>
          Hello '.$name.',<br>
          Thanks for getting a quick quote with Financial Life Insurance.<br>
          If you havent already applied online and you re still thinking about it,<br>
          We have summarised your personal quote below.<br>
          You can apply online and pick up where you left off:<br>
          <a href="http://demo.webandcrafts.com/wp-financial-life" title="financeiallife" style="cursor: pointer;" target="_blank">Quote Link</a><br>
          Regards<br>
          Financial Life Insurance
        </div>
        </body></html>';
        // $mailResult = false;
        // $mailResult = wp_mail( $to, $subject, $message );
         //echo $mailResult;
        // exit;
        if(wp_mail($to, $subject, $message, $headers )) {
            echo 1;
            // echo $to;
        } else {
            echo 0;
        }
        exit;
    }
}

add_action('wp_ajax_seriousillnessForm', 'seriousillnessForm');
add_action('wp_ajax_nopriv_seriousillnessForm', 'seriousillnessForm');



/* Life Insurance cover Form */

if(!function_exists("insurancequote")) {
    function insurancequote() {
        global $wpdb;
        $name            = $_POST['name'];
        $email           = $_POST['email'];
        $contact         = $_POST['contact'];
        $termcover       = $_POST['termcover'];
        $lifecover       = $_POST['lifecover']; 
        $protection_type = $_POST['protection_type']; 
        $radiorelation   = $_POST['radiorelation']; 
        $gender1a        = $_POST['gender1a']; 
        $radiosmoker     = $_POST['radiosmoker'];
        $convertiable    = $_POST['convertiable']; 
        $day             = $_POST['day'];
        $month           = $_POST['month'];
        $years           = $_POST['years'];
        $sec_gender1a    = $_POST['sec_gender1a'];
        
        $secday           = $_POST['secday'];
        $secmonth         = $_POST['secmonth'];
        $secyears         = $_POST['secyears'];
        $sec_radiosmoker  = $_POST['sec_radiosmoker'];
      
        $apidob = $day.'/'.$month.'/'.$years;
        $apidob1 = '';
        if($radiorelation == 'Joint') {
            $apidob1 = $secday.'/'.$secmonth.'/'.$secyears;
        }
        
        $apismoker1 = ($radiosmoker == 1) ? 'Smoker' : 'Non-Smoker';
        $apismoker2 = ($sec_radiosmoker == 'Yes') ? 'Smoker' : 'Non-Smoker';
       
        $apidata = array(
            'dob1'    => $apidob,
            'dob2'    => $apidob1,
            'lc1'     => $lifecover,
            'lc2'     => $lifecover,
            'term'    => $termcover,
            'sex1'    => $gender1a,
            'sex2'    => $sec_gender1a,
            'smoker1' => $apismoker1,
            'smoker2' => $apismoker2
        );
        
        $plan_node = array(
            'qlo' => 'Y',
            'qla' => 'N',
            'qli' => 'N',
            'qio' => 'N'
        );
        
        $apiresult = call_best_advice_api($apidata,$plan_node);
        $html = '';
        
        foreach ($apiresult['Life Cover Only'] as $key => $val) {
            
            if($val !== '-' && $val !== 0) {
                $html .= "<tr><td>$key</td><td>$val</td></tr>";
            }
        }
        
        $dob =$day.'-'.$month.'-'.$years;
        $qry="INSERT INTO `wp_protection`"
               . " (`protection_name`,`amount`, `term_cover`, `type`, `name`, `contact_number`, `email`, `dob`, `gender`, `is_smoker`, `convertiable`)"
               . " VALUES ( '".$protection_type."', '".$lifecover."', '".$termcover."', '".$radiorelation."', '".$name."', '".$contact."', '".$email."', '".$dob."', '".$gender1a."', '".$radiosmoker."', '".$convertiable."');";
        $wpdb->query($qry);

        // More headers
        $headers[] = "MIME-Version: 1.0" . "\r\n";
        $headers[] = 'From: Financial Life <noreply@financiallife.com>' . "\r\n";
        $headers[] = 'Content-Type: text/html; charset=UTF-8';

        $to = "$email";
        $subject = "Thank you for Quoting with Financial Life Insurance";

        $message = '
        <!DOCTYPE html><html lang="en" xmlns="http://www.w3.org/1999/xhtml"><head>
            <meta name="viewport" content="width=device-width">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="x-apple-disable-message-reformatting">
            <title>Financial Life</title>
            <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&display=swap" rel="stylesheet">
        </head>
        
        <body width="100%" style="margin: 0; padding: 0 !important; mso-line-height-rule: exactly; background-color: #fff;font-family: Poppins, sans-serif;">
        <div style="padding: 20px;font-size:14px;line-height:30px;width: 500px;text-align: center;border-radius: 6px; border: 1px solid #ccc;">
          <a href="http://demo.webandcrafts.com/wp-financial-life" title="financeiallife" style="cursor: pointer;" target="_blank">
            <img src="http://demo.webandcrafts.com/wp-financial-life/wp-content/themes/financial-life/assets/images/mail-logo.png" alt="Financial Life Logo" title="Financial Life Logo">
          </a><br>
          Hello '.$name.',<br>
          Thanks for getting a quick quote with Financial Life Insurance.<br>
          If you havent already applied online and you re still thinking about it,<br>
          We have summarised your personal quote below.<br>'.
        
        $message .= '
        
        <table style="margin-top: 20px; width: 100%;">
            <tr>
                <td style="border:1px solid silver;border-radius:5px;">
                    <table style="width:100%;">
                        <tr>
                            <td style="width:35%;"><u>Provider</td>
                            <td><u>Premium</td>
                        </tr>';
        $message .=    $html;
        $message .= '   <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table><br>';
          'Regards<br>
          Financial Life Insurance
        </div>
        </body></html>';
        
        $headers_admin[] = "MIME-Version: 1.0" . "\r\n";
        $headers_admin[] = 'From: '.$name.' <'.$email.'>' . "\r\n";
        $headers_admin[] = 'Content-Type: text/html; charset=UTF-8';
        
        $admsub = "Life Insurance Cover Request";
        
        
        $admmsg = '
        <!DOCTYPE html><html lang="en" xmlns="http://www.w3.org/1999/xhtml"><head>

            <meta name="viewport" content="width=device-width">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="x-apple-disable-message-reformatting">
            <title>Financial Life</title>
            <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&display=swap" rel="stylesheet">
        </head>
        <style>
            table, tr, td{
                border: none;
                margin: 0px;
                box-sizing: border-box;
                padding: 0px;
                border-collapse: collapse;
            }
            img + div { display:none; }
        </style>
        <body width="100%" style="margin: 0; padding: 0 !important; mso-line-height-rule: exactly; background-color: #fff;font-family: Poppins, sans-serif;">
        <center style="width: 100%;">
            Hello Admin,<br>
            New Life Insurance Cover Request details is as follows <br>
          <table id="main-table" style="width:100%;max-width: 700px; min-width: 700px; background-color: #fff;border-collapse: collapse;border: none;margin: 0px;box-sizing: border-box;padding: 0px;color:#000;">
              <tr style="background-color:#fff;">
                  <td>
                    <table style="width: 100%;">
                        <tr>
                            <td align="left" style="padding: 18px 0 18px 34px;">
                                <a href="http://demo.webandcrafts.com/wp-financial-life" title="spirae" style="cursor: pointer;" target="_blank">
                                    <img src="http://demo.webandcrafts.com/wp-financial-life/wp-content/themes/financial-life/assets/images/logopng.png" alt="Financial Life Logo" title="Financial Life Logo">
                                </a
                            </td>
                        </tr>
                    </table>
                  </td>
              </tr>
              <tr>
                  <td>
                      <table style="width: 100%;">
                        <tr>
                            <td>
                              <table cellpadding="0" cellspacing="0"
                                style="text-align:center;font-family:Verdana,Arial;font-weight:normal;">
                                <tbody>
                                  <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb;">Name:
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;"> '.$name.'</td>
                                  </tr>
                                  <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Email:
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$email.'</td>
                                  </tr>
                                  <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Contact:
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$contact .'</td>
                                  </tr>
                                  <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Amount:
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$lifecover.'</td>
                                  </tr>
                                  
                                   <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Term of Cover:
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$termcover.'</td>
                                  </tr>
                                   <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Protection Type:
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$protection_type.'</td>
                                  </tr>
                                   <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Single/Joint:
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$radiorelation.'</td>
                                  </tr>
                                  <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Date Of Birth:
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;text-align: center;">'.$apidob .'</td>
                                  </tr>
                                        <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Gender:
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;text-align: center;">'.$gender1a.'</td>
                                  </tr>
                                  <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Convertible Option:
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$convertiable.'</td>
                                  </tr>
                                  <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Smoker:
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$apismoker1.'</td>
                                  </tr>';
                            if($radiorelation == 'Joint') {
                                $admmsg .= '<tr>
                                                <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Second Person Gender:</th>
                                                <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$sec_gender1a.'</td>
                                            </tr>
                                            <tr>
                                                <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Second DateOf Birth:</th>
                                                <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$apidob1.'</td>
                                            </tr>
                                            <tr>
                                                <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Second DateOf Birth:</th>
                                                <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$apismoker2.'</td>
                                            </tr>';
                            } 
                                  
                      $admmsg .= '</tbody>
                                </table>
                            </td>
                        </tr>
                      </table>
                  </td>
              </tr>
            </table>';
            $admmsg .= '
        
            <table style="width:550px;">
                <tr>
                    <td style="border:1px solid silver;border-radius:5px;">
                        <table style="width:100%;">
                            <tr>
                                <td style="width:35%;"><u>Provider</td>
                                <td><u>Premium</td>
                            </tr>';
            $admmsg .=    $html;
            $admmsg .= '   <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>';
        $admmsg .= '</center>
            Regards<br>
            Financial Life Insurance
        </body></html>';

        wp_mail(ADMINEMAIL, $admsub, $admmsg, $headers_admin );
        
        $result = array();
        
        if(wp_mail($to, $subject, $message, $headers )) {
            $result['status'] = 1;
            $result['data'] = base64_encode(serialize($apiresult['Life Cover Only']));
            echo json_encode($result);
        } else {
            $result['status'] = 0;
            $result['data'] = base64_encode(serialize($apiresult['Life Cover Only']));
            echo json_encode($result);
        }
        
        exit;
    }
}

add_action('wp_ajax_insurancequote', 'insurancequote');
add_action('wp_ajax_nopriv_insurancequote', 'insurancequote');



/* Life & Serious Illness Cover Form */

if(!function_exists("lifeseriousillnessquote")) {
    function lifeseriousillnessquote() {
        global $wpdb;
        $name              = $_POST['name'];
        $email             = $_POST['email'];
        $contact           = $_POST['contact'];
        $termcover         = $_POST['termcover'];
        $lifecover         = $_POST['lifecover'];
        $illnescover       = $_POST['illnescover']; 
        $protection_type   = $_POST['protection_type']; 
        $radiorelation     = $_POST['radiorelation']; 
        $gender1a          = $_POST['gender1a']; 
        $radiosmoker       = $_POST['radiosmoker'];
        $convertiable      = $_POST['convertiable']; 
        $day               = $_POST['day'];
        $month             = $_POST['month'];
        $years             = $_POST['years'];
      
        $dob =$day.'-'.$month.'-'.$years;
      
        $sec_gender1a = $_POST['sec_gender1a'];
        $secday           = $_POST['secday'];
        $secmonth         = $_POST['secmonth'];
        $secyears         = $_POST['secyears'];
        $sec_radiosmoker  = $_POST['sec_radiosmoker'];
      
        $apidob = $day.'/'.$month.'/'.$years;
        $apidob1 = '';
        if($radiorelation == 'Joint') {
            $apidob1 = $secday.'/'.$secmonth.'/'.$secyears;
        }
        
        $apismoker1 = ($radiosmoker == 1) ? 'Smoker' : 'Non-Smoker';
        $apismoker2 = ($sec_radiosmoker == 'Yes') ? 'Smoker' : 'Non-Smoker';
       
        $apidata = array(
            'dob1'    => $apidob,
            'dob2'    => $apidob1,
            'lc1'     => $lifecover,
            'lc2'     => $lifecover,
            'term'    => $termcover,
            'sex1'    => $gender1a,
            'sex2'    => $sec_gender1a,
            'smoker1' => $apismoker1,
            'smoker2' => $apismoker2
        );
        
        $plan_node = array(
            'qlo' => 'Y',
            'qla' => 'N',
            'qli' => 'Y',
            'qio' => 'N'
        );
        
        $apiresult = call_best_advice_api($apidata,$plan_node);
        $qry="INSERT INTO `wp_protection`"
         . " (`protection_name`,`amount`, `term_cover`, `type`, `name`, `contact_number`, `email`, `dob`, `gender`, `is_smoker`, `is_convertable`, `amount_illness_cover`)"
         . " VALUES ( '".$protection_type."', '".$lifecover."', '".$termcover."', '".$radiorelation."', '".$name."', '".$contact."', '".$email."', '".$dob."', '".$gender1a."', '".$radiosmoker."', '".$convertiable."', '".$illnescover."');";
       
        $wpdb->query($qry);

        // More headers
        $headers[] = "MIME-Version: 1.0" . "\r\n";
        $headers[] = 'From: Financial Life <noreply@financiallife.com>' . "\r\n";
        $headers[] = 'Content-Type: text/html; charset=UTF-8';

        $to = "$email";
        $subject = "Life and Serious Illness Cover Form";

        $message = '
        <!DOCTYPE html><html lang="en" xmlns="http://www.w3.org/1999/xhtml"><head>
            <meta name="viewport" content="width=device-width">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="x-apple-disable-message-reformatting">
            <title>Financial Life</title>
            <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&display=swap" rel="stylesheet">
        </head>
        
        <body width="100%" style="margin: 0; padding: 0 !important; mso-line-height-rule: exactly; background-color: #fff;font-family: Poppins, sans-serif;">
        <div style="padding: 20px;font-size:14px;line-height:30px;width: 500px;text-align: center;border-radius: 6px; border: 1px solid #ccc;">
          <a href="http://demo.webandcrafts.com/wp-financial-life" title="financeiallife" style="cursor: pointer;" target="_blank">
            <img src="http://demo.webandcrafts.com/wp-financial-life/wp-content/themes/financial-life/assets/images/mail-logo.png" alt="Financial Life Logo" title="Financial Life Logo">
          </a><br>
          Hello '.$name.',<br>
          Thanks for getting a quick quote with Financial Life Insurance.<br>
          If you havent already applied online and you re still thinking about it,<br>
          We have summarised your personal quote below.<br>'.
        
        $message .= '
        
        <table style="margin-top: 20px; width: 100%;">
            <tr>
                <td style="border:1px solid silver;border-radius:5px;">
                    <table style="width:100%;">
                        <tr>
                            <td style="width:35%;"><u>Provider</td>
                            <td><u>Premium</td>
                        </tr>';
        $message .=    $html;
        $message .= '   <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table><br>';
          'Regards<br>
          Financial Life Insurance
        </div>
        </body></html>';
        
        $headers_admin[] = "MIME-Version: 1.0" . "\r\n";
        $headers_admin[] = 'From: '.$name.' <'.$email.'>' . "\r\n";
        $headers_admin[] = 'Content-Type: text/html; charset=UTF-8';
        
        $admsub = "Life & Serious Illness Cover Request";
        
        $admmsg = '
        <!DOCTYPE html><html lang="en" xmlns="http://www.w3.org/1999/xhtml"><head>

            <meta name="viewport" content="width=device-width">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="x-apple-disable-message-reformatting">
            <title>Financial Life</title>
            <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&display=swap" rel="stylesheet">
        </head>
        <style>
            table, tr, td{
                border: none;
                margin: 0px;
                box-sizing: border-box;
                padding: 0px;
                border-collapse: collapse;
            }
            img + div { display:none; }
        </style>
        <body width="100%" style="margin: 0; padding: 0 !important; mso-line-height-rule: exactly; background-color: #fff;font-family: Poppins, sans-serif;">
        <center style="width: 100%;">
            Hello Admin,<br>
            New Life Insurance Cover Request details is as follows <br>
          <table id="main-table" style="width:100%;max-width: 700px; min-width: 700px; background-color: #fff;border-collapse: collapse;border: none;margin: 0px;box-sizing: border-box;padding: 0px;color:#000;">
              <tr style="background-color:#fff;">
                  <td>
                    <table style="width: 100%;">
                        <tr>
                            <td align="left" style="padding: 18px 0 18px 34px;">
                                <a href="http://demo.webandcrafts.com/wp-financial-life" title="spirae" style="cursor: pointer;" target="_blank">
                                    <img src="http://demo.webandcrafts.com/wp-financial-life/wp-content/themes/financial-life/assets/images/logopng.png" alt="Financial Life Logo" title="Financial Life Logo">
                                </a
                            </td>
                        </tr>
                    </table>
                  </td>
              </tr>
              <tr>
                  <td>
                      <table style="width: 100%;">
                        <tr>
                            <td>
                              <table cellpadding="0" cellspacing="0"
                                style="text-align:center;font-family:Verdana,Arial;font-weight:normal;">
                                <tbody>
                                  <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb;">Name:
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;"> '.$name.'</td>
                                  </tr>
                                  <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Email:
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$email.'</td>
                                  </tr>
                                  <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Contact:
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$contact .'</td>
                                  </tr>
                                  <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Amount:
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$lifecover.'</td>
                                  </tr>
                                  
                                   <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Term of Cover:
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$termcover.'</td>
                                  </tr>
                                   <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Protection Type:
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$protection_type.'</td>
                                  </tr>
                                   <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Single/Joint:
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$radiorelation.'</td>
                                  </tr>
                                  <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Date Of Birth:
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;text-align: center;">'.$apidob .'</td>
                                  </tr>
                                        <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Gender:
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;text-align: center;">'.$gender1a.'</td>
                                  </tr>
                                  <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Convertible Option:
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$convertiable.'</td>
                                  </tr>
                                  <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Smoker:
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$apismoker1.'</td>
                                  </tr>';
                            if($radiorelation == 'Joint') {
                                $admmsg .= '<tr>
                                                <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Second Person Gender:</th>
                                                <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$sec_gender1a.'</td>
                                            </tr>
                                            <tr>
                                                <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Second DateOf Birth:</th>
                                                <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$apidob1.'</td>
                                            </tr>
                                            <tr>
                                                <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Second DateOf Birth:</th>
                                                <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$apismoker2.'</td>
                                            </tr>';
                            } 
                                  
                      $admmsg .= '</tbody>
                                </table>
                            </td>
                        </tr>
                      </table>
                  </td>
              </tr>
            </table>';
            $admmsg .= '
        
            <table style="width:550px;">
                <tr>
                    <td style="border:1px solid silver;border-radius:5px;">
                        <table style="width:100%;">
                            <tr>
                                <td style="width:35%;"><u>Provider</td>
                                <td><u>Premium</td>
                            </tr>';
            $admmsg .=    $html;
            $admmsg .= '   <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>';
        $admmsg .= '</center>
            Regards<br>
            Financial Life Insurance
        </body></html>';

        wp_mail(ADMINEMAIL, $admsub, $admmsg, $headers_admin );
        
        $result = array();
        
        if(wp_mail($to, $subject, $message, $headers )) {
            $result['status'] = 1;
            $result['data'] = base64_encode(serialize($apiresult['Life Cover Only']));
            echo json_encode($result);
        } else {
            $result['status'] = 0;
            $result['data'] = base64_encode(serialize($apiresult['Life Cover Only']));
            echo json_encode($result);
        }
        
        exit;
    }
}

add_action('wp_ajax_lifeseriousillnessquote', 'lifeseriousillnessquote');
add_action('wp_ajax_nopriv_lifeseriousillnessquote', 'lifeseriousillnessquote');



/* Income Protection Form */

if(!function_exists("incomprotectioneform")) {
    function incomprotectioneform() {
        global $wpdb;
        $name             = $_POST['name'];
        $email            = $_POST['email'];
        $contact          = $_POST['contact'];
        $lifecover        = $_POST['lifecover'];
        $agecover         = $_POST['agecover']; 
        $protection_type  = $_POST['protection_type']; 
        $employmenttype   = $_POST['employmenttype']; 
        $gender1a         = $_POST['gender1a'];
        $day              = $_POST['day'];
        $month            = $_POST['month'];
        $years            = $_POST['years'];
      
      
        $dob =$day.'-'.$month.'-'.$years;
        $qry="INSERT INTO `wp_protection`"
         . " (`protection_name`,`amount`, `term_cover`, `type`, `name`, `contact_number`, `email`, `dob`, `gender`, `agecover`, `employmenttype`)"
         . " VALUES ( '".$protection_type."', '".$lifecover."', '".$termcover."', '".$radiorelation."', '".$name."', '".$contact."', '".$email."', '".$dob."', '".$gender1a."', '".$agecover."', '".$employmenttype."');";

      
        $wpdb->query($qry);
        // More headers
        $headers[] = "MIME-Version: 1.0" . "\r\n";
        $headers[] = 'From: Financial Life <noreply@financiallife.com>' . "\r\n";
        $headers[] = 'Content-Type: text/html; charset=UTF-8';

        $to = "$email";
        $subject = "Income Protection Form";

        $message = '
        <!DOCTYPE html><html lang="en" xmlns="http://www.w3.org/1999/xhtml"><head>

            <meta name="viewport" content="width=device-width">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="x-apple-disable-message-reformatting">
            <title>Financial Life</title>
            <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&display=swap" rel="stylesheet">
        </head>
        <style>
            table, tr, td{
                border: none;
                margin: 0px;
                box-sizing: border-box;
                padding: 0px;
                border-collapse: collapse;
            }
            img + div { display:none; }
        </style>
        <body width="100%" style="margin: 0; padding: 0 !important; mso-line-height-rule: exactly; background-color: #fff;font-family: Poppins, sans-serif;">
        <center style="width: 100%;">
          <table id="main-table" style="width:100%;max-width: 700px; min-width: 700px; background-color: #fff;border-collapse: collapse;border: none;margin: 0px;box-sizing: border-box;padding: 0px;color:#000;">
              <tr style="background-color:#fff;">
                  <td>
                    <table style="width: 100%;">
                        <tr>
                            <td align="left" style="padding: 18px 0 18px 34px;">
                                <a href="http://demo.webandcrafts.com/wp-financial-life" title="spirae" style="cursor: pointer;" target="_blank">
                                    <img src="http://demo.webandcrafts.com/wp-financial-life/wp-content/themes/financial-life/assets/images/logopng.png" alt="Financial Life Logo" title="Financial Life Logo">
                                </a
                            </td>
                        </tr>
                    </table>
                  </td>
              </tr>
              <tr>
                  <td>
                      <table style="width: 100%;">
                        <tr>
                            <td>
                              <table cellpadding="0" cellspacing="0"
                                style="text-align:center;font-family:Verdana,Arial;font-weight:normal;">
                                <tbody>
                                  <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb;">Name:
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;"> '.$name.'</td>
                                  </tr>
                                  <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Email:
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$email.'</td>
                                  </tr>
                                  <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Contact:
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$contact .'</td>
                                  </tr>
                                  <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Income Amount to Cover:
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$lifecover.'</td>
                                  </tr>
                                  
                                   <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Retirement age:
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$agecover.'</td>
                                  </tr>
                                  
                                   <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Protection Type:
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$protection_type.'</td>
                                  </tr>
                                  
                                   <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Employment Type:
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$employmenttype.'</td>
                                  </tr>
                                  
                                  <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Date Of Birth:
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;text-align: center;">'.$day.' / '.$month.' / '.$years.'</td>
                                  </tr>
                     
                                  <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Gender:
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$gender1a.'</td>
                                  </tr>
                                 
                                </tbody>
                              </table>
                            </td>
                        </tr>
                      </table>
                  </td>
              </tr>
          </table>
        </center>
        </body></html>';
        if(wp_mail($to, $subject, $message, $headers )) {
            echo 1;
        } else {
            echo 0;
        }
        exit;
    }
}

add_action('wp_ajax_incomprotectioneform', 'incomprotectioneform');
add_action('wp_ajax_nopriv_incomprotectioneform', 'incomprotectioneform');



/* Whole of Life Insurance Form */

if(!function_exists("wholelifeinsuranceform")) {
    function wholelifeinsuranceform() {
        global $wpdb;
        $name             = $_POST['name'];
        $email            = $_POST['email'];
        $contact          = $_POST['contact'];
        $lifecover        = $_POST['lifecover'];
        $protection_type  = $_POST['protection_type']; 
        $gender1a         = $_POST['gender1a'];
        $radiosmoker      = $_POST['radiosmoker'];
        $radiorelation    = $_POST['radiorelation'];
        $day              = $_POST['day'];
        $month            = $_POST['month'];
        $years            = $_POST['years'];
        
       $dob =$day.'-'.$month.'-'.$years;
       $qry="INSERT INTO `wp_protection`"
         . " (`protection_name`,`amount`, `type`, `name`, `contact_number`, `email`, `dob`, `gender`,`term_cover`, `is_smoker`)"
         . " VALUES ( '".$protection_type."', '".$lifecover."', '".$radiorelation."', '".$name."', '".$contact."', '".$email."', '".$dob."', '".$gender1a."', '', '".$radiosmoker."');";
        $wpdb->query($qry);
//     echo $qry;
//     exit;
      

        // More headers
        $headers[] = "MIME-Version: 1.0" . "\r\n";
        $headers[] = 'From: Financial Life <noreply@financiallife.com>' . "\r\n";
        $headers[] = 'Content-Type: text/html; charset=UTF-8';

        $to = "$email";
        $subject = " Whole of Life Insurance Form ";

        $message = '
        <!DOCTYPE html><html lang="en" xmlns="http://www.w3.org/1999/xhtml"><head>

            <meta name="viewport" content="width=device-width">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="x-apple-disable-message-reformatting">
            <title>Financial Life</title>
            <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&display=swap" rel="stylesheet">
        </head>
        <style>
            table, tr, td{
                border: none;
                margin: 0px;
                box-sizing: border-box;
                padding: 0px;
                border-collapse: collapse;
            }
            img + div { display:none; }
        </style>
        <body width="100%" style="margin: 0; padding: 0 !important; mso-line-height-rule: exactly; background-color: #fff;font-family: Poppins, sans-serif;">
        <center style="width: 100%;">
          <table id="main-table" style="width:100%;max-width: 700px; min-width: 700px; background-color: #fff;border-collapse: collapse;border: none;margin: 0px;box-sizing: border-box;padding: 0px;color:#000;">
              <tr style="background-color:#fff;">
                  <td>
                    <table style="width: 100%;">
                        <tr>
                            <td align="left" style="padding: 18px 0 18px 34px;">
                                <a href="http://demo.webandcrafts.com/wp-financial-life" title="spirae" style="cursor: pointer;" target="_blank">
                                    <img src="http://demo.webandcrafts.com/wp-financial-life/wp-content/themes/financial-life/assets/images/logopng.png" alt="Financial Life Logo" title="Financial Life Logo">
                                </a
                            </td>
                        </tr>
                    </table>
                  </td>
              </tr>
              <tr>
                  <td>
                      <table style="width: 100%;">
                        <tr>
                            <td>
                              <table cellpadding="0" cellspacing="0"
                                style="text-align:center;font-family:Verdana,Arial;font-weight:normal;">
                                <tbody>
                                  <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb;">Name:
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;"> '.$name.'</td>
                                  </tr>
                                  <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Email:
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$email.'</td>
                                  </tr>
                                  <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Contact:
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$contact .'</td>
                                  </tr>
                                  <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Amount of Cover:
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$lifecover.'</td>
                                  </tr>

                                   <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Protection Type:
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$protection_type.'</td>
                                  </tr>

                                  <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Date Of Birth:
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;text-align: center;">'.$day.' / '.$month.' / '.$years.'</td>
                                  </tr>
                     
                                  <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Gender:
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$gender1a.'</td>
                                  </tr>
                                  <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Smoker:
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$radiosmoker.'</td>
                                  </tr>
                                </tbody>
                              </table>
                            </td>
                        </tr>
                      </table>
                  </td>
              </tr>
          </table>
        </center>
        </body></html>';
        if(wp_mail($to, $subject, $message, $headers )) {
            echo 1;
          } else {
            echo 0;
          }exit;
    }
}

add_action('wp_ajax_wholelifeinsuranceform', 'wholelifeinsuranceform');
add_action('wp_ajax_nopriv_wholelifeinsuranceform', 'wholelifeinsuranceform');



/* Pension Form */

if(!function_exists("pensionform"))   {
    function pensionform() {
        global $wpdb;
        $firstname             = $_POST['firstname'];
        $lastname              = $_POST['lastname'];
        $email                 = $_POST['email'];
        $contact               = $_POST['contact'];
        $status                = $_POST['status'];
        $pensiontype           = $_POST['pensiontype']; 
        $country_select        = $_POST['country_select'];
        $contributing_pension  = $_POST['contributing_pension'];
    
        // More headers
        $headers[] = "MIME-Version: 1.0" . "\r\n";
        $headers[] = 'From: Financial Life <noreply@financiallife.com>' . "\r\n";
        $headers[] = 'Content-Type: text/html; charset=UTF-8';

        $to = "$email";
        $subject = "Pension Form";

        $message = '
        <!DOCTYPE html><html lang="en" xmlns="http://www.w3.org/1999/xhtml"><head>

            <meta name="viewport" content="width=device-width">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="x-apple-disable-message-reformatting">
            <title>Financial Life</title>
            <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&display=swap" rel="stylesheet">
        </head>
        <style>
            table, tr, td{
                border: none;
                margin: 0px;
                box-sizing: border-box;
                padding: 0px;
                border-collapse: collapse;
            }
            img + div { display:none; }
        </style>
        <body width="100%" style="margin: 0; padding: 0 !important; mso-line-height-rule: exactly; background-color: #fff;font-family: Poppins, sans-serif;">
        <center style="width: 100%;">
          <table id="main-table" style="width:100%;max-width: 700px; min-width: 700px; background-color: #fff;border-collapse: collapse;border: none;margin: 0px;box-sizing: border-box;padding: 0px;color:#000;">
              <tr style="background-color:#fff;">
                  <td>
                    <table style="width: 100%;">
                        <tr>
                            <td align="left" style="padding: 18px 0 18px 34px;">
                                <a href="http://demo.webandcrafts.com/wp-financial-life" title="spirae" style="cursor: pointer;" target="_blank">
                                    <img src="http://demo.webandcrafts.com/wp-financial-life/wp-content/themes/financial-life/assets/images/logopng.png" alt="Financial Life Logo" title="Financial Life Logo">
                                </a
                            </td>
                        </tr>
                    </table>
                  </td>
              </tr>
              <tr>
                  <td>
                      <table style="width: 100%;">
                        <tr>
                            <td>
                              <table cellpadding="0" cellspacing="0"
                                style="text-align:center;font-family:Verdana,Arial;font-weight:normal;">
                                <tbody>
                                  <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb;">First Name:
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;"> '.$firstname.'</td>
                                  </tr>
                                   <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb;">Last Name:
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;"> '.$lastname.'</td>
                                  </tr>
                                  <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Email:
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$email.'</td>
                                  </tr>
                                  <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Contact:
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$contact .'</td>
                                  </tr>
                                  <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Employment Status:
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$status.'</td>
                                  </tr>

                                   <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Pension Type:
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$pensiontype.'</td>
                                  </tr>
                     
                                  <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Country Type:
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$country_select.'</td>
                                  </tr>
                  
                                  <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">contributing Pension
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;text-align: center;">'.$contributing_pension.'</td>
                                  </tr>
                                </tbody>
                              </table>
                            </td>
                        </tr>
                      </table>
                  </td>
              </tr>
          </table>
        </center>
        </body></html>';
        if(wp_mail($to, $subject, $message, $headers )) {
            echo 1;
          } else {
            echo 0;
          }exit;
    }
}

add_action('wp_ajax_pensionform', 'pensionform');
add_action('wp_ajax_nopriv_pensionform', 'pensionform');



/* Investment Form */

if(!function_exists("investmentform"))   {
    function investmentform() {
        global $wpdb;
        $name                = $_POST['name'];
        $email               = $_POST['email'];
        $contact             = $_POST['contact'];
        $pensiontype         = $_POST['pensiontype']; 
        $income              = $_POST['income'];
        $day                 = $_POST['day'];
        $month               = $_POST['month'];
        $years               = $_POST['years'];
      
        // More headers
        $headers[] = "MIME-Version: 1.0" . "\r\n";
        $headers[] = 'From: Financial Life <noreply@financiallife.com>' . "\r\n";
        $headers[] = 'Content-Type: text/html; charset=UTF-8';

        $to = "$email";
        $subject = "Investment Form";

        $message = '
        <!DOCTYPE html><html lang="en" xmlns="http://www.w3.org/1999/xhtml"><head>

            <meta name="viewport" content="width=device-width">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="x-apple-disable-message-reformatting">
            <title>Financial Life</title>
            <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&display=swap" rel="stylesheet">
        </head>
        <style>
            table, tr, td{
                border: none;
                margin: 0px;
                box-sizing: border-box;
                padding: 0px;
                border-collapse: collapse;
            }
            img + div { display:none; }
        </style>
        <body width="100%" style="margin: 0; padding: 0 !important; mso-line-height-rule: exactly; background-color: #fff;font-family: Poppins, sans-serif;">
        <center style="width: 100%;">
          <table id="main-table" style="width:100%;max-width: 700px; min-width: 700px; background-color: #fff;border-collapse: collapse;border: none;margin: 0px;box-sizing: border-box;padding: 0px;color:#000;">
              <tr style="background-color:#fff;">
                  <td>
                    <table style="width: 100%;">
                        <tr>
                            <td align="left" style="padding: 18px 0 18px 34px;">
                                <a href="http://demo.webandcrafts.com/wp-financial-life" title="spirae" style="cursor: pointer;" target="_blank">
                                    <img src="http://demo.webandcrafts.com/wp-financial-life/wp-content/themes/financial-life/assets/images/logopng.png" alt="Financial Life Logo" title="Financial Life Logo">
                                </a
                            </td>
                        </tr>
                    </table>
                  </td>
              </tr>
              <tr>
                  <td>
                      <table style="width: 100%;">
                        <tr>
                            <td>
                              <table cellpadding="0" cellspacing="0"
                                style="text-align:center;font-family:Verdana,Arial;font-weight:normal;">
                                <tbody>
                                  <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb;">First Name:
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;"> '.$name.'</td>
                                  </tr>
                                  <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Email:
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$email.'</td>
                                  </tr>
                                  <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Contact:
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$contact.'</td>
                                  </tr>
                                  
                                  <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Approx Annual Income:
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$income.'</td>
                                  </tr>
                                  
                                   <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Date Of Birth:
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;text-align: center;">'.$day.' / '.$month.' / '.$years.'</td>
                                  </tr>
                                 
                                </tbody>
                              </table>
                            </td>
                        </tr>
                      </table>
                  </td>
              </tr>
          </table>
        </center>
        </body></html>';
        if(wp_mail($to, $subject, $message, $headers )) {
            echo 1;
          } else {
            echo 0;
          }exit;
    }
}

add_action('wp_ajax_investmentform', 'investmentform');
add_action('wp_ajax_nopriv_investmentform', 'investmentform');



/* Mortgages Form */

if(!function_exists("mortgagesform"))   {
    function mortgagesform() {
        global $wpdb;
        $applicant_type         = $_POST['applicant_type'];
        $name                   = $_POST['name'];
        $email                  = $_POST['email'];
        $contact                = $_POST['contact'];
        $postadd                = $_POST['postadd'];
        $pensiontype            = $_POST['pensiontype']; 
        $buy                    = $_POST['buy'];
        $price_property         = $_POST['price_property'];
        $load_amount            = $_POST['load_amount'];
        $property_address       = $_POST['property_address'];
        $type_property          = $_POST['type_property'];
        $purchase_price         = $_POST['purchase_price'];
        $year_purchase          = $_POST['year_purchase'];
        $amount_owe             = $_POST['amount_owe'];
        $monthly_mortgage       = $_POST['monthly_mortgage'];
        $estimated_value        = $_POST['estimated_value'];
        $cremployment_single    = $_POST['cremployment_single'];
        $time_employment_single = $_POST['time_employment_single'];
        $annual_income_single   = $_POST['annual_income_single'];
        $credit_rating          = $_POST['credit_rating'];
        $current_employer       = $_POST['current_employer'];
        $current_employer_time  = $_POST['current_employer_time'];
        $annual_income          = $_POST['annual_income'];
        $sec_credit_rating      = $_POST['sec_credit_rating'];
        $secs_name              = $_POST['secs_name'];
        $secs_email             = $_POST['secs_email'];
        $secs_contact           = $_POST['secs_contact'];
        $credit_rating          = $_POST['credit_rating'];
        $besttime_contact       = $_POST['besttime_contact'];
      
        // More headers
        $headers[] = "MIME-Version: 1.0" . "\r\n";
        $headers[] = 'From: Financial Life <noreply@financiallife.com>' . "\r\n";
        $headers[] = 'Content-Type: text/html; charset=UTF-8';

        $to = "$email";
        $subject = "Mortgages";

        $message = '<!DOCTYPE html><html lang="en" xmlns="http://www.w3.org/1999/xhtml"><head>

            <meta name="viewport" content="width=device-width">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="x-apple-disable-message-reformatting">
            <title>Financial Life</title>
            <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&display=swap" rel="stylesheet">
        </head>
        <style>
            table, tr, td{
                border: none;
                margin: 0px;
                box-sizing: border-box;
                padding: 0px;
                border-collapse: collapse;
            }
            img + div { display:none; }
        </style>
        <body width="100%" style="margin: 0; padding: 0 !important; mso-line-height-rule: exactly; background-color: #fff;font-family: Poppins, sans-serif;">
        <center style="width: 100%;">
          <table id="main-table" style="width:100%;max-width: 700px; min-width: 700px; background-color: #fff;border-collapse: collapse;border: none;margin: 0px;box-sizing: border-box;padding: 0px;color:#000;">
              <tr style="background-color:#fff;">
                  <td>
                    <table style="width: 100%;">
                        <tr>
                            <td align="left" style="padding: 18px 0 18px 34px;">
                                <a href="http://demo.webandcrafts.com/wp-financial-life" title="spirae" style="cursor: pointer;" target="_blank">
                                    <img src="http://demo.webandcrafts.com/wp-financial-life/wp-content/themes/financial-life/assets/images/logopng.png" alt="Financial Life Logo" title="Financial Life Logo">
                                </a
                            </td>
                        </tr>
                    </table>
                  </td>
              </tr>
              <tr>
                  <td>
                      <table style="width: 100%;">
                        <tr>
                            <td>
                              <table cellpadding="0" cellspacing="0"
                                style="text-align:center;font-family:Verdana,Arial;font-weight:normal;">
                                <tbody>
                                 <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Applicant type:
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$applicant_type.'</td>
                                  </tr>
                                  
                                  <tr>
                                  <td style="background: #626ba6;
                                  padding: 10px !important;
                                  color: #fff;">Single Applicant</td>
                                  </tr>
                                  
                                  
                                  
                                  <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb;">Name:
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;"> '.$name.'</td>
                                  </tr>
                                  <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Email:
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$email.'</td>
                                  </tr>
                                  <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Contact:
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$contact.'</td>
                                  </tr>';

                                if (!empty($postadd)) {
                                $message.='<tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Postal Address:
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$postadd.'</td>
                                  </tr>';
                                 } 
      
                                 if (!empty($secs_name)) {
                                $message.='<tr> <td style="background: #626ba6;
                                  padding: 10px !important;
                                  color: #fff;"> Second Applicant</td></tr>
                                   <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Name
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$secs_name.'</td>
                                  </tr>';                            
                                 }
      
                                if (!empty($sec_email)) {
                                  $message.=' <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Email
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$sec_email.'</td>
                                  </tr>';
                                }
      
                                if (!empty($sec_contact)) {
                                 $message.='<tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Contact Number
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$sec_contact.'</td>
                                  </tr>';
                                  }
      
                                if (!empty($buy)) {
                                  $message.='<tr><td style="background: #626ba6;
                                  padding: 10px !important;
                                  color: #fff;">The Property</td></tr>
                                  
                                 <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">What are you looking to buy?
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$buy.'</td>
                                  </tr>';
                                  }
      

                                  if (!empty($price_property)) {
                                  $message.='<tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">What is the price of the property you want to purchase?
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$price_property.'</td>
                                  </tr>';
                                  }
      
      
                                if (!empty($load_amount)) {
                                  $message.='<tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">What loan amount are you looking for?
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$load_amount.'</td>
                                  </tr>';
                                  }
                                  
                                  
                                  if (!empty($property_address)) {
                                  $message.='<tr><td style="background: #626ba6;
                                  padding: 10px !important;
                                  color: #fff;">Second Applicant Existing Property Details</td></tr>
                                  
                                  <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">What is your current property address?
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$property_address.'</td>
                                  </tr>';
                                  
                                  }
                                  
                                  if (!empty($type_property)) {
                                  $message.='<tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">What type of property is it?
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$type_property.'</td>
                                  </tr>';
                                  }
      
                                  if (!empty($credit_rating)) {
                                  $message.='<tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">What would you describe your credit rating?
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$credit_rating.'</td>
                                  </tr>';
                                  }
                                  
                                  if (!empty($purchase_price)) {
                                  $message.='<tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">What was it’s original purchase price?
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$purchase_price.'</td>
                                  </tr>';
                                  }
      
                                  if (!empty($year_purchase)) {
                                   $message.='<tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">What type of property is it?
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$year_purchase.'</td>
                                  </tr>';
                                  }
      
                                  if (!empty($amount_owe)) {
                                  $message.='<tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">What is the amount you owe on it at present?
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$amount_owe.'</td>
                                  </tr>';
                                  }
      
      
      
                                if (!empty($besttime_contact)) {
                                  $message.='<tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">What is the amount you owe on it at present?
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$besttime_contact.'</td>
                                  </tr>';
                                  }
      
      
      
                                if (!empty($monthly_mortgage)) {
                                   $message.='<tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">What is your current monthly mortgage payment on this property?
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$monthly_mortgage.'</td>
                                  </tr>';
                                  }
                                  
                                  if (!empty($estimated_value)) {
                                  $message.='<tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">What is it’s estimated present value?
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$estimated_value.'</td>
                                  </tr>';
                                  }
      
                                  if (!empty($cremployment_single)) {
                                  $message.='<tr> <td style="background: #626ba6;
                                  padding: 10px !important;
                                  color: #fff;">First Applicant</td></tr>
                                  
                                  <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Current Employer?
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$cremployment_single.'</td>
                                  </tr>';
                                  }

      
                                  if (!empty($time_employment_single)) {
                                  $message.='<tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Time with current employer (years)
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$time_employment_single.'</td>
                                  </tr>';
                                  }
                                  
                                  if (!empty($annual_income_single)) {
                                  $message.='<tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Annual Income (EUR)
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$annual_income_single.'</td>
                                  </tr>';
                                  }
                                  
      
                                 if (!empty($credit_rating)) {
                                   $message.='<tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">How would you describe your credit rating?
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$credit_rating.'</td>
                                  </tr>';
                                  }
                                  
                                  if (!empty($current_employer)) {
                                  $message.='<tr><td style="background: #626ba6;
                                  padding: 10px !important;
                                  color: #fff;">Second Applicant</td></tr>
                                   <tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Current Employer
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$current_employer.'</td>
                                  </tr>';
                                  }
      
                                  if (!empty($annual_income)) {
                                   $message.='<tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">Annual Income (EUR)
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$annual_income.'</td>
                                  </tr>';
                                  }
                                  
                                  if (!empty($sec_credit_rating)) {
                                    $message.='<tr>
                                    <th style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;background-color:#ebebeb">How would you describe your credit rating?
                                    </th>
                                    <td style="border:1px solid #dddddd;text-align:left;padding:8px;width:50%;text-align: center;">'.$sec_credit_rating.'</td>
                                  </tr>';
                                   }
                                $message.='</tbody>
                              </table>
                            </td>
                        </tr>
                      </table>
                  </td>
              </tr>
          </table>
        </center>
        </body></html>';
        if(wp_mail($to, $subject, $message, $headers )) {
            echo 1;
          } else {
            echo 0;
          }exit;
    }
}

add_action('wp_ajax_mortgagesform', 'mortgagesform');
add_action('wp_ajax_nopriv_mortgagesform', 'mortgagesform');

function init_theme_method() {
    add_thickbox();

}
add_action('init', 'init_theme_method');

function call_best_advice_api($data, $plan_node) {
    $system_name = "FinLife";
    $system_code = "gBs5d9jHx";
    $username = "FL_quotes";
    $password = "8BCxzw2g";
    $quote = array();

    $xml = "<Inputs>";
    $xml .= "<Authentication>";
    $xml .= "<Username>$username</Username>";
    $xml .= "<Password>$password</Password>";
    $xml .= "<RequestType>Term</RequestType>";
    $xml .= "<RequestFrom>$system_name</RequestFrom>";
    $xml .= "<RequestFromCode>$system_code</RequestFromCode>";
    $xml .= "</Authentication>";
    $xml .= "<Life1>";
    $xml .= "<DOB>{$data['dob1']}</DOB>";
    $xml .= "<Sex>{$data['sex1']}</Sex>";
    $xml .= "<Smoker>{$data['smoker1']}</Smoker>";
    $xml .= "<LifeCover>{$data['lc1']}</LifeCover>";
    $xml .= "</Life1>";
    
    if ($data['dob2'] !== '') {
        $xml .= "<Life2>";
        $xml .= "<DOB>{$data['dob2']}</DOB>";
        $xml .= "<Sex>{$data['sex2']}</Sex>";
        $xml .= "<Smoker>{$data['smoker2']}</Smoker>";
        $xml .= "<LifeCover>{$data['lc2']}</LifeCover>";
        $xml .= "</Life2>";
    }
    
    $xml .= "<Plan>";
    $xml .= "<Term>{$data['term']}</Term>";
    $xml .= "<Indexation>N</Indexation>";
    $xml .= "<MortgageInterest>6</MortgageInterest>";
    $xml .= "<Frequency>Monthly</Frequency>";
    $xml .= "<QuoteLifeOnly>{$plan_node['qlo']}</QuoteLifeOnly>";
    $xml .= "<QuoteLifeAccelerated>{$plan_node['qla']}</QuoteLifeAccelerated>";
    $xml .= "<QuoteLifeIllness>{$plan_node['qli']}</QuoteLifeIllness>";
    $xml .= "<QuoteIllnessOnly>{$plan_node['qio']}</QuoteIllnessOnly>";
    $xml .= "</Plan>";
    $xml .= "</Inputs>";

    $url = 'http://www.bestadvice.ie/interface3.php';
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, "xml=" . urlencode($xml));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    $xml_result = curl_exec($ch);
    curl_close($ch);

    $xml = simplexml_load_string($xml_result);
    
    if ($data['dob2'] != '') {
        foreach ($xml->Outputs->Quotes->Type as $quoteBlock) {

            $blockDesc = (string)$quoteBlock->Desc;

            foreach ($quoteBlock->Company as $arr) {
                $companyName = (string)$arr->Name;
                $premium = (string)$arr->JMortgage;
                if($premium !== '-') {
                    $quote[$blockDesc][$companyName] = $premium;
                }
            }

        }
    } else {
        foreach ($xml->Outputs->Quotes->Type as $quoteBlock) {
            $blockDesc = (string)$quoteBlock->Desc;
            
            foreach ($quoteBlock->Company as $arr) {
                
                $companyName = (string)$arr->Name;
                $premium = (string)$arr->SMortgage;
                if($premium !== '-' && $premium !== '0') {
                    $quote[$blockDesc][$companyName] = $premium;
                }
            }

        }
    }

    return $quote;
   
}