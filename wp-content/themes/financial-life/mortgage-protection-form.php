<?php
/**
  Template Name: mortgage-protection-form
*/

get_header(); ?>


<div id="barba-wrapper">
  <div class="barba-container deposits-savings" data-namespace="popup">
    <section class="mort-popup section-1">
      <div class="popup_contnt">
        <div class="fl-container">
          <div class="form_head">
            <h4>Mortgage Protection cover</h4>
            <a class="d-flex flex-vcenter" href="javascript:history.go(-1)">
              <span><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/right-arrow-stroke.svg"></span>Back</a>
          </div>

          <div class="modal-body">
            <form action="" id="mortgage-protection-form">
              <input type="hidden" name="action" value="mortgageprotectionForm" />
              <input type="hidden" name="protection_type" value="protection" id="protection_type" />
              <div class="is-floating-label">
                <label for="lifecover">Amount of Life Cover</label>
                <input type="text" name="lifecover" id="lifecover" />
              </div>
              <div class="is-floating-label">
                <select name="termcover" id="termcover">
                  <option value="0" disabled selected>Term of Cover</option>
                  <option value="10">10 years</option>
                  <option value="11">11 years</option>
                  <option value="12">12 years</option>
                  <option value="13">13 years</option>
                  <option value="14">14 years</option>
                  <option value="15">15 years</option>
                  <option value="16">16 years</option>
                  <option value="17">17 years</option>
                  <option value="18">18 years</option>
                  <option value="19">19 years</option>
                  <option value="20">20 years</option>
                  <option value="21">21 years</option>
                  <option value="22">22 years</option>
                  <option value="23">23 years</option>
                  <option value="24">24 years</option>
                  <option value="25">25 years</option>
                  <option value="26">26 years</option>
                  <option value="27">27 years</option>
                  <option value="28">28 years</option>
                  <option value="29">29 years</option>
                  <option value="30">30 years</option>
                  <option value="31">31 years</option>
                  <option value="32">32 years</option>
                  <option value="33">33 years</option>
                  <option value="34">34 years</option>
                  <option value="35">35 years</option>
                  <option value="36">36 years</option>
                  <option value="37">37 years</option>
                  <option value="38">38 years</option>
                  <option value="39">39 years</option>
                  <option value="40">40 years</option>
                </select>
              </div>

              <div class="radio-label">
                <p>
                  <input type="radio" id="test1" name="radiorelation" value="Single"  onclick="show1();" />
                  <label for="test1"> <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/single.svg">Single</label>
                </p>
                <p>
                  <input type="radio" id="test2" name="radiorelation" value="Joint" class="fresher_checked" onclick="show2();" />
                  <label for="test2"> <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/duble.svg">Joint</label>
                </p>
              </div>

              <div class="is-floating-label">
                <label for="name">Name</label>
                <input type="text" name="name" id="name" />
              </div>

              <div class="is-floating-label">
                <label for="contact">Contact Number</label>
                <input type="text" name="contact" id="contact" />
              </div>

              <div class="is-floating-label">
                <label for="email">Email</label>
                <input type="text" id="email" name="email" />
              </div>

              <div class="dob_wrpr">
                <label>Date Of Birth</label>
                <div class="date_wrpr">
                  <div class="is-floating-label datebirth">
                    <select name="day" id="">
                      <option value="0" disabled selected>DD</option>
                      <option value="01">1</option>
                      <option value="02">2</option>
                      <option value="03">3</option>
                      <option value="04">4</option>
                      <option value="05">5</option>
                      <option value="06">6</option>
                      <option value="07">7</option>
                      <option value="08">8</option>
                      <option value="09">9</option>
                      <option value="10">10</option>
                      <option value="11">11</option>
                      <option value="12">12</option>
                      <option value="13">13</option>
                      <option value="14">14</option>
                      <option value="15">15</option>
                      <option value="16">16</option>
                      <option value="17">17</option>
                      <option value="18">18</option>
                      <option value="19">19</option>
                      <option value="20">20</option>
                      <option value="21">21</option>
                      <option value="22">22</option>
                      <option value="23">23</option>
                      <option value="24">24</option>
                      <option value="25">25</option>
                      <option value="26">26</option>
                      <option value="27">27</option>
                      <option value="28">28</option>
                      <option value="29">29</option>
                      <option value="30">30</option>
                      <option value="31">31</option>
                    </select>
                  </div>
                  <div class="is-floating-label datebirth">
                    <select name="month" id="">
                      <option value="MM" disabled selected>MM</option>
                      <option value="01">Jan</option>
                      <option value="02">Feb</option>
                      <option value="03">Mar</option>
                      <option value="04">Apr</option>
                      <option value="05">May</option>
                      <option value="06">Jun</option>
                      <option value="07">Jul</option>
                      <option value="08">Aug</option>
                      <option value="09">Sep</option>
                      <option value="10">Oct</option>
                      <option value="11">Nov</option>
                      <option value="12">Dec</option>
                    </select>
                  </div>
                  <div class="is-floating-label datebirth">
                    <select name="years">
                      <option value="YY" disabled selected>YY</option>
                      <option value="2001">2001</option>
                      <option value="2000">2000</option>
                      <option value="1999">1999</option>
                      <option value="1998">1998</option>
                      <option value="1997">1997</option>
                      <option value="1996">1996</option>
                      <option value="1995">1995</option>
                      <option value="1994">1994</option>
                      <option value="1993">1993</option>
                      <option value="1992">1992</option>
                      <option value="1991">1991</option>
                      <option value="1990">1990</option>
                      <option value="1989">1989</option>
                      <option value="1988">1988</option>
                      <option value="1987">1987</option>
                      <option value="1986">1986</option>
                      <option value="1985">1985</option>
                      <option value="1984">1984</option>
                      <option value="1983">1983</option>
                      <option value="1982">1982</option>
                      <option value="1981">1981</option>
                      <option value="1980">1980</option>
                      <option value="1979">1979</option>
                      <option value="1978">1978</option>
                      <option value="1977">1977</option>
                      <option value="1976">1976</option>
                      <option value="1975">1975</option>
                      <option value="1974">1974</option>
                      <option value="1973">1973</option>
                      <option value="1972">1972</option>
                      <option value="1971">1971</option>
                      <option value="1970">1970</option>
                      <option value="1969">1969</option>
                      <option value="1968">1968</option>
                      <option value="1967">1967</option>
                      <option value="1966">1966</option>
                      <option value="1965">1965</option>
                      <option value="1964">1964</option>
                      <option value="1963">1963</option>
                      <option value="1962">1962</option>
                      <option value="1961">1961</option>
                      <option value="1960">1960</option>
                      <option value="1959">1959</option>
                      <option value="1958">1958</option>
                      <option value="1957">1957</option>
                      <option value="1956">1956</option>
                      <option value="1955">1955</option>
                      <option value="1954">1954</option>
                      <option value="1953">1953</option>
                      <option value="1952">1952</option>
                      <option value="1951">1951</option>
                      <option value="1950">1950</option>
                      <option value="1949">1949</option>
                      <option value="1948">1948</option>
                      <option value="1947">1947</option>
                      <option value="1946">1946</option>
                      <option value="1945">1945</option>
                      <option value="1944">1944</option>
                      <option value="1943">1943</option>
                      <option value="1942">1942</option>
                      <option value="1941">1941</option>
                      <option value="1940">1940</option>
                      <option value="1939">1939</option>
                      <option value="1938">1938</option>
                      <option value="1937">1937</option>
                    </select>
                  </div>
                </div>
              </div>

              <div class="radio-label">
                <p>
                  <input type="radio" id="test3" name="gender1a" value="Male" />
                  <label for="test3"> <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/man.svg">Male</label>
                </p>
                <p>
                  <input type="radio" id="test4" name="gender1a" value="Female" />
                  <label for="test4"> <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/woman.svg">Female</label>
                </p>
              </div>

              <div class="radio-wrpr">
                <label>Smoker</label>
                <div class="radio-label">
                  <p>
                    <input type="radio" id="test5" name="radiosmoker" value="1" />
                    <label for="test5"> <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/smoker.svg">Yes</label>
                  </p>
                  <p>
                    <input type="radio" id="test6" name="radiosmoker" value="0" />
                    <label for="test6"> <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/no_smoke.svg">No</label>
                  </p>
                </div>
              </div>

              <div class="second-persion" id="joint-sec" style="display:none;">
                <div class="wrap-fill wrap-20">
                  <h4 class="aps-m0">Second Person</h4>
                </div>
                <div class="dob_wrpr">
                  <label>Date Of Birth</label>
                  <div class="date_wrpr">
                    <div class="is-floating-label datebirth">
                      <select name="secday" id="secday">
                        <option value="0" disabled selected>DD</option>
                        <option value="01">1</option>
                        <option value="02">2</option>
                        <option value="03">3</option>
                        <option value="04">4</option>
                        <option value="05">5</option>
                        <option value="06">6</option>
                        <option value="07">7</option>
                        <option value="08">8</option>
                        <option value="09">9</option>
                        <option value="10">10</option>
                        <option value="11">11</option>
                        <option value="12">12</option>
                        <option value="13">13</option>
                        <option value="14">14</option>
                        <option value="15">15</option>
                        <option value="16">16</option>
                        <option value="17">17</option>
                        <option value="18">18</option>
                        <option value="19">19</option>
                        <option value="20">20</option>
                        <option value="21">21</option>
                        <option value="22">22</option>
                        <option value="23">23</option>
                        <option value="24">24</option>
                        <option value="25">25</option>
                        <option value="26">26</option>
                        <option value="27">27</option>
                        <option value="28">28</option>
                        <option value="29">29</option>
                        <option value="30">30</option>
                        <option value="31">31</option>
                      </select>
                    </div>
                    <div class="is-floating-label datebirth">
                      <select name="secmonth" id="secmonth">
                        <option value="MM" disabled selected>MM</option>
                        <option value="01">Jan</option>
                        <option value="02">Feb</option>
                        <option value="03">Mar</option>
                        <option value="04">Apr</option>
                        <option value="05">May</option>
                        <option value="06">Jun</option>
                        <option value="07">Jul</option>
                        <option value="08">Aug</option>
                        <option value="09">Sep</option>
                        <option value="10">Oct</option>
                        <option value="11">Nov</option>
                        <option value="12">Dec</option>
                      </select>
                    </div>
                    <div class="is-floating-label datebirth">
                      <select name="secyears" id="secyears">
                        <option value="YY" disabled selected>YY</option>
                        <option value="2001">2001</option>
                        <option value="2000">2000</option>
                        <option value="1999">1999</option>
                        <option value="1998">1998</option>
                        <option value="1997">1997</option>
                        <option value="1996">1996</option>
                        <option value="1995">1995</option>
                        <option value="1994">1994</option>
                        <option value="1993">1993</option>
                        <option value="1992">1992</option>
                        <option value="1991">1991</option>
                        <option value="1990">1990</option>
                        <option value="1989">1989</option>
                        <option value="1988">1988</option>
                        <option value="1987">1987</option>
                        <option value="1986">1986</option>
                        <option value="1985">1985</option>
                        <option value="1984">1984</option>
                        <option value="1983">1983</option>
                        <option value="1982">1982</option>
                        <option value="1981">1981</option>
                        <option value="1980">1980</option>
                        <option value="1979">1979</option>
                        <option value="1978">1978</option>
                        <option value="1977">1977</option>
                        <option value="1976">1976</option>
                        <option value="1975">1975</option>
                        <option value="1974">1974</option>
                        <option value="1973">1973</option>
                        <option value="1972">1972</option>
                        <option value="1971">1971</option>
                        <option value="1970">1970</option>
                        <option value="1969">1969</option>
                        <option value="1968">1968</option>
                        <option value="1967">1967</option>
                        <option value="1966">1966</option>
                        <option value="1965">1965</option>
                        <option value="1964">1964</option>
                        <option value="1963">1963</option>
                        <option value="1962">1962</option>
                        <option value="1961">1961</option>
                        <option value="1960">1960</option>
                        <option value="1959">1959</option>
                        <option value="1958">1958</option>
                        <option value="1957">1957</option>
                        <option value="1956">1956</option>
                        <option value="1955">1955</option>
                        <option value="1954">1954</option>
                        <option value="1953">1953</option>
                        <option value="1952">1952</option>
                        <option value="1951">1951</option>
                        <option value="1950">1950</option>
                        <option value="1949">1949</option>
                        <option value="1948">1948</option>
                        <option value="1947">1947</option>
                        <option value="1946">1946</option>
                        <option value="1945">1945</option>
                        <option value="1944">1944</option>
                        <option value="1943">1943</option>
                        <option value="1942">1942</option>
                        <option value="1941">1941</option>
                        <option value="1940">1940</option>
                        <option value="1939">1939</option>
                        <option value="1938">1938</option>
                        <option value="1937">1937</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="radio-label">
                  <p>
                    <input type="radio" id="sectest1" name="sec_gender1a" value="Male">
                    <label for="sectest1"> <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/man.svg">Male</label>
                  </p>
                  <p>
                    <input type="radio" id="sectest2" name="sec_gender1a" value="Female">
                    <label for="sectest2"> <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/woman.svg">Female</label>
                  </p>
                </div>
                <div class="radio-wrpr">
                  <label>Smoker</label>
                  <div class="radio-label">
                    <p>
                      <input type="radio" id="sectest3" name="sec_radiosmoker" value="Yes">
                      <label for="sectest3"> <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/smoker.svg">Yes</label>
                    </p>
                    <p>
                      <input type="radio" id="sectest4" name="sec_radiosmoker" value="No">
                      <label for="sectest4"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/no_smoke.svg">No</label>
                    </p>
                  </div>
                </div>
              </div>

              <div class="termandcodtn_wrpr">
                <div class="condtn_wrpr">
                  <div class="chk_box">
                    <input type="checkbox" id="terms" name="terms" />
                    <label class="cutm_chk" for="terms"></label>
                  </div>
                  <p>I have read and accept the <a href="#" class="term-business">Terms of Business</a> and <a href="#" class="privacy-statement">Privacy Statement</a></p>
                </div>
                <div class="formbtn-wrap animate-btn blue">
                  <input type="submit" class="" id="submitbtn" name="submit" onclick="" value="Get Quote" />
                  <span></span>
                </div>
              </div>
            </form>
            
            <form id="apiform" style="display:none" method="post" action="<?php echo site_url().'/thank-you/';?>">
                <input type="hidden" class="data" name="data">
                <input type="submit" value="Submit">
            </form>
            
          </div>
        </div>
      </div>
    </section>
  </div>
</div>


<?php
get_footer();
