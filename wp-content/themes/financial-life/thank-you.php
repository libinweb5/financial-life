<?php
/**
  Template Name: thank-you
 */
get_header();
?>
<div id="barba-wrapper">
    <div class="barba-container deposits-savings" data-namespace="popup">
        <div class="fl-container">
            <div class="thanku-wrap">

                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/thumbs-up.png">
                <h3>Thank you for requesting a Quote with Financial Life</h3>
                <p> We will be in touch with your tailored quote in the next 24 hours.</p>
                <div class="thanku_contact">
                    <div id="20" class="">
                    </div>
                </div>

                <?php
                $data = $_POST['data'];
                $quotes = unserialize(base64_decode($data));
                $output = '';
                if (!empty($quotes)) {
                    $output .= '<h3>Your Quotes:</h3>';
                    $output .= '<table style="width:100%;max-width: 50%;margin: 30px auto;">';
                    $output .= '<tr><td style="width:35%;"><u>Provider</td><td><u>Premium</td></tr>';
                    foreach ($quotes as $company => $quote) {
                        $output .= '<tr><td>' . $company . '</td><td>' . $quote . '</td></tr>';
                    }
                    $output .= '</table>';
                } else {
                    $output .= '<div>No results found</div>';
                }

                echo $output;
                ?>
                <div class="mpc-button">
                    <a href="<?php echo home_url(); ?>" class="anotherquote">Get Another Quote</a>
                    <a href="<?php echo home_url(); ?>" class="backtohome">Back to Homepage</a>
                </div>
            </div>

        </div>
    </div>
</div>
<?php
get_footer();
