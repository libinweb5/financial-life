<?php

/**
  Template Name: about-us
*/


get_header();
?>

<div id="barba-wrapper">
  <div class="barba-container about-us" data-namespace="about-us">
    <main class="pension_overlay">
      <section class="banner about-banner">
        <div class="abt_banner_contnt">
          <h1><?php the_field('inner_banner_content');?></h1>
          <p><?php the_field('inner_banner_sub_content');?></p>
          <img class="about_bg" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/about_bg.svg">
          <div class="abt_bnr_wrpr">
            <?php if( have_rows('about_banner_images') ): ?>
            <?php while( have_rows('about_banner_images') ): the_row(); ?>
            <img src="<?php the_sub_field('banner_image');?>" data-srcset="<?php the_sub_field('banner_image');?>">
            </li>
            <?php endwhile; ?>
            <?php endif; ?>
          </div>
        </div>
      </section>
    </main>

    <section class="about-section ceo">
      <div class="fl-container">
        <div class="ceo_wrpr">
          <?php the_field('about_main_contents');?>
        </div>
        <!--
        <div class="ceo_blk">
          <h4><?php the_field('ceo_message_content');?></h4>
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/comma.svg">
          <div class="img_wrpr">
            <img src="<?php the_field('ceo_image');?>">
            <div class="contnt_wrpr">
              <h4><?php the_field('ceo_name');?></h4>
              <p><?php the_field('ceo_designation');?></p>
            </div>
          </div>
        </div>
        -->
      </div>
    </section>


<!--
    <section class="mortgage-protection-cover section-2 mortgage-protection_scroll_box mission_vision-wrap">
      <span class="cover_bg"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/mortgage_cover.svg"></span>
      <div class="fl-container">
        <h3><?php the_field('mission_vision_title');?></h3>

        <div class="cover-slider" id="cover-slider">
          <div class="item cover_item">

            <img src="<?php the_field('mission_icon');?>">
            <h4><?php the_field('mission_title');?></h4>

            <p><?php the_field('mission_content');?></p>
          </div>
          <div class="item cover_item">
            <img src="<?php the_field('vision_icon');?>">
            <h4><?php the_field('vision_title');?></h4>
            <p><?php the_field('vision__content');?></p>
          </div>
        </div>
      </div>
    </section>
-->


    <!--
    <section class="about-section our_mission">
      <div class="fl-container">
        <div class="insure_life_wrpr">
          <div class="insure_img">
            <img src="<?php the_field('insure_main_images');?>">
          </div>
          <div class="inusre_cntnt">
            <h3><?php the_field('insure_main_titles');?></h3>
            <p><?php the_field('insure_sub_titles');?></p>

            <?php if( have_rows('insure_list') ): ?>
            <?php while( have_rows('insure_list') ): the_row(); ?>
            <div class="insure_inner_blk">
              <img src="<?php the_sub_field('list_icon');?>">
              <div class="inner_blk_cntnt">
                <p><span><?php the_sub_field('list_title');?></span> <?php the_sub_field('list_content');?></p>
              </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>

            <a class="animate-btn" href="javascript:void(0)">Learn More<span></span></a>
          </div>
        </div>
      </div>
    </section>
    -->

    <section class="about-section team parterns">
      <div class="fl-container">
        <div class="team_wrpr">

          <!--
          <div class="team_grp_wrpr">
            <img src="<?php the_field('insure_team_banner_image');?>" data-srcset="<?php the_field('insure_team_banner_image');?>">
          </div>
          -->

          <div class="grp_members_wrpr">
            <h3 class=""><?php the_field('management_team_title');?></h3>
            <p><?php the_field('management_team_sub_title');?></p>
            <?php 
                query_posts(array( 
                  'post_type' => 'management', 
                  'order' => 'ASC',
                  'posts_per_page' => '3',
                ) );  
             ?>
            <div class="membrs_wrpr">
              <?php while (have_posts()) : the_post(); ?>
              <div class="members_blk">
                <?php the_post_thumbnail();?>
                <div class="membr_contnt">
                  <div class="dp_details">
                    <h4><?php the_title(); ?></h4>
                    <p><?php the_field('designation');?></p>
                    <a href="<?php the_permalink(); ?>" class="restr">
                      <span class="readbio">Contact Now</span>
                    </a>
                  </div>
                </div>
                <div class="team_member_bio"><?php the_sub_field('team_member_bio');?></div>
              </div>
              <?php endwhile; wp_reset_query(); ?>
            </div>
            <!-- end all about items -->
          </div>
        </div>
      </div>
    </section>

    <!--
    <section class="about-section parterns">
      <div class="fl-container">
        <div class="key_wrpr">
          <div class="key_cntnt">
            <h4><?php the_field('business_partners_title');?></h4>
            <p><?php the_field('business_partners_content');?>
          </div>

          <div class="key_silder" id="key_slider">
            <?php if( have_rows('business_partners') ): ?>
            <?php while( have_rows('business_partners') ): the_row(); ?>
            <div class="item key_item">
              <img src="<?php the_sub_field('partners_img');?>">
            </div>
            <?php endwhile; ?>
            <?php endif; ?>

          </div>
        </div>
      </div>
    </section>
    -->

    <!-- Modal -->

    <!--
    <div class="modal fade teammodal" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            </button>
          </div>
          <div class="modal-body">
            <div class="dp_wrpr">

            </div>
          </div>
        </div>
      </div>
    </div>
  -->

  </div>
</div>


<?php
get_footer();
