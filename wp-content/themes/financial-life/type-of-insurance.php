<?php

/**
  Template Name: type-of-insurance
*/


get_header();
?>

<div id="barba-wrapper">
  <div class="barba-container mortgage-protection" data-namespace="mortgage-protection">
    <section class="banner mortgage-protection-banner" style="background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/images/blue_banner.png);">
      <div class="fl-container">
        <div class="inner_banner_contnt">
          <div class="banner_data">
            <h1>Type of Insurance</h1>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi.</p>
            <div class="get_q_wrpr">
              <a class="animate-btn blue" href="<?php echo get_page_link( get_page_by_path( 'contact-us' ) ); ?>">Contact Us<span></span></a>
              <a class="animate-btn blue" href="demo.webandcrafts.com/wp-financial-life/">Get Quote<span></span></a>
            </div>
          </div>
          <div class="banner_img">
            <img src="<?php the_field('inner_banner_icon'); ?>">
          </div>
        </div>
      </div>
    </section>



    <section class="single_joint-dual-cover type_insurance_section type_insurance_section-inner">
      <div class="fl-container">
    
        <?php if( have_rows('types_of_life_insurance') ): ?>
        <?php while( have_rows('types_of_life_insurance') ): the_row(); ?>

        <div class="types_life_insurance-data">
          <div class="mpi_img_wrpr">
            <img src="<?php the_sub_field('types_insurance_img');?>">
          </div>
          <div class="mpi_content_wrpr">
            <h4><?php the_sub_field('insurance_types_head');?></h4>
            <div>
              <?php the_sub_field('insurance_types_content');?>
            </div>
          </div>
        </div>
        <?php endwhile; ?>
        <?php endif; ?>

      </div>
    </section>


  </div>
</div>


<?php
get_footer();
