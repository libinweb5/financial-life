<?php

/**
  Template Name: life-and-serious-illness-cover
*/


get_header();
?>


<div id="barba-wrapper">
  <div class="barba-container mortgage-protection" data-namespace="mortgage-protection">
    <section class="banner mortgage-protection-banner" style="background-image: url(<?php the_field('banner_background_image'); ?>);">
      <div class="fl-container">
        <div class="inner_banner_contnt">
          <div class="banner_data">
            <h1><?php the_field('inner_banner_title');?></h1>
              <p><?php the_field('inner_banner_sub_content');?></p>
            <div class="get_q_wrpr">
              <a class="animate-btn blue" href="<?php echo get_page_link( get_page_by_path( 'contact-us' ) ); ?>">Contact Us<span></span></a>
              <a class="animate-btn blue" href="<?php the_field('get_quote_url');?>">Get Quote<span></span></a>
            </div>
          </div>
          <div class="banner_img">
            <img src="<?php the_field('inner_banner_icon'); ?>">
          </div>
        </div>
      </div>
    </section>

    <section class="mpi-section section-3">
      <div class="fl-container">
        <div class="mpi_wrpr">
          <div class="mpi_img_wrpr">
            <img src="<?php the_field('mpi_main_image');?>">
          </div>
          <div class="mpi_contnt_wrpr">
            <h3><?php the_field('mpi_title');?></h3>
            <p><?php the_field('mpi_sub_content');?></p>
            <ul>
              <?php if( have_rows('mpi_list') ): ?>
              <?php while( have_rows('mpi_list') ): the_row(); ?>
              <li><span><img src="<?php the_sub_field('list_icon');?>"></span> <b><?php the_sub_field('list_title');?></b>
                <p><?php the_sub_field('list_detail_content');?></p>
              </li>
              <?php endwhile; ?>
              <?php endif; ?>
            </ul>
            <a class="animate-btn blue" href="<?php the_field('get_quote_url');?>">Get Quote<span></span></a>
          </div>
        </div>
      </div>
    </section>


    <section class="mortgage-protection-cover section-2 illness_scrolling_box">
      <span class="cover_bg"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/mortgage_cover.svg"></span>
      <div class="fl-container">
        <h3><?php the_field('protection_title');?></h3>
        <p><?php the_field('protection__sub_content');?></p>
        <div class="cover-slider" id="cover-slider">
          <?php if( have_rows('protection__slide_content') ): ?>
          <?php while( have_rows('protection__slide_content') ): the_row(); ?>
          <a href="javascript:void(0)" data-toggle="modal" data-target="#qamodal" class="qamodal_cls">
            <div class="item cover_item">
              <img src="<?php the_sub_field('icon');?>">
              <h4><?php the_sub_field('title');?></h4>
              <div>
                <?php
                  $content = get_sub_field('details');
                  $short_content = implode(' ', array_slice(explode(' ', $content), 0, 14));
                ?>
                <div>
                  <div class="short_cont"><?php echo $short_content;?></div>
                  <div class="more_content" style="display:none;"><?php echo $content;?></div>
                </div>
              </div>
            </div>
          </a>
          <?php endwhile; ?>
          <?php endif; ?>
        </div>
      </div>
    </section>

    <div class="modal fade " id="qamodal" tabindex="-1" role="dialog" aria-labelledby="qamodalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            </button>
          </div>
          <div class="modal-body">
            <div class="qst_ans_wrap">
            </div>
          </div>
        </div>
      </div>
    </div>

    <?php
get_footer();
?>
