<?php

/**
  Template Name: mortgages
*/


get_header();
?>

<div id="barba-wrapper">
  <div class="barba-container mortgages" data-namespace="mortgages">
    <section class="mort-popup mortgages section-1">
      <div class="popup_contnt">
        <div class="fl-container">
          <div class="form_head">
            <div class="">
              <h4>Mortgage Application</h4>
              <p>You can apply for a mortgage right now by filling out this form. Application entries will
                not be processed unless items marked required are filled in.</p>
            </div>
          </div>
          <div class="modal-body">

            <form action="" id="mortgages-form" method="POST">
              <input type="hidden" name="action" value="mortgagesform" />
              <!--  <input type="hidden" name="protection_type" value="mortgagesform" id="protection_type" />-->
              <div class="radio-label">
                <p>
                  <input type="radio" id="test1" name="applicant_type" value="Single">
                  <label for="test1"> <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/single.svg">Single Applicant</label>
                </p>
                <p>
                  <input type="radio" id="test2" name="applicant_type" value="Joints">
                  <label for="test2"> <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/duble.svg">Joint Application</label>
                </p>
              </div>

              <!-- Single Applicant -->

              <div class="single_applicant">
                <h4>Single Applicant</h4>
              </div>

              <div class="is-floating-label">
                <label for="name">Name</label>
                <input type="text" id="name" name="name" />
              </div>
              <div class="is-floating-label">
                <label for="email">Email</label>
                <input type="text" id="email" name="email" />
              </div>
              <div class="is-floating-label">
                <label for="contact">Contact Number</label>
                <input type="tex t" id="contact" name="contact" />
              </div>
              <div class="is-floating-label text">
                <label for="postadd">Postal Address</label>
                <textarea type="text" id="postadd" name="postadd"></textarea>
              </div>


              <div class="single_applicant">
                <h4>Second Applicant (if applicable)</h4>
              </div>



              <div class="second_applicant">
                <p>Second Applicant Details (required if applying jointly)<span></span></p>
                <!-- Second Applicant (if applicable) -->
                <div class="is-floating-label">
                  <label for="secs_name">Name (required if applying jointly)</label>
                  <input type="text" id="secs_name" name="secs_name" />
                </div>
                <div class="is-floating-label">
                  <label for="secs_email">Email (required if applying jointly)</label>
                  <input type="text" id="secs_email" name="secs_email" />
                </div>
                <div class="is-floating-label">
                  <label for="secs_contact">Contact Number (required if applying jointly)</label>
                  <input type="text" id="secs_contact" name="secs_contact" />
                </div>
              </div>

              <!-- Mortgage Details -->

              <div class="single_applicant">
                <h4>Mortgage Details</h4>
              </div>

              <div class="is-floating-label">
                <select id="buy" name="buy">
                  <option disabled selected>What type of Mortgage are you looking for?</option>
                  <option>First time buyer</option>
                  <option>Second Time Buyer</option>
                  <option>Refinance/remortgage</option>
                  <option>Mover/|Extender</option>
                  <option>Investor/Buy to let</option>
                </select>
              </div>

              <div class="is-floating-label">
                <label>What is the price of the property you want to purchase?</label>
                <input type="text" name="price_property" id="price_property" />
              </div>
              <div class="is-floating-label">
                <label>What loan amount are you looking for?</label>
                <input type="text" id="load_amount" name="load_amount" />
              </div>

              <div class="single_applicant">
                <h4>Existing Property Details (if applicable)</h4>
              </div>

              <div class="second_applicant">
                <p>Existing Property Details<span></span></p>
                <div class="is-floating-label">
                  <select name="type_property" id="type_property">
                    <option disabled selected>What type of property is it ?</option>
                    <option>Semi Detached</option>
                    <option>Detached</option>
                    <option>Terraced</option>
                    <option>Apartment</option>
                    <option>Bungalow</option>
                  </select>
                </div>


                <div class="is-floating-label">
                  <select name="credit_rating" id="credit_rating">
                    <option disabled selected>What would you describe your credit rating?</option>
                    <option>Excellent</option>
                    <option>Very Good</option>
                    <option>Fair</option>
                    <option>Poor</option>
                  </select>
                </div>

                <div class="is-floating-label text">
                  <label>What is your current property address?</label>
                  <textarea type="text" id="property_address" name="property_address"></textarea>
                </div>


                <div class="is-floating-label">
                  <label>What was it’s original purchase price?</label>
                  <input type="text" id="purchase_price" name="purchase_price" />
                </div>
                <div class="is-floating-label">
                  <label>What year did you purchase it?</label>
                  <input type="text" name="year_purchase" id="year_purchase" />
                </div>
                <div class="is-floating-label">
                  <label>What is the amount you owe on it at present?</label>
                  <input type="text" name="amount_owe" id="amount_owe" />
                </div>
                <div class="is-floating-label">
                  <label>What is your current monthly mortgage payment on this property?</label>
                  <input type="text" id="monthly_mortgage" name="monthly_mortgage" />
                </div>
                <div class="is-floating-label">
                  <label>What is it’s estimated present value?</label>
                  <input type="text" id="estimated_value" name="estimated_value" />
                </div>

              </div>

              <!-- First Applicant -->

              <div class="single_applicant">
                <h3>Employment, Income & Credit History</h3>
                <h4>First Applicant</h4>
              </div>

              <div class="is-floating-label">
                <label for="cremployment_single">Current Employer</label>
                <input type="text" name="cremployment_single" id="cremployment_single" />
              </div>
              <div class="is-floating-label">
                <label for="time_employment_single">Time with current employer (years)</label>
                <input type="text" name="time_employment_single" id="time_employment_single" />
              </div>
              <div class="is-floating-label">
                <label for="annual_income_single">Annual Income (EUR)</label>
                <input type="text" name="annual_income_single" id="annual_income_single" />
              </div>
              <div class="is-floating-label">
                <select name="credit_rating" id="credit_rating">
                  <option disabled selected>How would you describe your credit rating?</option>
                  <option>How would you describe your credit rating?</option>
                  <option>How would you describe your credit rating?</option>
                  <option>How would you describe your credit rating?</option>
                </select>
              </div>

              <div class="single_applicant">
                <h4>Second Applicant (if applicable)</h4>
              </div>

              <div class="second_applicant">
                <p>Second Applicant Details (required if applying jointly)<span></span></p>

                <div class="is-floating-label">
                  <label for="current_employer">Current Employer</label>
                  <input type="text" name="current_employer" id="current_employer" />
                </div>
                <div class="is-floating-label">
                  <label for="current_employer_time">Time with current employer (years)</label>
                  <input type="text" name="current_employer_time" id="current_employer_time" />
                </div>
                <div class="is-floating-label">
                  <label for="annual_income">Annual Income (EUR)</label>
                  <input type="text" id="annual_income" name="annual_income" />
                </div>
                <div class="is-floating-label">
                  <select id="sec_credit_rating" name="sec_credit_rating">
                    <option disabled selected>How would you describe your credit rating?</option>
                    <option>How would you describe your credit rating?</option>
                    <option>How would you describe your credit rating?</option>
                    <option>How would you describe your credit rating?</option>
                  </select>
                </div>

              </div>


              <div class="single_applicant">
                <h4>Getting the most from Financial Life</h4>
              </div>

              <div class="is-floating-label">
                <select name="besttime_contact" id="besttime_contact">
                  <option disabled selected>Best time to contact you</option>
                  <option>9AM -12 NOON</option>
                  <option>12 NOON - 3 PM</option>
                  <option>3 PM - 6.30 PM</option>
                </select>
              </div>

              <div class="termandcodtn_wrpr">
                <div class="condtn_wrpr">
                  <div class="chk_box">
                    <input type="checkbox" id="terms" name="terms" />
                    <label class="cutm_chk" for="terms"></label>
                  </div>
                  <p>I/We declare that the information given in this form is true and complete to the best
                    of my/our knowledge and
                    belief, whether completed by me or otherwise and all facts relevant to this
                    application have been disclosed.</p>
                </div>
              </div>
              <div class="decltion_btn mortgages_footbtn">
                <!-- Google reCAPTCHA box -->

                <div id="recaptcha-element" class=""></div>
                <div class="g-recaptcha" data-sitekey="6Ld7mtkUAAAAAEO-U0KZE4nMlu17ISe4sZvC-1UM" data-callback="recaptchaCallback"></div>
                <input type="hidden" class="hiddenRecaptcha required" name="hiddenRecaptcha" id="hiddenRecaptcha">

                <div class="mortgages_btn"><input type="submit" class="animate-btn blue" id="submitbtn" name="submit" value="Send" /></div>
                <span></span>
              </div>
              <span id="recaptcha-error" style="display:none; color: red;font-size: 14px;">The CAPTCHA field is required.</span>
            </form>
          </div>
        </div>
      </div>
    </section>
  </div>
</div>
<script>
  function recaptchaCallback() {
    $('#hiddenRecaptcha').valid();
  };

</script>

<?php
get_footer();
