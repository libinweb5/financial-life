<?php

/**
  Template Name: life-insurance
*/


get_header();
?>

<div id="barba-wrapper">
  <div class="barba-container mortgage-protection" data-namespace="mortgage-protection">
    <section class="banner mortgage-protection-banner" style="background-image: url(<?php the_field('banner_background_image'); ?>);">
      <div class="fl-container">
        <div class="inner_banner_contnt">
          <div class="banner_data">
            <h1><?php the_field('inner_banner_title');?></h1>
            <p><?php the_field('inner_banner_sub_content');?></p>
            <div class="get_q_wrpr">
              <a class="animate-btn blue" href="<?php echo get_page_link( get_page_by_path( 'contact-us' ) ); ?>">Contact Us<span></span></a>
              <a class="animate-btn blue" href="<?php the_field('get_quote_url');?>">Get Quote<span></span></a>
            </div>
          </div>
          <div class="banner_img">
            <img src="<?php the_field('inner_banner_icon'); ?>">
          </div>
        </div>
      </div>
    </section>

    <section class="mortgage-protection-cover section-2 life-insurance_scroll-box">
      <span class="cover_bg"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/mortgage_cover.svg"></span>
      <div class="fl-container">
        <h3><?php the_field('protection_title');?></h3>
        <p><?php the_field('protection__sub_content');?></p>
        <div class="cover-slider" id="cover1-slider">
          <?php if( have_rows('protection__slide_content') ): ?>
          <?php while( have_rows('protection__slide_content') ): the_row(); ?>
          <a href="<?php the_permalink(); ?>" data-toggle="modal" data-target="#insuranceModal" class="readbio">
            <div class="item cover_item">
              <img src="<?php the_sub_field('icon');?>">
              <h4><?php the_sub_field('title');?></h4>
              <div class="modal-content-display" style="display:none;">
                <?php the_sub_field('details');?>
              </div>
            </div>
          </a>
          <?php endwhile; ?>
          <?php endif; ?>
        </div>
        <!-- <div class="animate-btn blue">-->
        <!--  <a  href="<?php the_field('more_button_link');?>">Learn More</a>-->
          
        <!--</div>-->
        <div class="contactbtn-wrap animate-btn blue slider-button w-50 mx-auto">
              <!--<a href="http://demo.webandcrafts.com/wp-financial-life/contact-us">Contact Now</a>-->
                  <a  href="<?php the_field('more_button_link');?>">Learn More</a>
              <span></span>
        </div>
       </div>
       
    </section>


    <section class="mpi-section section-3 life-insurance-mpi-section">
      <div class="fl-container">
        <div class="mpi_wrpr">
          <div class="mpi_img_wrpr">
            <img src="<?php the_field('mpi_main_image');?>">
          </div>
          <div class="mpi_contnt_wrpr">
            <h3><?php the_field('mpi_title');?></h3>
            <p><?php the_field('mpi_sub_content');?></p>
            <ul>
              <?php if( have_rows('mpi_list') ): ?>
              <?php while( have_rows('mpi_list') ): the_row(); ?>
              <li><span><img src="<?php the_sub_field('list_icon');?>"></span> <b><?php the_sub_field('list_title');?></b>
                <p><?php the_sub_field('list_detail_content');?></p>
              </li>
              <?php endwhile; ?>
              <?php endif; ?>
            </ul>
            <a class="animate-btn blue" href="<?php the_field('get_quote_url');?>">Get Quote<span></span></a>
          </div>
        </div>
      </div>
    </section>


    <section class="mortgage-protection-cover section-2 life-insurance-cost-section">
      <span class="cover_bg"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/mortgage_cover.svg"></span>
      <div class="fl-container">
        <h3><?php the_field('insurance_cost_main_title');?></h3>
        <p><?php the_field('insurance_cost_subtitle');?></p>
        <div class="cover-slider" id="insurance-cost">
          <?php if( have_rows('insurance_cost_items') ): ?>
          <?php while( have_rows('insurance_cost_items') ): the_row(); ?>
          <div class="item cover_item">
            <img src="<?php the_sub_field('insurance_cost_item_icon');?>">
            <h4><?php the_sub_field('insurance_cost_item_title');?></h4>
            <p><?php the_sub_field('insurance_cost_item_content');?></p>
          </div>
          <?php endwhile; ?>
          <?php endif; ?>
        </div>
      </div>
    </section>

    <!--
    <section class="single_joint-dual-cover">
      <div class="fl-container">
        <div class="">
          <h3><?php the_field('joint_and_dual_cover_title');?></h3>
          <p>
            <?php the_field('joint_and_dual_cover_content');?>
          </p>
        </div>
      </div>
    </section>
-->


    <!--
    <section class="single_joint-dual-cover type_insurance_section mpi-section ">
      <div class="fl-container">
        <div class="">
          <h3><?php the_field('types_of_life_insurance_title');?></h3>
          <p><?php the_field('types_of_life_insurance');?></p>
          <a class="animate-btn blue" href="<?php the_field('more_button_link');?>">Learn More</a>
        </div>
      </div>
    </section>
-->


    <div class="modal fade teammodal" id="insuranceModal" tabindex="-1" role="dialog" aria-labelledby="insuranceModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            </button>
          </div>
          <div class="modal-body">
            <div class="dp_wrpr">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php
get_footer();
