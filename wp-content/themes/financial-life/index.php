<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage financial-life
 * @since 1.0.0
 */
get_header();
?>

<div id="barba-wrapper">
  <div class="barba-container deposits-savings" data-namespace="home">
    <main class="home_overlay">
      <section class="banner home-banner section-1">
        <div class="banner_contnt">
          <div class="banner-slider" id="banner-slider">
            <?php if( have_rows('banner_slide') ): ?>
            <?php while( have_rows('banner_slide') ): the_row(); ?>
            <div class="item banner-slide-item ">
              <img src="<?php the_sub_field('slider_image');?>" data-srcset="<?php the_sub_field('slider_image');?>">
              <div class="fl-container">
                <div class="owl_title fl-container h100 d-flex flex-vcenter">
                  <div class="banner-ttle">
                    <h1><?php the_sub_field('banner_content');?></h1>
                    <a class="d-flex flex-vcenter" href="<?php the_sub_field('know_more_button_link');?>">
                      <span><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/right-arrow-stroke.svg"></span><?php the_sub_field('know_more_button');?></a>
                  </div>
                  <div class="banner_box">
                    <h2>Select Your Cover</h2>
                    <form id="protection_select">
                      <input type="hidden" name="action" value="protection_box" />
                      <div class="banner_box_select">
                        <select id="protection_box" name="protection_box">
                          <!-- <option value="0" disabled selected>Select Your Cover</option>-->
                          <option value="form">Mortgage Protection</option>
                          <option value="form">Life Insurance</option>
                          <option value="form">Life & Serious Illness</option>
                          <option value="form">Serious Illness</option>
                          <option value="form">Income Protection</option>
                          <option value="form">Whole of Life-Cover</option>
                        </select>
                      </div>

                      <!--
                      <a class="animate-btn blue" href="mortgage-protection-form.php">Get Quote
                      <span></span></a>
                      -->
                      <div class="protection_box-btn">
                        <button type="button" id="buttonId" class="animate-btn blue">Get Quote
                          <span></span>
                        </button>

                      </div>

                    </form>
                  </div>
                </div>
              </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
          </div>
          <div class="banner_steps">
            <p>Life Insurance in 3 Simple Steps</p>
            <span><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/arrow-down-2.svg"></span>
          </div>
        </div>
      </section>
    </main>

    <!-- Banner section end here -->

    <section class="home-section section-2">
      <div class="fl-container">
        <div class="steps_wrpr">
          <?php if( have_rows('life_insurance_steps') ): ?>
          <?php while( have_rows('life_insurance_steps') ): the_row(); ?>
          <div class="step_blk">
            <img src="<?php the_sub_field('step_icon');?>">
            <div class="steps_cntnt">
              <p><?php the_sub_field('step_count');?></p>
              <h4><?php the_sub_field('step_title');?></h4>
              <p><?php the_sub_field('step_paragraph');?></p>
            </div>
          </div>
          <?php endwhile; ?>
          <?php endif; ?>
        </div>
      </div>
    </section>


    <section class="home-section section-3">
      <div class="fl-container">
        <div class="insure_life_wrpr">
          <div class="insure_img">
            <img src="<?php the_field('insure_your_life_image');?>">
          </div>
          <div class="inusre_cntnt">
            <h3><?php the_field('insure_your_life_title');?></h3>
            <?php if( have_rows('insure_your_life') ): ?>
            <?php while( have_rows('insure_your_life') ): the_row(); ?>
            <div class="insure_inner_blk">
              <img src="<?php the_sub_field('insure_your_life_sub_icon');?>">
              <div class="inner_blk_cntnt">
                <p><span><?php the_sub_field('insure_your_life_sub_title');?></span> - <?php the_sub_field('insure_your_life_content');?></p>
              </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>

            <a class="animate-btn" href="<?php the_field('learn_more_link');?>"><?php the_field('learn_more');?><span></span></a>
          </div>
        </div>
      </div>
    </section>


    <section class="home-section section-4">
      <div class="slik_main_wrpr">
        <!--        <div class="fl-container">-->
        <div class="blue_wrpr">
          <h4><?php the_field('best_quote_title');?></h4>
          <div class="blue_arrw">
            <span class="cust_prev"></span>
            <span class="cust_next"></span>
          </div>
          <img class="bg move-element	" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/back_dble.svg">
        </div>
        <div class="blue_slik" id="blue_slik">
          <?php if( have_rows('protection') ): ?>
          <?php while( have_rows('protection') ): the_row(); ?>
          <div class="item blue_item">
            <img src="<?php the_sub_field('protection_img');?>">
            <h4><?php the_sub_field('protection_title');?></h4>
            <p><?php the_sub_field('protection_content');?></p>
            <a class="animate-btn" href="<?php the_sub_field('protection_button_link');?>"><?php the_sub_field('protection_button');?><span></span></a>
            <a class="animate-btn" href="">Get Quote<span></span></a>
          </div>
          <?php endwhile; ?>
          <?php endif; ?>
        </div>
        <!--        </div>-->
      </div>
    </section>

    <section class="home-section section-5">
      <div class="fl-container">
        <div class="key_wrpr">
          <div class="key_cntnt">
            <h4><?php the_field('business_partners_title');?></h4>
            <p><?php the_field('business_partners_content');?>
            </p>
          </div>
          <div class="key_silder" id="key_slider">
            <?php if( have_rows('business_partners') ): ?>
            <?php while( have_rows('business_partners') ): the_row(); ?>
            <div class="item key_item">
              <img src="<?php the_sub_field('partners_img');?>">
            </div>
            <?php endwhile; ?>
            <?php endif; ?>

          </div>
        </div>
      </div>
    </section>


    <section class="home-section section-6">
      <div class="fl-container">
        <div class="testimonial_wrpr">
          <div class="testimonial_vdo_wrpr">
            <h4><?php the_field('testimonials_home_page_title', 225); ?>
            </h4>
            <div class="vedio_wrpr">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/testi_vedio.webp" data-srcset="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/testi_vedio.png">
            </div>
            <div class="testi_arrw">
              <span class="cust_prev"></span>
              <span class="cust_next"></span>
            </div>
          </div>
          <div class="clents_comments testimonial-slider" id="testimonial-slider">
            <?php if( have_rows('testimonials', 225)): ?>
            <?php while( have_rows('testimonials', 225)): the_row(); ?>
            <div class="item testi_item">
              <p><?php the_sub_field('client_comments');?></p>
              <p class="client_name"><?php the_sub_field('client_name');?></p>
              <p class="client_dsgntn"><?php the_sub_field('client_designation');?></p>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
          </div>
        </div>
        <div class="wpcr3_show_btn">
          <a href="javascript:void(0);" data-toggle="modal" data-target="#user_review">Create your own review</a>
        </div>
      </div>
    </section>

    <!--
    <section class="home-section section-6">
      <div class="fl-container">
        <div class="blue_clients">
          <h4><?php the_field('growing_businesses');?></h4>
          <span class="circle_1 move-element"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/ellipse-circle.svg"></span>
          <span class="circle_2 move-element"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/ellipse-circle-2.svg"></span>
        </div>
      </div>
    </section>
-->

    <section class="home-section home_contact section-7">
      <div class="fl-container">
        <div class="home_contact_main_wrpr">
          <h3><?php the_field('start_financial_life');?></h3>
          <div class="home_contact_wrpr">

            <div class="online_wrpr">
              <img src="<?php the_field('online_icon');?>">
              <h4><?php the_field('caption');?></h4>
              <p><?php the_field('online_paragraph_text');?></p>
              <a href="<?php echo get_page_link( get_page_by_path( 'contact-us' ) ); ?>" class="animate-btn blue">Get Quote <span></span></a>
            </div>

            <div class="call_wrpr">
              <img src="<?php the_field('call_icon');?>">
              <h4><?php the_field('call_caption');?></h4>
              <p><?php the_field('call_paragraph_text');?></p>
              <a class="phone-num animate-btn" href="tel: (01) 256 0430"><svg xmlns="http://www.w3.org/2000/svg" width="15.102" height="15.118" viewBox="0 0 15.102 15.118">
                  <path id="Path_5" data-name="Path 5" d="M1343.751-167.105a10.875,10.875,0,0,1-5.855-1.879,13.05,13.05,0,0,1-5.946-8.512,6.9,6.9,0,0,1,.116-3.276,3.119,3.119,0,0,1,.309-.728,1.218,1.218,0,0,1,1.64-.6,5.616,5.616,0,0,1,3.054,3.062,1.671,1.671,0,0,1-.5,1.976c-.082.076-.167.149-.25.223a.858.858,0,0,0-.2,1.314,11.439,11.439,0,0,0,3.259,3.554c.239.169.483.332.733.486.709.438.962.4,1.508-.211a1.854,1.854,0,0,1,2.608-.463,6.2,6.2,0,0,1,2.427,2.484,1.4,1.4,0,0,1-.806,2.185,3.9,3.9,0,0,1-.842.24C1344.534-167.174,1344.06-167.139,1343.751-167.105Z" transform="translate(-1331.807 182.223)" fill="#25A9DE" />
                </svg><?php the_field('contact_number', 'option'); ?><span></span></a>
            </div>

          </div>

        </div>
      </div>
    </section>


    <script>
      jQuery("body").mousemove(function(e) {

        parallaxIt(e, ".move-element", -100);
      });

      function parallaxIt(e, target, movement) {
        var $this = $("body");
        var relX = e.pageX - $this.offset().left;
        var relY = e.pageY - $this.offset().top;

        TweenMax.to(target, 1, {
          x: (relX - $this.width() / 10) / $this.width() * movement,
          y: (relY - $this.height() / 10) / $this.height() * movement
        });
      }

    </script>


    <div id="user_review" class="modal fade webmodal feedmodal user_review_wrap" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            </button>
          </div>
          <div class="modal-body" id="career-submit">
            <h2>User Review</h2>
            <!-- <span class="alumni-login-hd">Job Add</span> -->

            <form action="#" class="modal-form webform" method="POST" id="form-share1" enctype="multipart/form-data">
              <div class="form-group">
                <input type="text" id="username" name="username" placeholder="Name"/ required>
              </div>
              <div class="form-group">
                <input type="text" id="useremail" name="useremail" placeholder="Email"/ required>
              </div>
              <div class="form-group">
                <input type="text" id="website" name="website" placeholder="Website">
              </div>
              <div class="form-group">
                <input type="text" id="reviewtitle" name="reviewtitle" placeholder="Review Title">
              </div>

              <div class="share-item career_alumni_thanks" id="share-4" style="disply:none;">
                <div class="sub-heading share-idea">
                  <h3>Thank you for submitting your Comment.</h3>
                  <p>Your submission is being reviewed.</p>
                </div>
              </div>
              <button type="submit" class="btn_web_fill submit-btn career-submit" data-count="0" value="careers"><?php _e('Submit', 'alumni_careers') ?></button>
            </form>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>

<?php
get_footer();
