<?php

/**
  Template Name: find-my-pension
*/


get_header();
?>

<div id="barba-wrapper">
  <div class="barba-container terms pension_bg" data-namespace="terms">
    <main>
      <div class="fl-container">
        <div class="pension_wrpr">
          <div class="pension_wrap-img">
            <?php if (has_post_thumbnail( $post->ID ) ): ?>
            <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
            <img src="<?php echo $image[0]; ?>" alt="">
          </div>
          <?php endif; ?>
          <div class="pension_wrap-content">
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post();?>
            <h1><?php the_title();?></h1>
            <?php the_content();?>
            <?php endwhile; else: ?>
            <p>Sorry, no posts matched your criteria.</p>
            <?php endif; ?>
            <div class="contactbtn-wrap animate-btn blue">
              <a href="<?php echo get_page_link( get_page_by_path( 'contact-us' ) ); ?>">Contact Now</a>
              <span></span>
            </div>
<!--
            <div class="contactbtn-wrap animate-btn blue">
              <a href="<?php echo home_url(); ?>">Get Quote<span></span></a>
              <span></span>
            </div>
-->
          </div>
        </div>
      </div>
  </div>
  </main>
</div>
</div>

<?php
get_footer();
