<?php

/*Template Name: Blog */


get_header(); ?>




<div id="barba-wrapper">
  <div class="barba-container blog" data-namespace="blog">
    <section class="banner mortgage-protection-banner testimonial_banner blog_banner" style="background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/images/blue_bg.png);">
      <div class="fl-container">
        <div class="inner_banner_contnt">
          <div class="banner_data">
            <?php
            $args = array(
                'post_type' => 'post',
                'posts_per_page' => '1'
            );

            $post_query = new WP_Query($args);
              if($post_query->have_posts() ) {
                while($post_query->have_posts() ) {
                  $post_query->the_post();
                  $categories_p = get_the_terms(get_the_ID(), 'category');
                  $post_date = get_the_date( 'F j, Y' );
                  ?>
            <p class="latst_blog">Latest Blog</p>
            <h1><?php the_title(); ?></h1>
            <p class="insu_date"><span><?php echo $categories_p[0]->name; ?></span><span><?= $post_date;?></span></p>
            <a class="d-flex flex-vcenter" href="<?php the_permalink(); ?>"><span><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/right-arrow-stroke.svg"></span>Know More</a>

            <?php
                }
              }
              ?>
          </div>
        </div>
      </div>
    </section>

    <section class="blog_section">
      <div class="fl-container">
        <div class="filter_wrpr">
          <form>
            <select>
              <option value="*">Select category</option>
              <?php
                    $taxonomy     = 'category';
                    $orderby      = 'name';  
                    $args = array(
                        'taxonomy'     => $taxonomy,
                        'orderby'      => $orderby,
                    );
                    ?>
              <?php
                    $all_categories = get_categories($args);

                    foreach ($all_categories as $cat) {

                        $taxonomy = $cat->slug;
                        $orderby = 'name';
                        $args = array(
                            'taxonomy' => $taxonomy,
                            'orderby' => $orderby,
                        );
                        $category = get_queried_object();
                        $categort_slug = $cat->slug;
                        $categort_name = $cat->name;
                    
                        ?>

              <option value="<?= $categort_name ?>"><?= $categort_name ?></option>

              <?php
                        $i++;
                    }
                    ?>
            </select>
          </form>
        </div>

        <div class="blog_wrpr">
          <?php
            $args = array(
                'post_type' => 'post',
                'posts_per_page' => -1
            );

            $post_query = new WP_Query($args);
              if($post_query->have_posts() ) {
                while($post_query->have_posts() ) {
                  $post_query->the_post();
                  $categories_p = get_the_terms(get_the_ID(), 'category');
                  $post_date = get_the_date( 'F j, Y' );
                  $max_num_pages= $post_query->max_num_pages;
                  $found_posts= $post_query->found_posts;
                  ?>

          <a href="<?php the_permalink(); ?>" class="blog_blk <?php echo $categories_p[0]->name; ?>">
            <?php if (has_post_thumbnail( $post->ID ) ): ?>
            <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
            <div class="blog_wrap_img"><img src="<?php echo $image[0]; ?>" alt=""></div>
            <?php endif; ?>
            <?php
            $category_detail=get_the_category('4');//$post->ID
            foreach($category_detail as $cd){
            echo $cd->cat_name;
            }
                  ?>
            <p class="blog_title"><span><?php echo $categories_p[0]->name; ?></span><span><?= $post_date;?></span></p>
            <h4><?php the_title(); ?></h4>
          </a>
          <?php
                }
              }
              ?>
              
            <!--
            <div class="blog_btn  blog_btn-wrap">
            <a href="javascript:void(0)" class="animate-btn blue" href="javascript:void(0)">Load More
            <span></span>
            </a>
            </div>
            -->

        </div>
      </div>
    </section>
    <section class="home-section home_contact faq_contact ">
      <div class="fl-container">
        <div class="home_contact_main_wrpr">
          <h3><?php the_field('start_financial_life', 607);?></h3>
          <div class="home_contact_wrpr">

            <div class="online_wrpr">
              <img src="<?php the_field('online_icon', 607);?>">
              <h4><?php the_field('caption', 607);?></h4>
              <p><?php the_field('online_paragraph_text', 607);?></p>
              <a href="<?php echo get_page_link( get_page_by_path( 'contact-us' ) ); ?>" class="animate-btn blue">Get Quote <span></span></a>
            </div>

            <div class="call_wrpr">
              <img src="<?php the_field('call_icon', 607);?>">
              <h4><?php the_field('call_caption', 607 );?></h4>
              <p><?php the_field('call_paragraph_text', 607);?></p>
              <a class="phone-num animate-btn" href="tel: (01) 256 0430"><svg xmlns="http://www.w3.org/2000/svg" width="15.102" height="15.118" viewBox="0 0 15.102 15.118">
                  <path id="Path_5" data-name="Path 5" d="M1343.751-167.105a10.875,10.875,0,0,1-5.855-1.879,13.05,13.05,0,0,1-5.946-8.512,6.9,6.9,0,0,1,.116-3.276,3.119,3.119,0,0,1,.309-.728,1.218,1.218,0,0,1,1.64-.6,5.616,5.616,0,0,1,3.054,3.062,1.671,1.671,0,0,1-.5,1.976c-.082.076-.167.149-.25.223a.858.858,0,0,0-.2,1.314,11.439,11.439,0,0,0,3.259,3.554c.239.169.483.332.733.486.709.438.962.4,1.508-.211a1.854,1.854,0,0,1,2.608-.463,6.2,6.2,0,0,1,2.427,2.484,1.4,1.4,0,0,1-.806,2.185,3.9,3.9,0,0,1-.842.24C1344.534-167.174,1344.06-167.139,1343.751-167.105Z" transform="translate(-1331.807 182.223)" fill="#25A9DE" />
                </svg><?php the_field('contact_number', 'option'); ?><span></span></a>
            </div>

          </div>

        </div>
      </div>
    </section>
  </div>
</div>





<?php get_footer(); ?>
