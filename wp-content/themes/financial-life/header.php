<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage financial-life
 * @since 1.0.0
 */
?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
  <title>Financial Life</title>
  
  <meta name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover" />
  <script src="https://www.google.com/recaptcha/api.js" async defer></script>
  <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
  <?php wp_body_open(); ?>
  <header class="main-header logged fl_header">
    <div class="fl-container">
      <div class="head-logo">
        <div class="logo">
          <a class="logo_white" title="Financial Life Logo" href="<?php echo home_url();?>"><img src="<?php the_field('logo_image_white', 'option'); ?>"></a>
          <a class="logo_black" title="Financial Life Logo" href="<?php echo home_url();?>"><img src="<?php the_field('logo_image_black', 'option'); ?>"></a>
        </div>
      </div>
      <div class="head-menu">
        <nav class="main-nav">
          <ul>
            <li class="mob_class">
              <a class="logo_black" href="index.php">
                <img src="<?php the_field('logo_image_black', 'option'); ?>">
              </a>
            </li>

            <?php
                wp_nav_menu( array( 'theme_location' => 'main-menu', 'container' => false ) );
              ?>

            <li class="mob_class">
              <a class="phone-num animate-btn" href="tel: (01) 256 0430">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/call-white.svg"><?php the_field('contact_number', 'option'); ?><span></span>
              </a>
              <a class="phone-callnow animate-btn blue" href="<?php echo get_page_link( get_page_by_path( 'contact-us' ) ); ?>">
                Get Quote
                <span></span>
              </a>
            </li>
            <li class="mob_class socialmedia">
              <a href="javascript:void(0)">
                <svg xmlns="http://www.w3.org/2000/svg" width="7.991" height="14.779" viewBox="0 0 7.991 14.779">
                  <g id="facebook-logo_h" opacity="0.489">
                    <path id="Path_96_h" data-name="Path 96" d="M29.767,0,27.85,0a3.366,3.366,0,0,0-3.545,3.637V5.314H22.378a.3.3,0,0,0-.3.3v2.43a.3.3,0,0,0,.3.3h1.927v6.131a.3.3,0,0,0,.3.3h2.514a.3.3,0,0,0,.3-.3V8.347h2.253a.3.3,0,0,0,.3-.3V5.616a.3.3,0,0,0-.3-.3H27.422V3.893c0-.683.163-1.03,1.053-1.03h1.291a.3.3,0,0,0,.3-.3V.3A.3.3,0,0,0,29.767,0Z" transform="translate(-22.077)" />
                  </g>
                </svg>
              </a>
              <a href="javascript:void(0)">
                <svg xmlns="http://www.w3.org/2000/svg" width="13.511" height="13.481" viewBox="0 0 13.511 13.481">
                  <g id="XMLID_801_h" opacity="0.489">
                    <path id="XMLID_802_h" d="M7.857,99.73H5.145a.218.218,0,0,0-.218.218v8.713a.218.218,0,0,0,.218.218H7.857a.218.218,0,0,0,.218-.218V99.948A.218.218,0,0,0,7.857,99.73Z" transform="translate(-4.712 -95.398)" />
                    <path id="XMLID_803_h" d="M1.79.341A1.788,1.788,0,1,0,3.578,2.129,1.791,1.791,0,0,0,1.79.341Z" transform="translate(0 -0.341)" />
                    <path id="XMLID_804_h" d="M111.592,94.761a3.186,3.186,0,0,0-2.383,1V95.2a.218.218,0,0,0-.218-.218h-2.6a.218.218,0,0,0-.218.218v8.713a.218.218,0,0,0,.218.218H109.1a.218.218,0,0,0,.218-.218V99.6c0-1.453.395-2.019,1.407-2.019,1.1,0,1.191.907,1.191,2.093v4.236a.218.218,0,0,0,.218.218h2.707a.218.218,0,0,0,.218-.218V99.129C115.059,96.969,114.647,94.761,111.592,94.761Z" transform="translate(-101.548 -90.646)" />
                  </g>
                </svg>
              </a>
              <a href="javascript:void(0)">
                <svg xmlns="http://www.w3.org/2000/svg" width="15.803" height="12.834" viewBox="0 0 15.803 12.834">
                  <g id="twitter-black-shape_h" opacity="0.489">
                    <path id="Path_97" data-name="Path 97" d="M15.8,43.783a6.463,6.463,0,0,1-1.865.5A3.145,3.145,0,0,0,15.361,42.5a6.357,6.357,0,0,1-2.056.782,3.247,3.247,0,0,0-4.658-.075A3.121,3.121,0,0,0,7.7,45.5a3.594,3.594,0,0,0,.08.742,9.025,9.025,0,0,1-3.725-1A9.182,9.182,0,0,1,1.1,42.85a3.246,3.246,0,0,0-.05,3.179A3.235,3.235,0,0,0,2.1,47.182a3.213,3.213,0,0,1-1.464-.411v.04a3.13,3.13,0,0,0,.737,2.061,3.186,3.186,0,0,0,1.86,1.118,3.356,3.356,0,0,1-.852.11,4.132,4.132,0,0,1-.612-.05A3.246,3.246,0,0,0,4.8,52.3,6.341,6.341,0,0,1,.781,53.679,6.894,6.894,0,0,1,0,53.639a8.995,8.995,0,0,0,4.973,1.454,9.462,9.462,0,0,0,3.238-.546A8.231,8.231,0,0,0,10.8,53.083a9.8,9.8,0,0,0,1.85-2.111,9.412,9.412,0,0,0,1.158-2.492,9.2,9.2,0,0,0,.381-2.6q0-.281-.01-.421A6.782,6.782,0,0,0,15.8,43.783Z" transform="translate(0.001 -42.259)" />
                  </g>
                </svg>
              </a>
            </li>
          </ul>
        </nav>

        <div class="hdr-contact">
          <a class="phone-num animate-btn" href="tel: (01) 256 0430"><svg xmlns="http://www.w3.org/2000/svg" width="15.102" height="15.118" viewBox="0 0 15.102 15.118">
              <path id="Path_5h" data-name="Path 5" d="M1343.751-167.105a10.875,10.875,0,0,1-5.855-1.879,13.05,13.05,0,0,1-5.946-8.512,6.9,6.9,0,0,1,.116-3.276,3.119,3.119,0,0,1,.309-.728,1.218,1.218,0,0,1,1.64-.6,5.616,5.616,0,0,1,3.054,3.062,1.671,1.671,0,0,1-.5,1.976c-.082.076-.167.149-.25.223a.858.858,0,0,0-.2,1.314,11.439,11.439,0,0,0,3.259,3.554c.239.169.483.332.733.486.709.438.962.4,1.508-.211a1.854,1.854,0,0,1,2.608-.463,6.2,6.2,0,0,1,2.427,2.484,1.4,1.4,0,0,1-.806,2.185,3.9,3.9,0,0,1-.842.24C1344.534-167.174,1344.06-167.139,1343.751-167.105Z" transform="translate(-1331.807 182.223)" fill="#25A9DE" />
            </svg> <?php the_field('contact_number', 'option'); ?><span></span></a>
          <a class="phone-callnow animate-btn blue" href="<?php echo get_page_link( get_page_by_path( 'contact-us' ) ); ?>">Get Quote<span></span></a>
        </div>
      </div>
    </div>

    <svg class="ham hamRotate ham8 mob-btn" viewBox="0 0 100 100" width="70" onclick="this.classList.toggle('active')">
      <path class="line top" d="m 30,33 h 40 c 3.722839,0 7.5,3.126468 7.5,8.578427 0,5.451959 -2.727029,8.421573 -7.5,8.421573 h -20">
      </path>
      <path class="line middle" d="m 30,50 h 40"></path>
      <path class="line bottom" d="m 70,67 h -40 c 0,0 -7.5,-0.802118 -7.5,-8.365747 0,-7.563629 7.5,-8.634253 7.5,-8.634253 h 20">
      </path>
    </svg>

    <div class="circle"></div>
  </header>
