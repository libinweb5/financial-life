<?php

/**
* Template Name: 404
*/

get_header(); ?>

<div class="wrap">
  <div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
      <section class="error-404 not-found">
        <div class="container">
          <div class="page-not-found">
            <div class="empty-page-bg">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/404-02.png" alt="">
            </div>
            <div class="message-box">
              <h1><?php _e( '404', 'saec' ); ?></h1>
              <p><?php _e( 'Oops! That page can&rsquo;t be found.', 'financial life' ); ?></p>
              <div class="buttons-con">
                <div class="action-link-wrap">
                  <a href="javascript:history.go(-1)" class="link-back-button">Go Back</a>
                  <a href="<?php echo home_url(); ?>" class="link-button-home">Go to Home Page</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </main>
  </div>
</div>

<?php
get_footer();
