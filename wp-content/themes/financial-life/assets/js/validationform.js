// Client Form

$(document).on("submit", "#client-form", function (e) {
  e.preventDefault();
  data1 = new FormData($(this)[0]);
  $.ajax({
    type: 'POST',
    url: ajaxObj.ajaxurl,
    data: data1,
    processData: false,
    contentType: false,
    success: function (data) {
      $("#myElem").show();
      setTimeout(function () {
        $("#myElem").hide();
      }, 5000);
      $("#client-form")[0].reset();
    }
  });
});


// Contact Form

$(document).on("submit", "#contact-form", function (e) {
  e.preventDefault();
  data1 = new FormData($(this)[0]);
  $.ajax({
    type: 'POST',
    url: ajaxObj.ajaxurl,
    data: data1,
    processData: false,
    contentType: false,
    success: function (data) {
      $("#myElem").show();
      setTimeout(function () {
        $("#myElem").hide();
      }, 5000);
    }
  });
});



// Mortgage Protection cover Form

$(document).on("submit", "#mortgage-protection-form", function (e) {
  e.preventDefault();
  var form = $(this);
  data1 = new FormData($(this)[0]);
  $.ajax({
    type: 'POST',
    url: ajaxObj.ajaxurl,
    data: data1,
    processData: false,
    contentType: false,
    success: function (result) {
        var res = $.parseJSON(result);
        
        if(res.status == 1) {
            $('#apiform .data').val(res.data);
            $('#apiform').submit();
        }
        
    }
  });
});



// Serious Illness Form

$(document).on("submit", "#serious-illness-form", function (e) {
  e.preventDefault();
  data1 = new FormData($(this)[0]);
  $.ajax({
    type: 'POST',
    url: ajaxObj.ajaxurl,
    data: data1,
    processData: false,
    contentType: false,
    success: function (result) {
        if(result == '1') {
            window.location = "http://demo.webandcrafts.com/wp-financial-life/thank-you/";
        }
    }
  });
});


// Life Insurance cover Form

$(document).on("submit", "#insurance-quote", function (e) {
  e.preventDefault();
  data1 = new FormData($(this)[0]);
  $.ajax({
    type: 'POST',
    url: ajaxObj.ajaxurl,
    data: data1,
    processData: false,
    contentType: false,
    success: function (result) {
      window.location = "http://demo.webandcrafts.com/wp-financial-life/thank-you/";
    }
  });
});



// Life and Serious Illness Quote

$(document).on("submit", "#life-and-serious-illness-quote", function (e) {
  e.preventDefault();
  data1 = new FormData($(this)[0]);
  $.ajax({
    type: 'POST',
    url: ajaxObj.ajaxurl,
    data: data1,
    processData: false,
    contentType: false,
    success: function (result) {
      window.location = "http://demo.webandcrafts.com/wp-financial-life/thank-you/";
    }
  });
});


// Income Protection Form

$(document).on("submit", "#income-cover-form", function (e) {
  e.preventDefault();
  data1 = new FormData($(this)[0]);
  $.ajax({
    type: 'POST',
    url: ajaxObj.ajaxurl,
    data: data1,
    processData: false,
    contentType: false,
    success: function (result) {
      window.location = "http://demo.webandcrafts.com/wp-financial-life/thank-you/";
    }
  });
});


// Whole of Life Insurance Form

$(document).on("submit", "#whole-of-life-insurance-form", function (e) {
  e.preventDefault();
  data1 = new FormData($(this)[0]);
  $.ajax({
    type: 'POST',
    url: ajaxObj.ajaxurl,
    data: data1,
    processData: false,
    contentType: false,
    success: function (result) {
      window.location = "http://demo.webandcrafts.com/wp-financial-life/thank-you/";
    }
  });
});



// Investment Form

$(document).on("submit", "#investment", function (e) {
  e.preventDefault();
  data1 = new FormData($(this)[0]);
  $.ajax({
    type: 'POST',
    url: ajaxObj.ajaxurl,
    data: data1,
    processData: false,
    contentType: false,
    success: function (result) {
//       alert(result);
      window.location = "http://demo.webandcrafts.com/wp-financial-life/thank-you/";
    }
  });
});


// Pension Plan Form

$(document).on("submit", "#pension-form", function (e) {
  e.preventDefault();
  data1 = new FormData($(this)[0]);
  $.ajax({
    type: 'POST',
    url: ajaxObj.ajaxurl,
    data: data1,
    processData: false,
    contentType: false,
    success: function (result) {
      window.location = "http://demo.webandcrafts.com/wp-financial-life/thank-you/";
    }
  });
});


// Mortgages Form

$(document).on("submit", "#mortgages-form", function (e) {
  e.preventDefault();
  data1 = new FormData($(this)[0]);
  $.ajax({
    type: 'POST',
    url: ajaxObj.ajaxurl,
    data: data1,
    processData: false,
    contentType: false,
    success: function (result) {
      window.location = "http://demo.webandcrafts.com/wp-financial-life/thank-you/";
    }
  });
});
