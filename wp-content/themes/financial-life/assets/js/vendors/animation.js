/*!
 * VERSION: 1.7.5
 * DATE: 2015-02-26
 * UPDATES AND DOCS AT: http://greensock.com
 *
 * @license Copyright (c) 2008-2015, GreenSock. All rights reserved.
 * This work is subject to the terms at http://greensock.com/standard-license or for
 * Club GreenSock members, the software agreement that was issued with your membership.
 *
 * @author: Jack Doyle, jack@greensock.com
 **/
var controller = new ScrollMagic.Controller();

function fullAnim(){
    var _gsScope = "undefined" != typeof module && module.exports && "undefined" != typeof global ? global : this || window;
    (_gsScope._gsQueue || (_gsScope._gsQueue = [])).push(function() {
        "use strict";
        var t = document.documentElement,
        e = window,
        i = function(i, r) {
            var s = "x" === r ? "Width" : "Height",
                n = "scroll" + s,
                a = "client" + s,
                o = document.body;
            return i === e || i === t || i === o ? Math.max(t[n], o[n]) - (e["inner" + s] || t[a] || o[a]) : i[n] - i["offset" + s]
        },
        r = _gsScope._gsDefine.plugin({
            propName: "scrollTo",
            API: 2,
            version: "1.7.5",
            init: function(t, r, s) {
                return this._wdw = t === e, this._target = t, this._tween = s, "object" != typeof r && (r = {
                    y: r
                }), this.vars = r, this._autoKill = r.autoKill !== !1, this.x = this.xPrev = this.getX(), this.y = this.yPrev = this.getY(), null != r.x ? (this._addTween(this, "x", this.x, "max" === r.x ? i(t, "x") : r.x, "scrollTo_x", !0), this._overwriteProps.push("scrollTo_x")) : this.skipX = !0, null != r.y ? (this._addTween(this, "y", this.y, "max" === r.y ? i(t, "y") : r.y, "scrollTo_y", !0), this._overwriteProps.push("scrollTo_y")) : this.skipY = !0, !0
            },
            set: function(t) {
                this._super.setRatio.call(this, t);
                var r = this._wdw || !this.skipX ? this.getX() : this.xPrev,
                    s = this._wdw || !this.skipY ? this.getY() : this.yPrev,
                    n = s - this.yPrev,
                    a = r - this.xPrev;
                this._autoKill && (!this.skipX && (a > 7 || -7 > a) && i(this._target, "x") > r && (this.skipX = !0), !this.skipY && (n > 7 || -7 > n) && i(this._target, "y") > s && (this.skipY = !0), this.skipX && this.skipY && (this._tween.kill(), this.vars.onAutoKill && this.vars.onAutoKill.apply(this.vars.onAutoKillScope || this._tween, this.vars.onAutoKillParams || []))), this._wdw ? e.scrollTo(this.skipX ? r : this.x, this.skipY ? s : this.y) : (this.skipY || (this._target.scrollTop = this.y), this.skipX || (this._target.scrollLeft = this.x)), this.xPrev = this.x, this.yPrev = this.y
            }
        }),
        s = r.prototype;
    r.max = i, s.getX = function() {
        return this._wdw ? null != e.pageXOffset ? e.pageXOffset : null != t.scrollLeft ? t.scrollLeft : document.body.scrollLeft : this._target.scrollLeft
    }, s.getY = function() {
        return this._wdw ? null != e.pageYOffset ? e.pageYOffset : null != t.scrollTop ? t.scrollTop : document.body.scrollTop : this._target.scrollTop
    }, s._kill = function(t) {
        return t.scrollTo_x && (this.skipX = !0), t.scrollTo_y && (this.skipY = !0), this._super._kill.call(this, t)
    }
}), _gsScope._gsDefine && _gsScope._gsQueue.pop()();



var controller = new ScrollMagic.Controller();

function zoomout(x, y){

    var anim_elem = x;

    var trig_elem = y;

    var zoomout1 = TweenMax.staggerFromTo(anim_elem, 1, {scale:1.1}, {scale:1}, 0.5);

    new ScrollMagic.Scene({triggerElement: trig_elem,reverse: false,triggerHook: 'onEnter'}).setTween(zoomout1).addTo(controller);

}


function fadein(x, y) {
    var anim_elem = x;
    var trig_elem = y;
    var fadein1 = TweenMax.staggerFromTo(anim_elem, 1, {
        opacity: 0
    }, {
        opacity: 1
    }, 0.2);
    new ScrollMagic.Scene({
        triggerElement: trig_elem,
        reverse: false,
        triggerHook: 'onEnter'
    }).setTween(fadein1).addTo(controller);
}

function fadeinup(x, y) {
    var anim_elem = x;
    var trig_elem = y;
    var fadeinup1 = TweenMax.staggerFromTo(anim_elem, 0.8, {
        opacity: 0,
        y: 20
    }, {
        y: 0,
        opacity: 1,
        force3D: true
    }, 0.2);
    new ScrollMagic.Scene({
        triggerElement: trig_elem,
        reverse: false,
        triggerHook: 'onEnter',
        offset: 250
    }).setTween(fadeinup1).addTo(controller);
}

function fadeinleft(x,y){

    var anim_elem = x;

    var trig_elem = y;

    var fadeinleft1 = TweenMax.staggerFromTo(anim_elem, 0.8, {x:-20}, {x:0,opacity:1,force3D:true}, 0.2);

    new ScrollMagic.Scene({triggerElement: trig_elem,reverse: false,triggerHook: 'onEnter', offset: 50}).setTween(fadeinleft1).addTo(controller);

}

function fadeinright(x, y) {
    var anim_elem = x;
    var trig_elem = y;
    var fadeinright1 = TweenMax.staggerFromTo(anim_elem, 0.8, {
        x: 20
    }, {
        x: 0,
        opacity: 1,
        force3D: true
    }, 0.2);
    new ScrollMagic.Scene({
        triggerElement: trig_elem,
        reverse: false,
        triggerHook: 'onEnter',
        offset: 100
    }).setTween(fadeinright1).addTo(controller);
}

/*body scroll smoothness*/

// ! function() {
//     function e() {
//         z.keyboardSupport && f("keydown", a)
//     }

//     function t() {
//         if (!A && document.body) {
//             A = !0;
//             var t = document.body,
//                 o = document.documentElement,
//                 n = window.innerHeight,
//                 r = t.scrollHeight;
//             if (B = document.compatMode.indexOf("CSS") >= 0 ? o : t, D = t, e(), top != self) X = !0;
//             else if (r > n && (t.offsetHeight <= n || o.offsetHeight <= n)) {
//                 var a = document.createElement("div");
//                 a.style.cssText = "position:absolute; z-index:-10000; top:0; left:0; right:0; height:" + B.scrollHeight + "px", document.body.appendChild(a);
//                 var i;
//                 T = function() {
//                     i || (i = setTimeout(function() {
//                         L || (a.style.height = "0", a.style.height = B.scrollHeight + "px", i = null)
//                     }, 500))
//                 }, setTimeout(T, 10), f("resize", T);
//                 var l = {
//                     attributes: !0,
//                     childList: !0,
//                     characterData: !1
//                 };
//                 if (M = new V(T), M.observe(t, l), B.offsetHeight <= n) {
//                     var c = document.createElement("div");
//                     c.style.clear = "both", t.appendChild(c)
//                 }
//             }
//             z.fixedBackground || L || (t.style.backgroundAttachment = "scroll", o.style.backgroundAttachment = "scroll")
//         }
//     }

//     function o() {
//         M && M.disconnect(), h(I, r), h("mousedown", i), h("keydown", a), h("resize", T), h("load", t)
//     }

//     function n(e, t, o) {
//         if (p(t, o), 1 != z.accelerationMax) {
//             var n = Date.now(),
//                 r = n - R;
//             if (r < z.accelerationDelta) {
//                 var a = (1 + 50 / r) / 2;
//                 a > 1 && (a = Math.min(a, z.accelerationMax), t *= a, o *= a)
//             }
//             R = Date.now()
//         }
//         if (q.push({
//                 x: t,
//                 y: o,
//                 lastX: 0 > t ? .99 : -.99,
//                 lastY: 0 > o ? .99 : -.99,
//                 start: Date.now()
//             }), !P) {
//             var i = e === document.body,
//                 l = function() {
//                     for (var n = Date.now(), r = 0, a = 0, c = 0; c < q.length; c++) {
//                         var u = q[c],
//                             d = n - u.start,
//                             s = d >= z.animationTime,
//                             m = s ? 1 : d / z.animationTime;
//                         z.pulseAlgorithm && (m = x(m));
//                         var f = u.x * m - u.lastX >> 0,
//                             h = u.y * m - u.lastY >> 0;
//                         r += f, a += h, u.lastX += f, u.lastY += h, s && (q.splice(c, 1), c--)
//                     }
//                     i ? window.scrollBy(r, a) : (r && (e.scrollLeft += r), a && (e.scrollTop += a)), t || o || (q = []), q.length ? _(l, e, 1e3 / z.frameRate + 1) : P = !1
//                 };
//             _(l, e, 0), P = !0
//         }
//     }

//     function r(e) {
//         A || t();
//         var o = e.target,
//             r = u(o);
//         if (!r || e.defaultPrevented || e.ctrlKey) return !0;
//         if (w(D, "embed") || w(o, "embed") && /\.pdf/i.test(o.src) || w(D, "object")) return !0;
//         var a = -e.wheelDeltaX || e.deltaX || 0,
//             i = -e.wheelDeltaY || e.deltaY || 0;
//         return K && (e.wheelDeltaX && b(e.wheelDeltaX, 120) && (a = -120 * (e.wheelDeltaX / Math.abs(e.wheelDeltaX))), e.wheelDeltaY && b(e.wheelDeltaY, 120) && (i = -120 * (e.wheelDeltaY / Math.abs(e.wheelDeltaY)))), a || i || (i = -e.wheelDelta || 0), 1 === e.deltaMode && (a *= 40, i *= 40), !z.touchpadSupport && v(i) ? !0 : (Math.abs(a) > 1.2 && (a *= z.stepSize / 120), Math.abs(i) > 1.2 && (i *= z.stepSize / 120), n(r, a, i), e.preventDefault(), void l())
//     }

//     function a(e) {
//         var t = e.target,
//             o = e.ctrlKey || e.altKey || e.metaKey || e.shiftKey && e.keyCode !== N.spacebar;
//         document.contains(D) || (D = document.activeElement);
//         var r = /^(textarea|select|embed|object)$/i,
//             a = /^(button|submit|radio|checkbox|file|color|image)$/i;
//         if (r.test(t.nodeName) || w(t, "input") && !a.test(t.type) || w(D, "video") || y(e) || t.isContentEditable || e.defaultPrevented || o) return !0;
//         if ((w(t, "button") || w(t, "input") && a.test(t.type)) && e.keyCode === N.spacebar) return !0;
//         var i, c = 0,
//             d = 0,
//             s = u(D),
//             m = s.clientHeight;
//         switch (s == document.body && (m = window.innerHeight), e.keyCode) {
//             case N.up:
//                 d = -z.arrowScroll;
//                 break;
//             case N.down:
//                 d = z.arrowScroll;
//                 break;
//             case N.spacebar:
//                 i = e.shiftKey ? 1 : -1, d = -i * m * .9;
//                 break;
//             case N.pageup:
//                 d = .9 * -m;
//                 break;
//             case N.pagedown:
//                 d = .9 * m;
//                 break;
//             case N.home:
//                 d = -s.scrollTop;
//                 break;
//             case N.end:
//                 var f = s.scrollHeight - s.scrollTop - m;
//                 d = f > 0 ? f + 10 : 0;
//                 break;
//             case N.left:
//                 c = -z.arrowScroll;
//                 break;
//             case N.right:
//                 c = z.arrowScroll;
//                 break;
//             default:
//                 return !0
//         }
//         n(s, c, d), e.preventDefault(), l()
//     }

//     function i(e) {
//         D = e.target
//     }

//     function l() {
//         clearTimeout(E), E = setInterval(function() {
//             F = {}
//         }, 1e3)
//     }

//     function c(e, t) {
//         for (var o = e.length; o--;) F[j(e[o])] = t;
//         return t
//     }

//     function u(e) {
//         var t = [],
//             o = document.body,
//             n = B.scrollHeight;
//         do {
//             var r = F[j(e)];
//             if (r) return c(t, r);
//             if (t.push(e), n === e.scrollHeight) {
//                 var a = s(B) && s(o),
//                     i = a || m(B);
//                 if (X && d(B) || !X && i) return c(t, $())
//             } else if (d(e) && m(e)) return c(t, e)
//         } while (e = e.parentElement)
//     }

//     function d(e) {
//         return e.clientHeight + 10 < e.scrollHeight
//     }

//     function s(e) {
//         var t = getComputedStyle(e, "").getPropertyValue("overflow-y");
//         return "hidden" !== t
//     }

//     function m(e) {
//         var t = getComputedStyle(e, "").getPropertyValue("overflow-y");
//         return "scroll" === t || "auto" === t
//     }

//     function f(e, t) {
//         window.addEventListener(e, t, !1)
//     }

//     function h(e, t) {
//         window.removeEventListener(e, t, !1)
//     }

//     function w(e, t) {
//         return (e.nodeName || "").toLowerCase() === t.toLowerCase()
//     }

//     function p(e, t) {
//         e = e > 0 ? 1 : -1, t = t > 0 ? 1 : -1, (Y.x !== e || Y.y !== t) && (Y.x = e, Y.y = t, q = [], R = 0)
//     }

//     function v(e) {
//         return e ? (O.length || (O = [e, e, e]), e = Math.abs(e), O.push(e), O.shift(), clearTimeout(H), H = setTimeout(function() {
//             window.localStorage && (localStorage.SS_deltaBuffer = O.join(","))
//         }, 1e3), !g(120) && !g(100)) : void 0
//     }

//     function b(e, t) {
//         return Math.floor(e / t) == e / t
//     }

//     function g(e) {
//         return b(O[0], e) && b(O[1], e) && b(O[2], e)
//     }

//     function y(e) {
//         var t = e.target,
//             o = !1;
//         if (-1 != document.URL.indexOf("www.youtube.com/watch"))
//             do
//                 if (o = t.classList && t.classList.contains("html5-video-controls")) break;
//         while (t = t.parentNode);
//         return o
//     }

//     function S(e) {
//         var t, o, n;
//         return e *= z.pulseScale, 1 > e ? t = e - (1 - Math.exp(-e)) : (o = Math.exp(-1), e -= 1, n = 1 - Math.exp(-e), t = o + n * (1 - o)), t * z.pulseNormalize
//     }

//     function x(e) {
//         return e >= 1 ? 1 : 0 >= e ? 0 : (1 == z.pulseNormalize && (z.pulseNormalize /= S(1)), S(e))
//     }

//     function k(e) {
//         for (var t in e) C.hasOwnProperty(t) && (z[t] = e[t])
//     }
//     var D, M, T, E, H, C = {
//             frameRate: 150,
//             animationTime: 400,
//             stepSize: 100,
//             pulseAlgorithm: !0,
//             pulseScale: 4,
//             pulseNormalize: 1,
//             accelerationDelta: 50,
//             accelerationMax: 3,
//             keyboardSupport: !0,
//             arrowScroll: 50,
//             touchpadSupport: !1,
//             fixedBackground: !0,
//             excluded: ""
//         },
//         z = C,
//         L = !1,
//         X = !1,
//         Y = {
//             x: 0,
//             y: 0
//         },
//         A = !1,
//         B = document.documentElement,
//         O = [],
//         K = /^Mac/.test(navigator.platform),
//         N = {
//             left: 37,
//             up: 38,
//             right: 39,
//             down: 40,
//             spacebar: 32,
//             pageup: 33,
//             pagedown: 34,
//             end: 35,
//             home: 36
//         },
//         q = [],
//         P = !1,
//         R = Date.now(),
//         j = function() {
//             var e = 0;
//             return function(t) {
//                 return t.uniqueID || (t.uniqueID = e++)
//             }
//         }(),
//         F = {};
//     window.localStorage && localStorage.SS_deltaBuffer && (O = localStorage.SS_deltaBuffer.split(","));
//     var I, _ = function() {
//             return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || function(e, t, o) {
//                 window.setTimeout(e, o || 1e3 / 60)
//             }
//         }(),
//         V = window.MutationObserver || window.WebKitMutationObserver || window.MozMutationObserver,
//         $ = function() {
//             var e;
//             return function() {
//                 if (!e) {
//                     var t = document.createElement("div");
//                     t.style.cssText = "height:10000px;width:1px;", document.body.appendChild(t); {
//                         var o = document.body.scrollTop;
//                         document.documentElement.scrollTop
//                     }
//                     window.scrollBy(0, 1), e = document.body.scrollTop != o ? document.body : document.documentElement, window.scrollBy(0, -1), document.body.removeChild(t)
//                 }
//                 return e
//             }
//         }(),
//         U = window.navigator.userAgent,
//         W = /Edge/.test(U),
//         G = /chrome/i.test(U) && !W,
//         J = /safari/i.test(U) && !W,
//         Q = /mobile/i.test(U),
//         Z = (G || J) && !Q;
//     "onwheel" in document.createElement("div") ? I = "wheel" : "onmousewheel" in document.createElement("div") && (I = "mousewheel"), I && Z && (f(I, r), f("mousedown", i), f("load", t)), k.destroy = o, window.SmoothScrollOptions && k(window.SmoothScrollOptions), "object" == typeof exports ? module.exports = k : window.SmoothScroll = k
// }();


/*header Animation*/






$(document).ready(function() {
    $('.featuredWrap').addClass('animate_div');
    var footer_sub = TweenMax.staggerFromTo('.contactWraper .row > div', 0.9, {
        y: 30
    }, {
        y: 0,
        opacity: 1,
        force3D: true
    }, 0.3);
    new ScrollMagic.Scene({
        triggerElement: '.contactWraper',
        reverse: false,
        triggerHook: 'onEnter',
        offset: 250
    }).setTween(footer_sub).addTo(controller);




    var h_section2 = $('.home-banner');
    var fl_about = $('.about-us');
    // var h_about = $('.about_us');
    // var h_srvice = $('.services');
    // var h_news = $('.news_events');
    // var h_subsc = $('.pre_footer_subscribe');
    // var footer = $('footer');
    // var po_loc = $('.responsive-tabs');
    // var nav_channels = $('.nav-channels');

    // var visin_misin = $('.vision_wrpr');
    // var managmnt = $('.photo_section');
    // var m_service = $('.marine_serives');
    // var m_off = $('.offshore_wrpr_main');
    // var m_notice = $('.notice_mar_wrpr');
    //  var m_vessl = $('.vessl_shdle');
    //  var m_navigtn = $('.navigtn_wrpr');

    //  var e_servis = $('.e_services_wrpr');
    //  var contact_us = $('.contact_us_wrapper');





    if(h_section2.length !=0){

        var hsection6 = $('.banner_contnt .banner-slider .banner-ttle a,.banner_contnt .banner-slider .banner_box,.steps_wrpr .step_blk');
        fadeinup(hsection6,'body');

        var hsection2 = $('.insure_life_wrpr .insure_img img, .insure_life_wrpr .inusre_cntnt h3, .insure_life_wrpr .inusre_cntnt .insure_inner_blk, .insure_life_wrpr .inusre_cntnt a');
       fadeinup(hsection2,'.home-section.section-3');

       var hsection1 = $('.slik_main_wrpr .blue_wrpr h4,.slik_main_wrpr .blue_wrpr .blue_arrw');
        fadeinup(hsection1,'.home-section.section-4');

        var hsection3 = $('.key_wrpr .key_cntnt h4,.key_wrpr .key_cntnt p');
        fadeinup(hsection3,'.home-section.section-5');
        
        var hsection4 = $('.home-section.section-6 .testimonial_wrpr .testimonial_vdo_wrpr h4,.home-section.section-6 .testimonial_wrpr .testimonial_vdo_wrpr .vedio_wrpr,.slick-slide,.blue_clients h4');
        fadeinup(hsection4,'.home-section.section-6');

       var hsection5 = $('.home_contact .home_contact_main_wrpr h3,.home_contact .home_contact_main_wrpr .home_contact_wrpr .online_wrpr,.home_contact .home_contact_main_wrpr .home_contact_wrpr .call_wrpr');
        fadeinup(hsection5,'.home-section.section-7');

        var hsection7 = $('.footer .ftr_logo_wrpr,footer .ftr_nav .nav_blks,footer .ftr_contnt,footer .ftr_dwn');
        fadeinup(hsection7,'footer.fl-container');

        // var hsection1 = $('.steps_wrpr .step_blk');
        // fadeinup(hsection1,'.home-section.section-1');
    }
    
    // if(m_section3.length !=0){
    //     var mortgageprotection = $('.mortgage-protection-cover h3,.mortgage-protection-cover p,.mortgage-protection-cover .cover-slider .slick-slide,.mpi-section .fl-container .mpi_wrpr .mpi_img_wrpr,.mpi-section .fl-container .mpi_wrpr .mpi_contnt_wrpr h3,.mpi-section .fl-container .mpi_wrpr .mpi_contnt_wrpr > p,.mpi-section .fl-container .mpi_wrpr .mpi_contnt_wrpr ul li,.mpi-section .fl-container .mpi_wrpr .mpi_contnt_wrpr a,.three_steps .fl-container h3,.three_steps .fl-container .protcton_steps .protction_step_blk,.three_steps .fl-container a');
    //     fadeinup(mortgageprotection,'.mortgage-protection-cover.section-2');
    // }

    if(fl_about.length !=0){
        var about_1 = $('.about-banner .abt_banner_contnt h1,.about-banner .abt_banner_contnt p,.about-banner .abt_banner_contnt .abt_bnr_wrpr img');
        fadeinup(about_1,'body');

          var about_2 = $('.ceo .ceo_wrpr h3,.ceo .ceo_wrpr p,.ceo .ceo_blk h4,.ceo .ceo_blk .img_wrpr');
        fadeinup(about_2,'.ceo_wrpr');

          var about_3 = $('.insure_life_wrpr .insure_img,.our_mission .insure_life_wrpr .inusre_cntnt > h3,.our_mission .insure_life_wrpr .inusre_cntnt > p,.insure_life_wrpr .inusre_cntnt .insure_inner_blk,.our_mission .insure_life_wrpr .inusre_cntnt a');
        fadeinup(about_3,'.our_mission');
        
        var about_4 = $('.team .team_wrpr .grp_members_wrpr > h3,.team .team_wrpr .grp_members_wrpr > p,.team .team_wrpr .grp_members_wrpr .membrs_wrpr .members_blk');
        fadeinup(about_4,'.grp_members_wrpr');
        
        var hsection3 = $('.key_wrpr .key_cntnt h4,.key_wrpr .key_cntnt p,.key_wrpr .key_silder .key_item');
        fadeinup(hsection3,'.about-section.parterns');

        var hsection7 = $('.footer .ftr_logo_wrpr,footer .ftr_nav .nav_blks,footer .ftr_contnt,footer .ftr_dwn');
        fadeinup(hsection7,'footer.fl-container');
        
          // var hsection1 = $('.steps_wrpr .step_blk');
        // fadeinup(hsection1,'.home-section.section-1');
        
    }




    

    
    if ($(window).width() <= 1199) {
        $('.mob-btn').on('click', function() {
            if ($('html.show-menu').length == 0) {
                TweenMax.killTweensOf('.main-nav ul li');
                $('.main-nav ul li').removeAttr("style");
            } else {
                TweenMax.staggerFromTo('.main-nav ul li', 0.6, { y: 50}, { y: 0, opacity: 1, force3D: true}, 0.02);
            }
        });
    }
});




 
    var head_log = TweenMax.staggerFromTo('.search_weather', 0.5, {y:10}, {y:0,opacity:1,force3D:true}, 0.2);
    new ScrollMagic.Scene({triggerElement: '.search_weather',reverse: false,triggerHook: 'onEnter', offset: -60}).setTween(head_log).addTo(controller);
    
    var head_weath = TweenMax.staggerFromTo('header .main_nav .logo', 0.5, {y:10}, {y:0,opacity:1,force3D:true}, 0.2);
    new ScrollMagic.Scene({triggerElement: 'header .main_nav .logo',reverse: false,triggerHook: 'onEnter', offset: -60}).setTween(head_weath).addTo(controller);
    
    



}


    
 fullAnim();
