$('.main-nav ul li:has(ul)').addClass('submenu');
$('.main-nav ul li:has(ul)').append("<i></i>");
$('.main-nav ul i').click(function () {
  // $(this).parent('li').siblings().removeClass('open');
  // $(this).parent('li').toggleClass('open');
  $(this).parent('li').children('ul').slideToggle();
})

//Mobile Menu
if ($(window).width() <= 1199) {



  // $('.show-menu .main-header .mob-btn').parent('li').children('ul').slideToggle();

  $('.main-nav ul li a').click(function () {
    $('.mob-btn').click();
  });


  $(document).on("click", ".show-menu .main-header .circle", function () {
    $('.mob-btn').click();
  })

}

if ($(window).width() <= 480) {
  $(document).on("click", ".tabgroup .panel .panel_head", function () {
    $(this).parent().toggleClass("hgt");
  })

}



$('.mob-btn').click(function () {
  // $('.main-nav ul i').parent('li').children('ul').slideup();
  if (!$('html').hasClass('show-menu')) {
    $('html').addClass('show-menu');
  } else {
    $('html').removeClass('show-menu');
    $('.main-nav >ul>li>ul').slideUp();
  }
});

$('.overlay').click(function () {
  if ($('html').hasClass('show-menu')) {
    $('html').removeClass('show-menu');
  }
});

// Slim Header

var header = 0;

function scrollHead() {
  if ($(document).scrollTop() > 10) {
    if (header == 0) {
      header = 1;
      $('html').addClass('slim');
    }
  } else {
    if (header == 1) {
      header = 0;
      $('html').removeClass('slim');
    }
  }
}
scrollHead();
$(window).scroll(scrollHead);


// preventReload

function preventReload() {
  var links = document.querySelectorAll('a[href]');
  var cbk = function (e) {
    if (e.currentTarget.href === window.location.href) {
      e.preventDefault();
      e.stopPropagation();
    }
  };

  for (var i = 0; i < links.length; i++) {
    links[i].addEventListener('click', cbk);
  }
}


// multiple header
// var loc = window.location.href;
// var loc_spl = loc.split('/');
// if(loc_spl[loc_spl.length-1] == '' || loc_spl[loc_spl.length-1] == 'index.html' || loc_spl[loc_spl.length-1] == 'about-us.html'){
//    $('.rz-header').addClass('home-header');
// }
// else if(loc_spl[loc_spl.length-1] == 'newpage-1.html' || loc_spl[loc_spl.length-1] == 'newpage-2.html' || loc_spl[loc_spl.length-1] == 'newpage-3.html'){
//   $('.rz-header').addClass('admin-header');
//   $('.rz-header').addClass('inner-header');
// }
// else{
//   $('.rz-header').addClass('inner-header');
// }

// Slick slider

function slick() {
  $('#banner-slider').slick({
    draggable: true,
    arrows: false,
    dots: true,
    infinite: true,
    autoplay: true,
    loop: true,
    speed: 2000,
    slidesToShow: 1,
    slidesToScroll: 1,
    fade: true,
    responsive: [{
        breakpoint: 1450,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,

        }
      },
      {
        breakpoint: 1199,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 991,
        settings: {
          centerMode: true,
          infinite: true,
          centerPadding: '60px',
          slidesToShow: 1,
          speed: 500,
          variableWidth: false,
        }
      }, {
        breakpoint: 768,
        settings: {
          centerMode: true,
          infinite: true,
          centerPadding: '60px',
          slidesToShow: 1,
          speed: 500,
          variableWidth: false,
        }
      }
    ]

  });


  $('#blue_slik').slick({
    draggable: true,
    arrows: true,
    dots: false,
    infinite: true,
    autoplay: true,
    speed: 2000,
    slidesToShow: 4,
    slidesToScroll: 1,
    responsive: [{
        breakpoint: 1791,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,

        }
      },
      {
        breakpoint: 1323,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 2,
           slidesToScroll: 1,
        }
      }, {
        breakpoint: 768,
        settings: {
          //   centerMode: true,
          // infinite: true,
          // centerPadding: '60px',
          slidesToShow: 1,
          slidesToScroll: 1,
          // speed: 500,
          // variableWidth: false,
        }
      }
    ]

  });


  $('#key_slider').slick({
    draggable: true,
    loop: true,
    arrows: false,
    dots: true,
    infinite: true,
    autoplay: true,
    speed: 2000,
    slidesToShow: 4,
    slidesToScroll: 1,
    responsive: [{
        breakpoint: 1450,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 1,

        }
      },
      {
        breakpoint: 1199,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        }
      }, {
        breakpoint: 768,
        settings: {
          //   centerMode: true,
          // infinite: true,
          // centerPadding: '60px',
          slidesToShow: 1,
          slidesToScroll: 1,
          // speed: 500,
          // variableWidth: false,
        }
      }
    ]

  })


  $('#testimonial-slider').slick({
    draggable: true,
    arrows: true,
    dots: false,
    infinite: true,
    autoplay: true,
    speed: 2000,
    slidesToShow: 1,
    slidesToScroll: 1,
    responsive: [{
        breakpoint: 1450,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,

        }
      },
      {
        breakpoint: 1199,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 991,
        settings: {
          //   centerMode: true,
          // infinite: true,
          // centerPadding: '60px',
          slidesToShow: 1,
          slidesToScroll: 1
          // speed: 500,
          // variableWidth: false
        }
      }, {
        breakpoint: 768,
        settings: {
          //   centerMode: true,
          // infinite: true,
          // centerPadding: '60px',
          slidesToShow: 1,
          slidesToScroll: 1
          // speed: 500,
          // variableWidth: false,
        }
      }
    ]

  })
  
  $('#insurance-cost').slick({
    draggable: true,
    arrows: false,
    dots: true,
    infinite: true,
    autoplay: true,
    speed: 2000,
    slidesToShow: 2,
    slidesToScroll: 1,
    responsive: [{
        breakpoint: 1450,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,

        }
      },
    //   {
    //     breakpoint: 1199,
    //     settings: {
    //       slidesToShow: 3,
    //       slidesToScroll: 1,
    //     }
    //   },
      {
        breakpoint: 991,
        settings: {
          //   centerMode: true,
          // infinite: true,
          // centerPadding: '60px',
          slidesToShow: 2,
          slidesToScroll: 1,
          // speed: 500,
          // variableWidth: false
        }
      }, {
        breakpoint: 768,
        settings: {
          //   centerMode: true,
          // infinite: true,
          // centerPadding: '60px',
          slidesToShow: 1,
          slidesToScroll: 1,
          // speed: 500,
          // variableWidth: false,
        }
      }
    ]

  })

  $('#cover-slider').slick({
    draggable: true,
    arrows: false,
    dots: true,
    infinite: true,
    autoplay: true,
    speed: 2000,
    slidesToShow: 2,
    slidesToScroll: 1,
    responsive: [{
        breakpoint: 1450,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,

        }
      },
      {
        breakpoint: 1199,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 991,
        settings: {
          //   centerMode: true,
          // infinite: true,
          // centerPadding: '60px',
          slidesToShow: 2,
          slidesToScroll: 1,
          // speed: 500,
          // variableWidth: false
        }
      }, {
        breakpoint: 768,
        settings: {
          //   centerMode: true,
          // infinite: true,
          // centerPadding: '60px',
          slidesToShow: 1,
          slidesToScroll: 1,
          // speed: 500,
          // variableWidth: false,
        }
      }
    ]

  });
    
    $('#cover1-slider').slick({
    draggable: true,
    arrows: false,
    dots: true,
    infinite: true,
    autoplay: true,
    speed: 2000,
    slidesToShow: 3,
    slidesToScroll: 1,
    responsive: [{
        breakpoint: 1450,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,

        }
      },
      {
        breakpoint: 1199,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 991,
        settings: {
          //   centerMode: true,
          // infinite: true,
          // centerPadding: '60px',
          slidesToShow: 2,
          slidesToScroll: 1,
          // speed: 500,
          // variableWidth: false
        }
      }, {
        breakpoint: 768,
        settings: {
          //   centerMode: true,
          // infinite: true,
          // centerPadding: '60px',
          slidesToShow: 1,
          slidesToScroll: 1,
          // speed: 500,
          // variableWidth: false,
        }
      }
    ]

  });
    
  $('.banner_box_select select').on('change', function () {
    var selVal = $(this).val();
    $(this).parents('form').find('.animate-btn').attr("href", selVal);
  });


  $(".slik_main_wrpr .blue_wrpr .blue_arrw span.cust_prev").on("click", function () {
    $('.slik_main_wrpr .blue_slik button.slick-prev').click();
  });
  $(".slik_main_wrpr .blue_wrpr .blue_arrw span.cust_next").on("click", function () {
    $('.slik_main_wrpr .blue_slik button.slick-next').click();
  });



  $(".testimonial_wrpr .testimonial_vdo_wrpr .testi_arrw span.cust_prev").on("click", function () {
    $('.testimonial_wrpr .clents_comments button.slick-prev').click();
  });
  $(".testimonial_wrpr .testimonial_vdo_wrpr .testi_arrw span.cust_next").on("click", function () {
    $('.testimonial_wrpr .clents_comments button.slick-next').click();
  });

}


// selectric

function selectric() {
  $(function () {
    $('select').selectric();
  });
}

// button animation 

function animate_btn() {

  $(function () {
    $('.animate-btn')
      .on('mouseenter', function (e) {
        var parentOffset = $(this).offset(),
          relX = e.pageX - parentOffset.left,
          relY = e.pageY - parentOffset.top;
        $(this).find('span').css({
          top: relY,
          left: relX
        })
      })
      .on('mouseout', function (e) {
        var parentOffset = $(this).offset(),
          relX = e.pageX - parentOffset.left,
          relY = e.pageY - parentOffset.top;
        $(this).find('span').css({
          top: relY,
          left: relX
        })
      });
  });
}

function sad() {
  var scroll = $(window).scrollTop();
  if (($('.barba-container').attr('data-namespace') == 'home') || ($('.barba-container').attr('data-namespace') == 'about-us')) {
    $("header.main-header").removeClass("header-fix-top_2");
  } else {
    $("header.main-header").addClass("header-fix-top_2");

  }
}
// nav bar fix top

function animate_header() {

  if ($("main").hasClass("home_overlay")) {
    $("body").addClass("overlay");
    $("body").removeClass("overlay_1");
  }

  if ($("main").hasClass("pension_overlay")) {
    $("body").addClass("overlay_1");
    $("body").removeClass("overlay");
  }


  if ($('.barba-container').attr('data-namespace') == 'home' || $('.barba-container').attr('data-namespace') == 'about-us' || $('.barba-container').attr('data-namespace') == 'pension') {



    $(window).scroll(function () {

      var scroll = $(window).scrollTop();
      // var smoothClass = $("header.main-header").hasClass("header-smooth");
      if (scroll >= 60) {
        $("body").addClass("nav-fixed-top");
        $("header.main-header").addClass("header-fix-top header-smooth");
      } else {
        $("header.main-header.header-fix-top").removeClass("header-fix-top");
        $("body").removeClass("nav-fixed-top");
      }
    });


    var scroll = $(window).scrollTop();
    if (scroll >= 60) {
      $("body").addClass("nav-fixed-top");
      $("header.main-header").addClass("header-fix-top header-smooth");
    } else {
      $("header.main-header.header-fix-top").removeClass("header-fix-top");
      $("body").removeClass("nav-fixed-top");
    }

  }
}

// floating input

function floating() {
  $('.is-floating-label input, .is-floating-label textarea').on('focus blur', function (e) {
    $(this).parents('.is-floating-label').toggleClass('is-focused', (e.type === 'focus' || this.value.length > 0));
  }).trigger('blur');
}

//date of birth
function datepiker() {
  $('.date').on('change', function () {
    var tHis = $(this);
    setTimeout(function () {
      var new_val = tHis.val().split('/'),
        dateYear = new_val[0],
        dateMonth = new_val[1],
        dateDay = new_val[2];

      $("#day").val(dateDay);
      $("#month").val(dateMonth);
      $("#year").val(dateYear);
    }, 0);
  });
  $(".date").datepicker({
    dateFormat: 'yy/MM/dd'
  });
}

// tab

function tab() {
  $(".tabgroup .panel .faq_blk .faq_ques").on("click", function () {
    $(this).parent().toggleClass("active");
  })

  $(".pension_service .fl-container .types_pension .pension_blk .pension_head").on("click", function () {
    $(this).parent().toggleClass("active1");
  })


  // mortgage

  $(".second_applicant p").on("click", function () {
    $(this).parent().toggleClass("show");
  })




}

function country() {
  // $(document).ready(function() {
  //   /*Query the user's IP address, make a request to freegeoip.net, parse json data for country code*/
  //   $.getJSON("http://freegeoip.net/json/", function(data) {
  //     //set country code and name
  //     setCountry(data.country_code, data.country_name);
  //   });

  // })

  function setCountry(code, name) {
    /*Move this <option> to the top of the list by removing it and recreating it at the top of the list*/
    $('#country_select').find('option[value="' + code + '"]').remove();
    $('#country_select').prepend('<option value="' + code + '" selected>' + name + '</option>');
  }
}




// Wrap every letter in a span

// var textWrapper = document.querySelector('.ml3');
// textWrapper.innerHTML = textWrapper.textContent.replace(/\S/g, "<span class='letter'>$&</span>");

// anime.timeline({loop: false})
//   .add({
//     targets: '.ml3 .letter',
//     opacity: [0,1],
//     easing: "easeInOutQuad",
//     duration: 2250,
//     delay: (el, i) => 150 * (i+1)
//   }).add({
//     targets: '.ml3',
//     opacity: [0,1],
//     duration: 1000,
//     easing: "easeOutExpo",
//     delay: 1000
//   });


/*webp support for IE */

function replacewebp() {
  function WebpIsSupported(callback) {
    // If the browser doesn't has the method createImageBitmap, you can't display webp format
    if (!window.createImageBitmap) {
      callback(false);
      return;
    }
    // Base64 representation of a white point image
    var webpdata = 'data:image/webp;base64,UklGRiQAAABXRUJQVlA4IBgAAAAwAQCdASoCAAEAAQAcJaQAA3AA/v3AgAA=';
    // Retrieve the Image in Blob Format
    fetch(webpdata).then(function (response) {
      return response.blob();
    }).then(function (blob) {
      // If the createImageBitmap method succeeds, return true, otherwise false
      createImageBitmap(blob).then(function () {
        callback(true);
      }, function () {
        callback(false);
      });
    });
  }
  WebpIsSupported(function (isSupported) {
    if (isSupported) {
      console.log("Supported");
    } else {
      console.log("Not supported");


      var WebP = new Image();
      WebP.onload = WebP.onerror = function () {
        if (WebP.height != 2) {
          $('img[data-srcset$=".webp"]').each(function (index, element) {
            $this = $(this)
            $src = $this.attr('data-srcset').replace('.webp', '.png');
            $this.attr('data-srcset', $src);
          });
          $('img[src$=".webp"]').each(function (index, element) {
            $this = $(this)
            $src = $this.attr('src').replace('.webp', '.png');
            $this.attr('src', $src);
          });
          $('source[data-srcset$=".webp"]').each(function (index, element) {
            $this = $(this)
            $src = $this.attr('data-srcset').replace('.webp', '.png');
            $this.attr('data-srcset', $src);
          });

        }
      };
      WebP.src = 'data:image/webp;base64,UklGRjoAAABXRUJQVlA4IC4AAACyAgCdASoCAAIALmk0mk0iIiIiIgBoSygABc6WWgAA/veff/0PP8bA//LwYAAA';
    }
  });

}




$(document).ready(function () {


  Barba.Dispatcher.on('transitionCompleted', function (currentStatus, oldStatus, container) {
    selectric();
    slick();
    animate_btn();
    animate_header();
    sad();
    floating();
    datepiker();
    tab();
    preventReload();
    country();
    replacewebp();
  });

  selectric();
  slick();
  animate_btn();
  animate_header();
  sad();
  floating();
  datepiker();
  tab();
  preventReload();
  country();
  replacewebp();
}); //Document Close



// $(window).on("scroll", function () {
//   animate_header();

// });

//   if ($('.barba-container').attr('data-namespace') == 'home') {
//     $(window).on("load", function () {
//       $("body").addClass("overlay");
//     });
//   }

//   if ($('.barba-container').attr('data-namespace') == 'pension') {
//     $(window).on("load", function () {
//       $("body").addClass("overlay_1");
//     });
//   }


var home = Barba.BaseView.extend({
  namespace: 'home',
  onEnter: function () {
    /* // The new Container is ready and attached to the DOM. */

  },
  onEnterCompleted: function () {
    /* // The Transition has just finished. */
  },
  onLeave: function () {
    /* // A new Transition toward a new page has just started. */
  },
  onLeaveCompleted: function () {
    /* // The Container has just been removed from the DOM. */
  }
});

Barba.Pjax.init();
Barba.Prefetch.init();
Barba.Pjax.cacheEnabled = false;
Barba.Pjax.start();

/*Barba transition */
var FadeTransition = Barba.BaseTransition.extend({
  start: function () {
    /**
     * This function is automatically called as soon the Transition starts
     * this.newContainerLoading is a Promise for the loading of the new container
     * (Barba.js also comes with an handy Promise polyfill!)
     */

    // As soon the loading is finished and the old page is faded out, let's fade the new page
    Promise
      .all([this.newContainerLoading, this.fadeOut()])
      .then(this.fadeIn.bind(this));
  },

  fadeOut: function () {
    /**
     * this.oldContainer is the HTMLElement of the old Container
     */

    return $(this.oldContainer).animate({
      opacity: 0
    }).promise();
  },

  fadeIn: function () {
    /**
     * this.newContainer is the HTMLElement of the new Container
     * At this stage newContainer is on the DOM (inside our #barba-container and with visibility: hidden)
     * Please note, newContainer is available just after newContainerLoading is resolved!
     */
    $(window).scrollTop(0)
    var _this = this;
    var $el = $(this.newContainer);

    $(this.oldContainer).hide();

    $el.css({
      visibility: 'visible',
      opacity: 0
    });

    $el.animate({
      opacity: 1
    }, 400, function () {
      /**
       * Do not forget to call .done() as soon your transition is finished!
       * .done() will automatically remove from the DOM the old Container
       */

      _this.done();
    });
  }
});

/**
 * Next step, you have to tell Barba to use the new Transition
 */

Barba.Pjax.getTransition = function () {
  /**
   * Here you can use your own logic!
   * For example you can use different Transition based on the current page or link...
   */

  return FadeTransition;
};


//Form validation

function preventReload() {

  /* About us Modal */

  $('.readbio').click(function (e) {
    e.preventDefault();
    var content_txt = $(this).html();
    $('#exampleModal .modal-body .dp_wrpr').html(content_txt);
  });

  $("#buttonId").click(function () {
    var protection_box_value = $('#protection_box').val();
    var protection_box_name = $('#protection_box :selected').text();
    window.location = "http://demo.webandcrafts.com/wp-financial-life/" + protection_box_name + "/" + protection_box_value;

  });


  /* Blog */


  $("select").change(function () {
    $(this).find("option:selected").each(function () {
      var optionValue = $(this).attr("value");
      if (optionValue) {
        $(".blog_blk").not("." + optionValue).hide();
        $("." + optionValue).show();
      } else {
        $(".blog_blk").hide();
      }
    });
  }).change();


  $("li.faq-tab-holder").click(function () {
    $(this).addClass('active').siblings().removeClass('active');
    $(".faq_blk").hide();
    var cls = jQuery(this).attr('id');
    $('.' + cls).show();
  });


  $("#find_faq").validate({
    rules: {
      faqname: {
        required: true
      }
    },
    submitHandler: function (form) {
      $.ajax({
        url: ajaxObj.ajaxurl,
        type: 'POST',
        data: {
          action: 'get_faq_details',
          faqname: jQuery('#faqname').val()
        },
        beforeSend: function () {
          $("#ideas-loader").show();
        },
        success: function (data) {
          $("#loadpanel, .faq-tab-holder").hide();
          if (data == false) {
            $("#achieve-view").html("<b>No Result Found</b>!");
            $("#ideas-loader").hide();
          } else {
            jQuery('#achieve-view').html(data);
            $("#ideas-loader").hide();
          }

        },
      });
    }
  });


//client-form 
  
  $('#client-form').validate({
    
    ignore: ':hidden:not(select)',
    errorPlacement: function (error, element) {
      var data = element.data('selectric');
      error.appendTo(data ? element.closest('.' + data.classes.wrapper).parent() : element.parent());
    },
    rules: {
      name: {
        required: true,
      },
      email: {
        required: true,
        email: true,
      },
      contact: {
        required: true,
        minlength: 8,
        maxlength: 12,
        number: true,
      },
      query: {
        required: true,
      },
    },

    messages: {
      email: "Please enter a valid email address",
      contact: "Not a valid number",
      query: "This field is required"
    },
    errorElement: 'div',
    errorLabelContainer: '.errorTxt'
  });

  
  
  
    $('#client-form-joseph').validate({
    
    ignore: ':hidden:not(select)',
    errorPlacement: function (error, element) {
      var data = element.data('selectric');
      error.appendTo(data ? element.closest('.' + data.classes.wrapper).parent() : element.parent());
    },
    rules: {
      name: {
        required: true,
      },
      email: {
        required: true,
        email: true,
      },
      contact: {
        required: true,
        minlength: 8,
        maxlength: 12,
        number: true,
      },
      query: {
        required: true,
      },
    },

    messages: {
      email: "Please enter a valid email address",
      contact: "Not a valid number",
      query: "This field is required"
    },
    errorElement: 'div',
    errorLabelContainer: '.errorTxt'
  });
  
  
  
  
  $('#client-form-aidan').validate({
    
    ignore: ':hidden:not(select)',
    errorPlacement: function (error, element) {
      var data = element.data('selectric');
      error.appendTo(data ? element.closest('.' + data.classes.wrapper).parent() : element.parent());
    },
    rules: {
      name: {
        required: true,
      },
      email: {
        required: true,
        email: true,
      },
      contact: {
        required: true,
        minlength: 8,
        maxlength: 12,
        number: true,
      },
      query: {
        required: true,
      },
    },

    messages: {
      email: "Please enter a valid email address",
      contact: "Not a valid number",
      query: "This field is required"
    },
    errorElement: 'div',
    errorLabelContainer: '.errorTxt'
  });


  $('#contact-form').validate({
    submitHandler: function (form) {
      var response = grecaptcha.getResponse();
      //recaptcha failed validation
      if (response.length == 0) {
        //            alert('no');
        $('#recaptcha-error').show();
        return false;
      } else {
        $('#recaptcha-error').hide();
        //            alert('yes');
        return true;
      }
    },
    ignore: ':hidden:not(select)',
    errorPlacement: function (error, element) {
      var data = element.data('selectric');
      error.appendTo(data ? element.closest('.' + data.classes.wrapper).parent() : element.parent());
    },
    rules: {
      name: {
        required: true,
      },
      email: {
        required: true,
        email: true,
      },
      contact: {
        required: true,
        minlength: 8,
        maxlength: 12,
        number: true,
      },
      terms: {
        required: true,
      },
      protection: {
        required: true
      }
    },

    messages: {
      email: "Please enter a valid email address",
      contact: "Not a valid number",
      protection: "List is required"
    },
    errorElement: 'div',
    errorLabelContainer: '.errorTxt'
  });

  
  
  

  $('#mortgage-protection-form').validate({
    ignore: ':hidden:not(select)',
    errorPlacement: function (error, element) {
      var data = element.data('selectric');
      error.appendTo(data ? element.closest('.' + data.classes.wrapper).parent() : element.parent());
    },
    rules: {
      amount: {
        required: true,
      },

      age: {
        required: true,
      },

      name: {
        required: true,
        minlength: 2,
      },

      email: {
        required: true,
        email: true,
      },
      contact: {
        required: true,
        minlength: 8,
        maxlength: 15,
        number: true,
      },
      gender1a: {
        required: true,
      },
      radiosmoker: {
        required: true,
      },
      terms: {
        required: true,
      },
      day: {
        required: true,
      },
      month: {
        required: true,
      },
      years: {
        required: true,
      },
      lifecover: {
        required: true,
        number: true,
      },
      radiorelation: {
        required: true,
      },
      termcover: {
        required: true,
      }

    },

    messages: {
      email: "Please enter a valid email address",
      contact: "Not a valid number",
      age: "Please choose age",
      amount: "Please choose a value",
      gender1a: "This field is required",
      terms: "Terms and condition not checked",
      radiorelation: "This field is required",
      radiosmoker: "This field is required",
      day: "Please choose a Day",
      month: "Please choose a Month",
      years: "Please choose a Years",
      lifecover: "Please enter valid Amount",
      termcover: "List is required"
    },
    errorElement: 'div',
    errorLabelContainer: '.errorTxt'
  });

  
  
  
  
  $('#serious-illness-form').validate({
    ignore: ':hidden:not(select)',
    errorPlacement: function (error, element) {
      var data = element.data('selectric');
      error.appendTo(data ? element.closest('.' + data.classes.wrapper).parent() : element.parent());
    },
    rules: {
      amount: {
        required: true,
      },

      age: {
        required: true,
      },

      name: {
        required: true,
        minlength: 2,
      },

      email: {
        required: true,
        email: true,
      },
      contact: {
        required: true,
        minlength: 8,
        maxlength: 15,
        number: true,
      },
      gender1a: {
        required: true,
      },
      radiosmoker: {
        required: true,
      },
      terms: {
        required: true,
      },
      day: {
        required: true,
      },
      month: {
        required: true,
      },
      years: {
        required: true,
      },
      lifecover: {
        required: true,
        number: true,
      },
      radiorelation: {
        required: true,
      },
      termcover: {
        required: true,
      }
      //      sec_gender1a: {
      //        required: true,
      //      },
      //      sec_radiosmoker: {
      //        required: true,
      //      }
    },

    messages: {
      email: "Please enter a valid email address",
      contact: "Not a valid number",
      age: "Please choose age",
      amount: "Please choose a value",
      gender1a: "This field is required",
      terms: "Terms and condition not checked",
      radiorelation: "This field is required",
      radiosmoker: "This field is required",
      day: "Please choose a Day",
      month: "Please choose a Month",
      years: "Please choose a Years",
      lifecover: "Please enter valid Amount",
      termcover: "List is required"
    },
    errorElement: 'div',
    errorLabelContainer: '.errorTxt'
  });
  
  
  
  
  
  $('#insurance-quote').validate({
    ignore: ':hidden:not(select)',
    errorPlacement: function (error, element) {
      var data = element.data('selectric');
      error.appendTo(data ? element.closest('.' + data.classes.wrapper).parent() : element.parent());
    },
    rules: {
      name: {
        required: true,
      },
      email: {
        required: true,
        email: true,
      },
      contact: {
        required: true,
        minlength: 8,
        maxlength: 15,
        number: true,
      },
      gender1a: {
        required: true,
      },
      radiosmoker: {
        required: true,
      },
      terms: {
        required: true,
      },
      lifecover: {
        required: true,
        number: true,
      },
      radiorelation: {
        required: true,
      },
      convertiable: {
        required: true,
      },
      termcover: {
        required: true,
      },
      day: {
        required: true,
      },
      month: {
        required: true,
      },
      years: {
        required: true,
      }

      //      secday: {
      //        required: true,
      //      },
      //      secmonth: {
      //        required: true,
      //      },
      //      secyears: {
      //        required: true,
      //      },
      //      sec_gender1a: {
      //        required: true,
      //      },
      //      sec_radiosmoker: {
      //        required: true,
      //      }
    },

    messages: {
      name: "This field is required",
      secday: "Please choose a Day",
      secmonth: "Please choose a Month",
      secyears: "Please choose a Years",
      sec_radio_group_5: "List is required",
      sec_radio_group_6: "List is required",
      convertiable: "List is required",
      email: "Please enter a valid email address",
      contact: "Not a valid number",
      day: "Please choose a Day",
      month: "Please choose a Month",
      years: "Please choose a Years"
    },
    errorElement: 'div',
    errorLabelContainer: '.errorTxt'
  });

  $('#life-and-serious-illness-quote').validate({
    ignore: ':hidden:not(select)',
    errorPlacement: function (error, element) {
      var data = element.data('selectric');
      error.appendTo(data ? element.closest('.' + data.classes.wrapper).parent() : element.parent());
    },
    rules: {
      name: {
        required: true,
      },
      email: {
        required: true,
        email: true,
      },
      contact: {
        required: true,
        minlength: 8,
        maxlength: 15,
        number: true,
      },
      terms: {
        required: true,
      },
      lifecover: {
        required: true,
      },
      radiorelation: {
        required: true,
      },
      gender1a: {
        required: true,
      },
      termcover: {
        required: true,
      },
      convertiable: {
        required: true,
      },
      radiosmoker: {
        required: true,
      },
      illnescover: {
        required: true
      },
      termscover: {
        required: true,
      },
      day: {
        required: true,
      },
      month: {
        required: true,
      },
      years: {
        required: true,
      }
      //      secday: {
      //        required: true,
      //      },
      //      secmonth: {
      //        required: true,
      //      },
      //      secyears: {
      //        required: true,
      //      },
      //      sec_gender1a: {
      //        required: true,
      //      },
      //      sec_radiosmoker: {
      //        required: true,
      //      }
    },

    messages: {
      name: "This field is required",
      email: "Please enter a valid email address",
      contact: "Not a valid number",
      day: "Please choose a Day",
      month: "Please choose a Month",
      years: "Please choose a Years",
      secday: "Please choose a Day",
      secmonth: "Please choose a Month",
      secyears: "Please choose a Years",
      sec_gender1a: "List is required",
      sec_radiosmoker: "List is required",
    },
    errorElement: 'div',
    errorLabelContainer: '.errorTxt'
  });

  $('#income-cover-form').validate({
    ignore: ':hidden:not(select)',
    errorPlacement: function (error, element) {
      var data = element.data('selectric');
      error.appendTo(data ? element.closest('.' + data.classes.wrapper).parent() : element.parent());
    },
    rules: {
      name: {
        required: true,
      },
      email: {
        required: true,
        email: true,
      },
      contact: {
        required: true,
        minlength: 8,
        maxlength: 15,
        number: true,
      },
      gender1a: {
        required: true,
      },
      radiosmoker: {
        required: true,
      },
      amountcover: {
        required: true,
        number: true,
      },
      terms: {
        required: true,
      },
      agecover: {
        required: true,
      },
      amount: {
        required: true,
      },
      employment: {
        required: true,
      },
      day: {
        required: true,
      },
      month: {
        required: true,
      },
      years: {
        required: true,
      },
      lifecover: {
        required: true,
      },
      employmenttype: {
        required: true,
      }
    },

    messages: {
      lifecover: "This field is required",
      agecover: "This field is required",
      employmenttype: "This field is required",
      email: "Please enter a valid email address",
      contact: "Not a valid number",
      employment: "This field is required",
      amountcover: "Please Enter Number",
      day: "Please choose a Day",
      month: "Please choose a Month",
      years: "Please choose a Years",
      gender1a: "This field is required",
      radiosmoker: "This field is required"
    },
    errorElement: 'div',
    errorLabelContainer: '.errorTxt'
  });

  $('#whole-of-life-insurance-form').validate({
    ignore: ':hidden:not(select)',
    errorPlacement: function (error, element) {
      var data = element.data('selectric');
      error.appendTo(data ? element.closest('.' + data.classes.wrapper).parent() : element.parent());
    },
    rules: {
      name: {
        required: true,
      },
      email: {
        required: true,
        email: true,
      },
      contact: {
        required: true,
        minlength: 8,
        maxlength: 15,
        number: true,
      },
      gender1a: {
        required: true,
      },
      radiosmoker: {
        required: true,
      },
      terms: {
        required: true,
      },
      lifecover: {
        required: true,
      },
      day: {
        required: true,
      },
      month: {
        required: true,
      },
      years: {
        required: true
      }
    },

    messages: {
      email: "Please enter a valid email address",
      contact: "Not a valid number",
      day: "Please choose a Day",
      month: "Please choose a Month",
      years: "Please choose a Years"
    },
    errorElement: 'div',
    errorLabelContainer: '.errorTxt'
  });

  $('#pension-form').validate({
    ignore: ':hidden:not(select)',
    errorPlacement: function (error, element) {
      var data = element.data('selectric');
      error.appendTo(data ? element.closest('.' + data.classes.wrapper).parent() : element.parent());
    },
    rules: {
      firstname: {
        required: true,
      },
      lastname: {
        required: true,
      },
      email: {
        required: true,
        email: true,
      },
      contact: {
        required: true,
        minlength: 8,
        maxlength: 15,
        number: true,
      },
      radiorelation: {
        required: true,
      },
      contributing_pension: {
        required: true,
      },
      status: {
        required: true,
      },
      pensiontype: {
        required: true,
      },
      country_select: {
        required: true,
      },
      terms: {
        required: true,
      }
    },

    messages: {
      email: "Please enter a valid email address",
      contact: "Not a valid number",
      status: "List is required",
      pensiontype: "List is required",
      country: "List is required",
      contributing_pension: "This field is required",
      country_select: "Please select Country",
    },
    errorElement: 'div',
    errorLabelContainer: '.errorTxt'
  });

  $('#investment').validate({
    ignore: ':hidden:not(select)',
    errorPlacement: function (error, element) {
      var data = element.data('selectric');
      error.appendTo(data ? element.closest('.' + data.classes.wrapper).parent() : element.parent());
    },
    rules: {
      name: {
        required: true,
      },
      contact: {
        required: true,
        minlength: 8,
        maxlength: 15,
        number: true,
      },
      email: {
        required: true,
        email: true,
      },
      lifecover: {
        required: true,
      },
      gender1a: {
        required: true,
      },
      day: {
        required: true,
      },
      month: {
        required: true,
      },
      years: {
        required: true,
      },
      income: {
        required: true,
      },

      terms: {
        required: true,
      }
    },

    messages: {
      email: "Please enter a valid email address",
      contact: "Not a valid number",
      lifecover: "List is required",
      radio_group_10: "List is required",
      day: "Please choose a Day",
      month: "Please choose a Month",
      years: "Please choose a Years"
    },
    errorElement: 'div',
    errorLabelContainer: '.errorTxt'
  });

  $('#mortgages-form').validate({
    submitHandler: function (form) {
      var response = grecaptcha.getResponse();
      //recaptcha failed validation
      if (response.length == 0) {
        //            alert('no');
        $('#recaptcha-error').show();
        return false;
      } else {
        $('#recaptcha-error').hide();
        //            alert('yes');
        return true;
      }
    },
    ignore: ':hidden:not(select)',
    errorPlacement: function (error, element) {
      var data = element.data('selectric');
      error.appendTo(data ? element.closest('.' + data.classes.wrapper).parent() : element.parent());
    },
    rules: {
      applicant_type: {
        required: true,
      },
      name: {
        required: true,
      },
      email: {
        required: true,
        email: true,
      },
      contact: {
        required: true,
        number: true,
        minlength: 8,
        maxlength: 15,
      },
      terms: {
        required: true,
      },
    },

    messages: {
      applicant_type: "This field is required",
      name: "This field is required",
      buy: "This field is required",
      price_property: "This field is required",
      load_amount: "This field is required",
      property_address: "This field is required",
      type_property: "This field is required",
      purchase_price: "This field is required",
      year_purchase: "This field is required",
      amount_owe: "This field is required",
      monthly_mortgage: "This field is required",
      estimated_value: "This field is required",
      cremployment_single: "This field is required",
      time_employment_single: "This field is required",
      annual_income_single: "This field is required",
      credit_rating: "This field is required",
      time_contact: "This field is required",
      current_app_status: "This field is required",
      terms: "This field is required",
      secs_name: "This field is required",
      secs_email: "Please enter a valid email address",
      secs_contact: "This field is required",
      annual_income: "This field is required",
      terms: "This field is required",
      hiddenRecaptcha: "dsdssdsd"
    },
    errorElement: 'div',
    errorLabelContainer: '.errorTxt',

  });

  /* Select box jQuery validaation */

  $('select[name="lifecover"]').on('change', function () {
    $(this).valid();
  });

  $('select[name="termcover"]').on('change', function () {
    $(this).valid();
  });

  $('select[name="day"]').on('change', function () {
    $(this).valid();
  });

  $('select[name="month"]').on('change', function () {
    $(this).valid();
  });

  $('select[name="years"]').on('change', function () {
    $(this).valid();
  });

  $('select[name="secday"]').on('change', function () {
    $(this).valid();
  });

  $('select[name="secmonth"]').on('change', function () {
    $(this).valid();
  });

  $('select[name="secyears"]').on('change', function () {
    $(this).valid();
  });

  $('select[name="lifecover"]').on('change', function () {
    $(this).valid();
  });

  $('select[name="protection"]').on('change', function () {
    $(this).valid();
  });

  $('select[name="illnescover"]').on('change', function () {
    $(this).valid();
  });

  $('select[name="agecover"]').on('change', function () {
    $(this).valid();
  });

  $('select[name="employmenttype"]').on('change', function () {
    $(this).valid();
  });

  $('select[name="protection_box"]').on('change', function () {
    $(this).valid();
  });


  
  
   /* Life Insurance Modal */
  
  $('.readbio').click(function(e) {
    e.preventDefault();
    var content_txt = $(this).html();
    $('#insuranceModal .modal-body .dp_wrpr').html(content_txt);
  });


  $('.qamodal_cls').click(function(e) {
    e.preventDefault();
    var content_txt = $(this).html();
    $('#qamodal .modal-body .qst_ans_wrap').html(content_txt);
  });


  
  
}
