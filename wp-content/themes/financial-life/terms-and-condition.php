<?php

/**
  Template Name: terms-and-condition
*/


get_header();
?>


<div id="barba-wrapper">
  <div class="barba-container terms" data-namespace="terms">
    <main>
      <section class="privacy_wrpr">
        <div class="fl-container">
          <div class="privcy">
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post();?>
            <h1><?php the_title();?></h1>
            <?php the_content();?>
            <?php endwhile; else: ?>
            <p>Sorry, no posts matched your criteria.</p>
            <?php endif; ?>
          </div>
        </div>
      </section>
    </main>

  </div>
</div>

<?php
get_footer();
